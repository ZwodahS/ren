tests:
	haxe -L heaps-dev -L hxrandom -L hlsdl -L console -L zf -p src -D test -D debug -D loggingLevel=30 --hl test.hl --main tests.Test

lint:
	haxelib run formatter -s src

docs: xml pages

xml:
	haxe docs.hxml

pages:
	haxelib run dox -i build/docs -o build/pages --include ren
