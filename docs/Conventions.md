# Overview

This documentations list some of the conventions used in the repo.
This does not reflect the current state of the repo but what will be used moving forward.
A timestamp is also provided for each comment to know when it was made.

## Naming

### Capitalization

(18 Aug 2021)
Constants are usually in Capitalized CamelCase. They are usually final, but sometimes it might be var.
In case when it is a `var` and not `final` the field is meant to be used like a constant and should only be modified during construction and should not be modified afterwards.
