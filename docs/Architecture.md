# Overview

This doc documents the overall architecture for the framework and various decisions/implementations.

# Systems

## ActionSystem / TurnSystem

ActionSystem is the main system that handles actions taken by entity. It is a very light system that doesn't do much. The bulk of work is actually done by the TurnSystem.

As for TurnSystem, as of this writing, there are 2 TurnSystem provided in ren.ext, namely TUTurnSystem and APTurnSystem. The details for the differences in the 2 systems can be found in the their respective files, which is not the purpose of this doc.

New types of TurnSystems can be implemented as long as they follows the conventions.

### Messages
All TurnSystem should emit the follow messages.

#### 1. EntityTurnStart

This message is emitted to inform the start of an entity's turn. The message contains a disrupted flag to allow for other systems to disrupt the entity turn. TurnSystem need to read this flag to move to the next entity as though the entity's turn was ended by the entity, including firing the respective events.

#### 2. EntityActiveTurn

This denotes the start of the entity's turn. The entity can now take their. For player this will be to handle the player's input. For AI, it can be handled by AISystem.

#### 3. EntityTurnEnd

This message is fired to denote the end of entity turn. The TurnSystem will have to listen EntityTakeAction to know when entity took action.
