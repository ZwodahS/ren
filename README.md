# Goals
The goal of this repo is to provide a structure for roguelike development.
It will provide systems for commonly used features in roguelike.
These features can be opt in by adding them into the World.

Some systems depends on others, but most of them are only loosely coupled by events.
You can easily implement your own systems and emit the same messages to create the same effects.

# Designs

As mentioned, all systems communications are done via Messages and the MessageDispatcher.

# Usage
This project is currently for personal usage, and is highly unstable.
There will be breaking changes, so use it at your own risk.
If you choose to use anything, would highly suggest copying stuffs over to your own repo, or just take inspiration from it.
Things will be refactored and may be rendered useless as many of the things there are highly experimental.
