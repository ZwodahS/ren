# Design Choices

Ideally all design choices and the changes will be documented here to explain why the choice was made.
Some decision will be missed, but will try to document as much of the important decision and assumptions.

## Assumptions / Null-Assumptions

### A.1 Assumptions on currentLevel.
Wed Jan 06 2021
Initially at the start of the project, ren.core.World has a currentLevel.
The main reason is it was meant to be the current level that the player is on.
This is because of the original assumption that the engine will assume that there will only be one level, similar to mystery dungeon games or like cogmind.
Later on, the concept of loading / unloading level was added, but the assumption of currentLevel was not removed.
The reason for not removing was because RenderSystem was still using currentLevel to decide how to render.

Since then, the rendersystem has been refactored, and currentLevel in world was no longer necessary.

This assumption also made level transition hard, so since it doesn't add much, it will be removed.
This removal was done together with M.1, which deals with movement between levels.
In additional to that, LevelChanged message will also be removed, since it is now useless.


## Movements

### M.1 Movement between levels
??? ??? ?? 2020
Movement between levels will be handled by firing 2 different events.
The first event will be from oldPosition -> null. When this event fired, the LocationComponent.level of the entity will still be the old level.
The second event will be from null -> newPosition. When this event fired, the LocationComponent.level of the entity will be the new level.

Wed Jan 06 2021
Not sure when the above decided is made, however, the new implementation will now be a single event, which will be fired when the entity is already on the new level.
The event fired will be EntityMoved, which now contains the Level object, in additional to the position.

In additional to that, level changing is also removed, together with the concepts of "currentLevel" in ren.core.World.
See A.1 for details on this.

