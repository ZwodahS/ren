# Overview
This doc document the attempt at enabling batch actions, in particular batched movement.
The idea behind batched actions is to allow for animations of movement to happen at the same time.
This has a lot of benefits but to enable it requires quite a lot of redesign.

# Attempt 1
Fri Apr 02 13:05:59 2021
Even tho I got the basic idea done, I have decided to revert the code.
The code is now in the branch `failedattempt/batchactions`.

The idea behind it is quite simple, check if an action can be batched with existings actions, and if possible, add it to the batch and trigger the entity next turn.
This requires some changes, in particular having a specific event that trigger the turn passing to allow the turn system to choose the next entity.
The ActionSystem will also prevent any entity from performing 2 actions in the same batch.

If the action cannot be batched then the current batch will be flushed and the new actions will either be added to batch or perform immediately.

Although the attempt got to a point where it is almost working, there were a few problems.

## Problems
### 1. Convoluted Code
The ActionSystem became a piece of mess and had to work around a few things in order to make things general.
Due to the change, the State Diagram of ActionSystem become extremely hard to managed.

### 2. Weird Interactions
For example, init() will check if the action can be perform.
The data set in init will then affect the return value of "canBatcHWith".
Afterwhich, the state of the world is no longer the same as it as during init.
For action to be able to properly handle it, it will need some prediction on what the state of the world is after all the batched actions is performed.

This is a problem as the design now requires all actions to handle future unimplemented batched action and have to predict accordingly.
This makes the code really hard to reason about.

### 3. Little to no gain
There is little to gain from having a complex architcture at the engine level.
The solution can be just as easy as speeding up animations etc.

## Conclusion
With this, I have decided not to do this, at least with this implementation.
In order to do it, I suspect we will need to change some underlying assumptions.

For example, the state control cannot be handle by different systems.
Right now the state is handled by action system firing EntityTakeAction and TurnSystem firing EntityActiveTurn.
This was done intentionally to allow ActionSystem to be separated from any implementation of Turnsystem.
If there is an attempt to do this again in the future, likely merging TurnSystem and ActionSystem will be a good place to start.

