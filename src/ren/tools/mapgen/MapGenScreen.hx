package ren.tools.mapgen;

import ren.mapgen.MapgenPipeline;
import ren.mapgen.MapgenMap;

import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;
import zf.WrappedValue;
import zf.Assets.LoadedSpritesheet;
import zf.Point2i;

using zf.h2d.ObjectExtensions;

/**
	This is a generic screen for visualising map generation algorithm.
**/
class MapGenScreen extends zf.Screen {
	var runner: MapgenPipeline;
	var sb: h2d.SpriteBatch;
	var mapRender: Vector2D<h2d.SpriteBatch.BatchElement>;
	var mapString: Vector2D<String>;
	var ss: LoadedSpritesheet;
	var stateText: h2d.Text;

	var controlStartX: Int;

	public var font: h2d.Font;

	public var speed: Int = 1;

	var mapSize: Point2i;
	var lastControl: h2d.Object;

	var controlDrawLayer: h2d.Layers;

	public function new(runner: MapgenPipeline, ss: LoadedSpritesheet, font: h2d.Font = null,
			controlStartX: Int = 900) {
		super();
		this.font = font;
		this.controlStartX = controlStartX;

		this.addChild(this.controlDrawLayer = new h2d.Layers());
		this.controlDrawLayer.x = controlStartX;
		var interactive = new h2d.Interactive(1600, 900, this);
		interactive.cursor = Default;
		interactive.onWheel = function(e) {
			this.controlDrawLayer.y -= 3 * e.wheelDelta;
		}
		interactive.propagateEvents = true;

		this.stateText = new h2d.Text(this.font);
		this.stateText.x = 10;
		this.stateText.y = 10;
		this.stateText.text = "";
		this.addChild(this.stateText);

		this.runner = runner;
		this.ss = ss;
		this.mapSize = [0, 0];

		setMap(this.runner.map);

		if (this.font == null) {
			this.font = hxd.res.DefaultFont.get().clone();
			this.font.resizeTo(16);
		}

		var w = new WrappedValue<Int>(mapSize.x);
		w.listen(function(o, n) {
			this.mapSize.x = n;
		});
		addIntOption("SizeX", w);

		var w = new WrappedValue<Int>(mapSize.y);
		w.listen(function(o, n) {
			this.mapSize.y = n;
		});
		addIntOption("SizeY", w);

		var w = new WrappedValue<Int>(this.speed);
		w.listen(function(o, n) {
			this.speed = n;
		});
		addIntOption("Simulation Speed", w);

		this.setConfiguration(this.runner.configuration);
	}

	function setMap(map: MapgenMap) {
		final cells = map.cells;
		this.mapSize.x = cells.size.x;
		this.mapSize.y = cells.size.y;
		if (this.mapString == null || this.mapString.size != cells.size) {
			this.mapString = new Vector2D<String>(cells.size, "#");
		}

		if (this.mapRender != null && this.mapRender.size != cells.size) {
			this.sb.remove();
			this.mapRender = null;
		}
		if (this.mapRender == null) {
			this.sb = new h2d.SpriteBatch(this.ss.tile);
			if (this.stateText.text == "") this.stateText.text = " ";
			this.sb.putBelow(this.stateText, [0, 5]);
			this.addChild(this.sb);

			this.mapRender = new Vector2D<h2d.SpriteBatch.BatchElement>(cells.size, null);
			for (xy => item in this.mapRender.iterateYX()) {
				var be = this.sb.alloc(getTile('#'));
				be.x = xy.x * 8;
				be.y = xy.y * 8;
				this.mapRender.set(xy.x, xy.y, be);
			}
		}
		redrawMap();
	}

	var optionIndex = 0;

	function setConfiguration(c: Map<String, Dynamic>) {
		var keys = [for (f in c.keys()) f];
		keys.sort(function(s1, s2) {
			return s1 < s2 ? -1 : 1;
		});
		for (f in keys) {
			var v = c[f];
			if (Std.isOfType(v, Int)) {
				var wrapped = new WrappedValue<Int>(cast(v, Int));
				wrapped.listen(function(o, n) {
					c[f] = n;
				});
				this.addIntOption(f, wrapped);
			} else if (Std.isOfType(v, Bool)) {
				var wrapped = new WrappedValue<Bool>(cast(v, Bool));
				wrapped.listen(function(o, n) {
					c[f] = n;
				});
				this.addBoolOption(f, wrapped);
			} else if (Std.isOfType(v, Array)) {
				try {
					var arr: Array<Bool> = v;
					if (arr.length == 9 || arr.length == 6) {
						this.addCellOption(f, arr);
					}
				} catch (e) {}
			}
		}
	}

	public function addIntOption(name: String, wrapped: WrappedValue<Int>) {
		var control = new IntControl(this.font, name, wrapped, [40, 20]);
		this.controlDrawLayer.addChild(control);
		if (lastControl == null) {
			control.setY(10);
		} else {
			control.putBelow(this.lastControl, [0, 5]);
		}
		lastControl = control;
	}

	public function addBoolOption(name: String, wrapped: WrappedValue<Bool>) {
		var control = new BoolControl(this.font, name, wrapped, [40, 20]);
		this.controlDrawLayer.addChild(control);
		if (lastControl == null) {
			control.setY(10);
		} else {
			control.putBelow(this.lastControl, [0, 5]);
		}
		lastControl = control;
	}

	public function addCellOption(name: String, arr: Array<Bool>) {
		var control = new CellularControl(this.font, name, arr);
		this.controlDrawLayer.addChild(control);
		if (lastControl == null) {
			control.setY(10);
		} else {
			control.putBelow(this.lastControl, [0, 5]);
		}
		lastControl = control;
	}

	dynamic public function getTile(s: String): h2d.Tile {
		var a = this.ss.assets.get('${s}');
		if (a == null) {
			Logger.warn('Assets for ${s} not found');
			a = this.ss.assets.get('?');
		}
		return a.getTile();
	}

	function finish() {
		this.runner.finish();
		redrawMap();
	}

	function runStep() {
		this.runner.step();
		redrawMap();
	}

	function runBigStep() {
		this.runner.bigStep();
		redrawMap();
	}

	var paused: Bool = false;

	override public function update(dt: Float) {
		if (paused) return;
		var hasChange = false;
		for (i in 0...this.speed) {
			if (this.runner.step()) hasChange = true;
		}
		if (hasChange) redrawMap();
	}

	public function redrawMap() {
		for (key => item in this.runner.map.cells.iterate()) {
			var curr = this.mapString.get(key.x, key.y);
			if (curr != item) {
				var b = this.mapRender.get(key.x, key.y);
				var t = getTile(item);
				b.t = t;
				this.mapString.set(key.x, key.y, item);
			}
		}
		this.stateText.text = this.runner.getState();
	}

	override public function onEvent(e: hxd.Event) {
		if (e.kind == hxd.Event.EventKind.EKeyDown) {
			switch (e.keyCode) {
				case hxd.Key.SPACE:
					runStep();
				case hxd.Key.U:
					runBigStep();
				case hxd.Key.R:
					restart();
				case hxd.Key.W:
					restart(false);
				case hxd.Key.S:
					this.paused = !this.paused;
				case hxd.Key.F:
					finish();
			}
		}
	}

	var currSeed: Int = 0;

	public function restart(newSeed = true) {
		var seed = newSeed ? this.runner.map.r.randomInt(zf.Constants.MaxInt32) : this.runner.map.seed;
		var map = MapgenMap.makeNew(this.mapSize.clone(), seed);
		this.runner.reset(map);
		this.setMap(map);
	}
}
