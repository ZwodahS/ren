package ren.tools.mapgen;

import zf.WrappedValue;
import zf.Point2i;

class BoolControl extends h2d.Layers {
	var boolValue: WrappedValue<Bool>;
	var text: h2d.Text;

	var labelText: h2d.Text;
	var size: Point2i;

	public function new(font: h2d.Font, label: String, boolValue: WrappedValue<Bool>, size: Point2i = null) {
		super();
		this.boolValue = boolValue;
		if (size == null) size = [32, 16];
		this.size = size;

		boolValue.listen(function(o, n) {
			this.setValue(n);
		});

		this.labelText = new h2d.Text(font);
		this.labelText.text = label;
		this.labelText.x = -this.labelText.textWidth - 10;
		this.add(this.labelText, 0);

		this.text = new h2d.Text(font);
		this.text.maxWidth = size.x;
		this.text.textAlign = Center;
		setValue(boolValue.value);

		var toggleButton = new h2d.Bitmap(h2d.Tile.fromColor(0x777777, size.x, size.y));
		toggleButton.addChild(this.text);
		var interactive = new h2d.Interactive(size.x, size.y, toggleButton);
		interactive.onClick = function(e: hxd.Event) {
			this.boolValue.value = !this.boolValue.value;
			this.text.text = boolValue.value ? "Yes" : "No";
		};
		this.add(toggleButton, 0);
	}

	function setValue(n: Bool) {
		this.text.text = this.boolValue.value ? "Yes" : "No";
	}
}
