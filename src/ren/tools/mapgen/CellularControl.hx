package ren.tools.mapgen;

import zf.WrappedValue;
import zf.Point2i;

using zf.h2d.ObjectExtensions;

private class Cell extends zf.ui.Button {
	public var value(default, set): Bool = false;
	public var bg: h2d.Bitmap;

	var text: h2d.Text;

	public function set_value(i: Bool): Bool {
		this.value = i;
		updateButton();
		return this.value;
	}

	public function new(font: h2d.Font, initialValue: Bool) {
		super(16, 16);
		this.bg = new h2d.Bitmap(h2d.Tile.fromColor(0xFFFFFF, 16, 16));
		this.addChild(this.bg);
		this.text = new h2d.Text(font);
		this.addChild(this.text);
		this.text.maxWidth = 16;
		this.text.textAlign = Center;
		this.value = initialValue;
	}

	override public function updateButton() {
		this.text.text = '${this.value ? 1 : 0}';
		this.bg.color.setColor(this.value ? 0xFF444444 : 0xFFAAAAAA);
	}
}

class CellularControl extends h2d.Layers {
	var cellLogic: Array<Bool>;

	var labelText: h2d.Text;
	var size: Point2i;

	var controls: Array<Cell>;

	public function new(font: h2d.Font, label: String, cellLogic: Array<Bool>, size: Point2i = null) {
		super();
		Assert.assert(cellLogic.length == 9 || cellLogic.length == 6);
		this.cellLogic = cellLogic;

		this.labelText = new h2d.Text(font);
		this.labelText.text = label;
		this.labelText.x = -this.labelText.textWidth - 10;
		this.add(this.labelText, 0);
		var last: h2d.Object = null;

		for (i in 0...cellLogic.length) {
			var c = new Cell(font, cellLogic[i]);
			c.onLeftClick = function() {
				c.value = !c.value;
				cellLogic[i] = c.value;
			}
			this.addChild(c);
			if (last != null) c.putOnRight(last, [4, 0]);
			last = c;
		}
	}
}
