package ren.tools.mapgen;

import zf.WrappedValue;
import zf.Point2i;

using zf.h2d.ObjectExtensions;

class IntControl extends h2d.Layers {
	public var Padding: Int = 4;
	public var TextWidth: Int = 50;

	var intValue: WrappedValue<Int>;
	var text: h2d.Text;

	var labelText: h2d.Text;
	var addButton: h2d.Bitmap;
	var add5Button: h2d.Bitmap;
	var subtractButton: h2d.Bitmap;
	var subtract5Button: h2d.Bitmap;
	var size: Point2i;

	public function new(font: h2d.Font, label: String, intValue: WrappedValue<Int>, size: Point2i = null) {
		super();
		this.intValue = intValue;
		if (size == null) size = [32, 16];
		this.size = size;

		intValue.listen(function(o, n) {
			this.setValue(n);
		});

		this.labelText = new h2d.Text(font);
		this.labelText.text = label;
		this.labelText.x = -this.labelText.textWidth - 10;
		this.add(this.labelText, 0);

		var decrease5 = new h2d.Text(font);
		decrease5.text = '-5';
		decrease5.x = (size.x - decrease5.textWidth) / 2;
		decrease5.y = (size.y - decrease5.textHeight) / 2;

		this.subtract5Button = new h2d.Bitmap(h2d.Tile.fromColor(0x777777, size.x, size.y));
		this.subtract5Button.addChild(decrease5);
		this.subtract5Button.x = 0;
		this.add(this.subtract5Button, 0);
		var interactive = new h2d.Interactive(size.x, size.y, this.subtract5Button);
		interactive.onClick = function(e: hxd.Event) {
			this.intValue.value -= 5;
			setValue(this.intValue.value);
		};

		var decrease = new h2d.Text(font);
		decrease.text = '-1';
		decrease.x = (size.x - decrease.textWidth) / 2;
		decrease.y = (size.y - decrease.textHeight) / 2;

		this.subtractButton = new h2d.Bitmap(h2d.Tile.fromColor(0x777777, size.x, size.y));
		this.subtractButton.addChild(decrease);
		this.subtractButton.putOnRight(subtract5Button, [Padding, 0]);
		this.add(this.subtractButton, 0);
		var interactive = new h2d.Interactive(size.x, size.y, this.subtractButton);
		interactive.onClick = function(e: hxd.Event) {
			this.intValue.value -= 1;
			setValue(this.intValue.value);
		};

		this.text = new h2d.Text(font);
		this.text.text = '${intValue.value}';
		this.text.maxWidth = TextWidth;
		this.text.textAlign = Center;
		this.text.putOnRight(this.subtractButton, [2, 0]).setY(size.y, AlignCenter, -2);
		this.add(this.text, 0);

		var increase = new h2d.Text(font);
		increase.text = '+1';
		increase.x = (size.x - increase.textWidth) / 2;
		increase.y = (size.y - increase.textHeight) / 2;

		this.addButton = new h2d.Bitmap(h2d.Tile.fromColor(0x777777, size.x, size.y));
		this.addButton.addChild(increase);
		this.addButton.putOnRight(this.subtractButton, [TextWidth + Padding * 2, 0]);
		this.add(this.addButton, 0);
		var interactive = new h2d.Interactive(size.x, size.y, this.addButton);
		interactive.onClick = function(e: hxd.Event) {
			this.intValue.value += 1;
			setValue(this.intValue.value);
		};

		var increase5 = new h2d.Text(font);
		increase5.text = '+5';
		increase5.x = (size.x - increase5.textWidth) / 2;
		increase5.y = (size.y - increase5.textHeight) / 2;

		this.add5Button = new h2d.Bitmap(h2d.Tile.fromColor(0x777777, size.x, size.y));
		this.add5Button.addChild(increase5);
		this.add5Button.putOnRight(this.addButton, [Padding, 0]);
		this.add(this.add5Button, 0);
		var interactive = new h2d.Interactive(size.x, size.y, this.add5Button);
		interactive.onClick = function(e: hxd.Event) {
			this.intValue.value += 5;
			setValue(this.intValue.value);
		};
	}

	function setValue(v: Int) {
		this.text.text = '${this.intValue.value}';
	}
}
