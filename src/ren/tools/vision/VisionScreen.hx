package ren.tools.vision;

import zf.Point2i;
import zf.Assets.LoadedSpritesheet;
import zf.ds.Vector2D;

import ren.utils.Sight.VisionAlgo;
import ren.utils.Sight.Visibility;

class VisionScreen extends zf.Screen {
	var ss: LoadedSpritesheet;
	var characterPosition: Point2i;

	var sb: h2d.SpriteBatch;
	var map: Vector2D<h2d.SpriteBatch.BatchElement>;
	var mapString: Vector2D<String>;
	var visionMap: Vector2D<Int>;
	var SIZE: Point2i = [100, 100];

	var algo: VisionAlgo;
	var characterRange: Int = 5;
	var rangeText: h2d.Text;

	public var blockDiagonal: Bool = true;

	public function new(ss: LoadedSpritesheet,
			additionalAreas: Array<{pos: Point2i, str: Array<String>}> = null) {
		super();
		this.ss = ss;
		this.characterPosition = [0, 0];

		this.sb = new h2d.SpriteBatch(this.ss.tile);
		this.map = new Vector2D(SIZE, null);
		this.mapString = new Vector2D(SIZE, '.');
		this.visionMap = new Vector2D(SIZE, 0);
		for (xy => item in this.map.iterateYX()) {
			var be = this.sb.alloc(getTile('.'));
			be.x = xy.x * 8;
			be.y = xy.y * 8;
			this.map.set(xy.x, xy.y, be);
			this.updateVision(xy.x, xy.y, 0);
		}
		this.addChild(this.sb);

		inline function setGrid(x: Int, y: Int, string: Array<String>) {
			for (indY => line in string) {
				var posY = indY + y;
				for (indX in 0...line.length) {
					var char = line.charAt(indX);
					var posX = x + indX;
					this.mapString.set(posX, posY, char);
				}
			}
		}

		// draw line of wall
		for (x in 10...30) {
			// add a random gap in the wall
			if (x == 15) continue;
			this.mapString.set(x, 5, '#');
		}

		// add a random pillar
		this.mapString.set(15, 15, '#');

		setGrid(30, 10, ['#.', '.#',]);

		// draw 2 line of wall
		for (x in 10...30) {
			// add a random gap in the wall
			if (x == 15) continue;
			this.mapString.set(x, 15, '#');
			this.mapString.set(x, 17, '#');
		}

		setGrid(10, 40, [
			'######################',
			'......................',
			'#####.################',
			'..##..##...',
			'.##..#.##..'
		]);

		setGrid(10, 46, [
			'g##..gggggg..#.......#',
			'g#....ggggg..#.......#',
			'.............#rrrrrrr#',
			'g#....ggggg..###.#.###',
			'g##..gggggg..##.....##',
		]);

		if (additionalAreas != null) {
			for (d in additionalAreas) {
				var pos = d.pos;
				var str = d.str;
				setGrid(pos.x, pos.y, str);
			}
		}

		updateRendering();

		var font = hxd.res.DefaultFont.get().clone();
		font.resizeTo(16);
		this.rangeText = new h2d.Text(font);
		this.rangeText.x = 810;
		this.rangeText.y = 10;
		this.addChild(this.rangeText);
		updateText();
	}

	function updateText() {
		this.rangeText.text = 'Range: ${this.characterRange}';
	}

	public function setAlgo(algo: VisionAlgo) {
		this.algo = algo;
	}

	override public function onEvent(e: hxd.Event) {
		if (e.kind == hxd.Event.EventKind.EKeyDown) {
			switch (e.keyCode) {
				case hxd.Key.Q:
					moveCharacter(-1, -1);
				case hxd.Key.E:
					moveCharacter(1, -1);
				case hxd.Key.Z:
					moveCharacter(-1, 1);
				case hxd.Key.C:
					moveCharacter(1, 1);
				case hxd.Key.A:
					moveCharacter(-1, 0);
				case hxd.Key.W:
					moveCharacter(0, -1);
				case hxd.Key.D:
					moveCharacter(1, 0);
				case hxd.Key.S:
					moveCharacter(0, 1);
				case hxd.Key.NUMBER_1:
					if (this.characterRange >= 9) {
						updateVisionRange(this.characterRange - 1);
					} else {
						updateVisionRange(1);
					}
				case hxd.Key.NUMBER_2:
					updateVisionRange(2);
				case hxd.Key.NUMBER_3:
					updateVisionRange(3);
				case hxd.Key.NUMBER_4:
					updateVisionRange(4);
				case hxd.Key.NUMBER_5:
					updateVisionRange(5);
				case hxd.Key.NUMBER_6:
					updateVisionRange(6);
				case hxd.Key.NUMBER_7:
					updateVisionRange(7);
				case hxd.Key.NUMBER_8:
					updateVisionRange(8);
				case hxd.Key.NUMBER_9:
					if (this.characterRange >= 9) {
						updateVisionRange(this.characterRange + 1);
					} else {
						updateVisionRange(9);
					}
			}
		} else if (e.kind == hxd.Event.EventKind.EPush) {
			var x = Std.int(e.relX / 8);
			var y = Std.int(e.relY / 8);
			var c = this.mapString.get(x, y);
			if (c == null) return;
			var nc = c == "#" ? "." : "#";
			this.mapString.set(x, y, nc);
			updateRendering([x, y]);
			updateVisionMap();
		}
	}

	function moveCharacter(x: Int, y: Int) {
		var oldPosition = this.characterPosition.clone();
		var newPosition: Point2i = this.characterPosition + [x, y];
		this.SIZE.boundPoint(newPosition, true);
		resetVisionMap();
		this.characterPosition.update(newPosition);
		updateRendering(oldPosition);
		updateRendering(newPosition);
		updateVisionMap();
	}

	function getTile(s: String): h2d.Tile {
		return this.ss.assets['${s}'].getTile();
	}

	function resetVisionMap() {
		for (x in -characterRange...characterRange + 1) {
			for (y in -characterRange...characterRange + 1) {
				updateVision(x + characterPosition.x, y + characterPosition.y, 0);
			}
		}
	}

	function updateVisionMap() {
		if (this.algo == null) return;
		var vm = this.algo(this.characterPosition, this.characterRange,
			function(x, y, d: zf.Direction): Visibility {
				var t = this.mapString.get(x, y);
				if (t == null) return OutOfBound;
				if (this.blockDiagonal) {
					var pt: Point2i = d; // convert to point
					var t2 = this.mapString.get(x - pt.x, y);
					var t3 = this.mapString.get(x, y - pt.y);
					if (t2 == "#" && t3 == "#") return Invisible | BlockVision;
				}
				if (t == '#' || t == "g") return Visible | BlockVision;
				return Visible;
			}, null);

		for (xy => value in vm.iterateYX()) {
			var x = xy.x + characterPosition.x - characterRange;
			var y = xy.y + characterPosition.y - characterRange;
			this.updateVision(x, y, value);
		}
	}

	function updateVisionRange(range: Int) {
		resetVisionMap();
		this.characterRange = range;
		updateVisionMap();
		updateText();
	}

	function updateRendering(pt: Point2i = null) {
		if (pt == null) {
			for (xy => item in this.mapString.iterateYX()) {
				var be = this.map.get(xy.x, xy.y);
				if (xy.x == this.characterPosition.x && xy.y == this.characterPosition.y) {
					var t = getTile('@');
					be.t = t;
				} else {
					var t = getTile(item);
					be.t = t;
				}
			}
		} else {
			var be = this.map.get(pt.x, pt.y);
			if (pt == this.characterPosition) {
				var t = getTile('@');
				be.t = t;
			} else {
				var s = this.mapString.get(pt.x, pt.y);
				var t = getTile(s);
				be.t = t;
			}
		}
	}

	function updateVision(x: Int, y: Int, value: Int) {
		this.visionMap.set(x, y, value);
		var t = this.map.get(x, y);
		if (t == null) return;
		t.a = (value * 1.0 / 10 * 0.8) + 0.3; // bound it between 0.5 to 1.0
	}
}
