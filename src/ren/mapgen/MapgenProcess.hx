package ren.mapgen;

import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;
import zf.Logger;
import zf.Point2i;
import zf.Recti;

/**
	A parent algorithm class to generate a level.
**/
class MapgenProcess {
	/**
		Mainly used for display
	**/
	public var confPrefix: String = "";

	public var name: String;

	public var state: String;

	public var map: MapgenMap;

	public var debug: Bool = false;

	public function new(map: MapgenMap) {
		this.map = map;
		this.name = "";
		this.state = "";
	}

	public function init(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_debug'] != null) this.debug = configuration['${confPrefix}_debug'];
	}

	/**
		Step the process by 1.
	**/
	public function step() {}

	/**

		@return true if there are any more step, false otherwise
	**/
	public function canStep(): Bool {
		return false;
	}

	/**
		Child class should implement a better finish if stepping is slow
	**/
	public function finish() {
		while (canStep()) {
			step();
		}
	}

	/**
		called when the process finishes
	**/
	public function onFinish() {}

	/**
		this is called after onFinish, for external cleanup
	**/
	dynamic public function onProcessFinish() {}
}
