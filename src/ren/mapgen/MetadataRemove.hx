package ren.mapgen;

class MetadataRemove extends FunctionProcess {
	public var toRemove: Array<String>;

	public function new(map: MapgenMap, toRemove: Array<String>) {
		super(map, "Metadata Remove");
		this.toRemove = toRemove;
	}

	override public function func(map: MapgenMap) {
		for (key in this.toRemove) {
			this.map.metadata.remove(key);
		}
	}

	public static function removeKeys(keys: Array<String>, map: MapgenMap): MapgenProcess {
		return new MetadataRemove(map, keys);
	}
}
