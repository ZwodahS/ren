package ren.mapgen.maze;

import zf.Direction;
import zf.ds.Vector2D;
import zf.Point2i;

/**
	A process used to clean deadends

	Deadend are defined by that 3 out of 4 of the direction is not passable.

	Mon Jun 14 15:59:10 2021
	Currently this does not check all 8 directions, it only checks the 4 adjacent space
**/
class DeadendCleaner extends MapgenProcess {
	var maybeDeadends: List<Point2i>;

	/**
		chance to clean the dead end

		default chance is 100%
	**/
	public var cleanProbability: Int = 100;

	public var replaceSymbol: String = "#";

	public var passableSymbols: Array<String>;

	public function new(map: MapgenMap) {
		super(map);
		this.maybeDeadends = new List<Point2i>();
		this.passableSymbols = ['.'];
		this.name = "Deadend Cleaner";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration["deadendcleaner_probability"] != null) {
			this.cleanProbability = configuration["deadendcleaner_probability"];
		}

		final cells = map.cells;
		for (xy => str in cells.iterate()) {
			if (str != '.') continue;
			if (getAdjacentSpaces([xy.x, xy.y]).length <= 1) {
				this.maybeDeadends.add([xy.x, xy.y]);
			}
		}
	}

	override public function canStep(): Bool {
		return this.maybeDeadends.length != 0;
	}

	override public function step() {
		if (!this.canStep()) return;
		while (this.canStep()) {
			var p = this.maybeDeadends.pop();
			this.state = '${this.maybeDeadends.length}';
			var spaces = getAdjacentSpaces(p);
			if (spaces.length > 1) continue;
			// check probability
			if (this.cleanProbability < 100 && !this.map.r.randomChance(this.cleanProbability)) continue;
			// add the affected spaces to the next check
			for (s in spaces) maybeDeadends.push(s);
			// set it to the replace symbol
			this.map.cells.set(p.x, p.y, this.replaceSymbol);
			return;
		}
	}

	function getAdjacentSpaces(point: Point2i): Array<Point2i> {
		var points: Array<Point2i> = [];
		for (d in Direction.allFourDirections()) {
			var p = point + d;
			if (isPassable(this.map.cells.get(p.x, p.y))) points.push(p);
		}
		return points;
	}

	dynamic public function isPassable(str: String): Bool {
		return this.passableSymbols.contains(str);
	}
}
