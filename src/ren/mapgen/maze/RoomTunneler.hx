package ren.mapgen.maze;

import zf.Direction;
import zf.Point2i;
import zf.Recti;
import zf.ds.Vector2D;

using zf.ds.ArrayExtensions;
using zf.RandExtensions;
using zf.MathExtensions;

enum TunnelOrder {
	Random;
	Stack;
	Queue;
}

@:structInit private class Room {
	public var id: Int;
	public var rect: Recti;
	public var availableExits: Array<Direction>;
}

/**
	# Overview
	Starting from a room, tunnel out from the room and create new tunnel to another room.
	Repeat this N times to create up to N number of rooms or until no rooms can be created.
	If starting room is not provided, then a random starting room will be placed on the map.

	# Usage Examples
	There are a few ways to use this.

	## Normal
	Setting up min/max rooms, we can create rooms that are guaranteed to be connected.
	## Tunnel only
	By setting the room size to 1, this will be able to create tunnels only.

	# Configurations
	- minDistance(Int, default: 3): minimum distance between room
	- maxDistance(Int, default: 15): maximum distance between room
	- maxRoom(Int, default: -1): maximum number of rooms to placed. -1 will exhaust until no room can be placed.
	- minRoomSize(Int, default: 1): the minimum size of room to generate
	- maxRoomSize(Int, default: 1): the maximum size of room to generate
	- sharedSizeVarianceMin/sharedSizeVarianceMax(Int, default: -1):
			when provided the size is randomized using a shared pool
	- tunnelSymbol(String, default: "."): the symbol used when tunneling
	- roomSymbol(String, default: "r"): the symbol used when placing room
	- borderSymbol(String, default: null): if provided border will be placed around the room
	- borderSize(Int, 1): if borderSymbol is provided, this decide the size of the border.
	- allowTunnelCrossing(Bool, false): if true, tunnel will cross other tunnels
	- startingRoom(Recti, null): if provided this will the starting room
	- tunnelOrder(TunnelOrder, Random): the order which rooms are chosen

	## Dynamic Functions
	- canPlaceRoomCell
	- canTunnel
	- getRoomSize

	Usually there is no need to modify them.

	# Output
	- rooms(Map<Int, Room>): the generated rooms
	- roomsList(List<Room>): the generated rooms as List

**/
class RoomTunneler extends MapgenProcess {
	/** Input **/
	/**
		min distance between the room
	**/
	public var minDistance: Int = 3;

	/**
		max distance between the romo
	**/
	public var maxDistance: Int = 15;

	/**
		Maximum number to place. If set to -1, it will place until there is no where to tunnel
	**/
	public var maxRoom: Int = -1;

	/**
		Minimum size of the room generated
	**/
	public var minRoomSize: Int = 1;

	/**
		Maximum size of the room generated
		if the value of max is lesser than min, it will be set to min
	**/
	public var maxRoomSize: Int = 1;

	/**
		if none of sharedSizeVariance are provided, then the size will be randomed between
		minRoomSize - maxRoomSize for both dimension

		if only min is provided, then the variance will be set to min
		if only max is provided, then the variance will be set to max
		if both are provided, then a variance will be randomed between min and max

		After the variance is set, then the amount will be randomly spread to both dimension

		if variance > ((maxRoomSize - minRoomSize) * 2) then it will have the same effect as setting it as -1
	**/
	public var sharedSizeVarianceMin: Int = -1;

	public var sharedSizeVarianceMax: Int = -1;

	/**
		The value used for tunneling.
	**/
	public var tunnelSymbol: String = ".";

	/**
		The value placed for rooms
	**/
	public var roomSymbol: String = "r";

	/**
		The border symbol placed around the room
	**/
	public var borderSymbol: String = null;

	/**
		The size of the border
	**/
	public var borderSize: Int = 1;

	public var allowedSymbols: Array<String> = null;

	/**
		If tunneler can cross tunnel
	**/
	public var allowTunnelCrossing: Bool = false;

	public var startingRoom: Recti;

	/**
		The order which room are selected.

		- Random: the next room to process is chosen at random
		- Stack: the next room to process is taken from the top of the stack, i.e. last added room is chosen first
		- Queue: the next room to process is taken from the start of the queue, i.e. the earlier room are chosen first
	**/
	public var tunnelOrder: TunnelOrder = Random;

	/** Process dataa **/
	var roomBound: Recti;

	var toProcess: Array<Room>;

	/** Output **/
	/**
		Generated rooms
	**/
	public var rooms: Map<Int, Room>;

	/**
		Generated rooms as list
	**/
	public var roomsList: List<Room>;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Random Tunneling";
	}

	inline function initConf(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_minDistance'] != null) {
			this.minDistance = configuration['${confPrefix}_minDistance'];
		}
		if (configuration['${confPrefix}_maxDistance'] != null) {
			this.maxDistance = configuration['${confPrefix}_maxDistance'];
		}
		if (configuration['${confPrefix}_maxRoom'] != null) {
			this.maxRoom = configuration['${confPrefix}_maxRoom'];
		}
		if (configuration['${confPrefix}_minRoomSize'] != null) {
			this.minRoomSize = configuration['${confPrefix}_minRoomSize'];
		}
		if (configuration['${confPrefix}_maxRoomSize'] != null) {
			this.maxRoomSize = configuration['${confPrefix}_maxRoomSize'];
		}
		if (configuration['${confPrefix}_sharedSizeVarianceMin'] != null) {
			this.sharedSizeVarianceMin = configuration['${confPrefix}_sharedSizeVarianceMin'];
		}
		if (configuration['${confPrefix}_sharedSizeVarianceMax'] != null) {
			this.sharedSizeVarianceMax = configuration['${confPrefix}_sharedSizeVarianceMax'];
		}
		if (configuration['${confPrefix}_allowTunnelCrossing'] != null) {
			this.allowTunnelCrossing = configuration['${confPrefix}_allowTunnelCrossing'];
		}
		if (this.maxRoomSize < this.minRoomSize) this.maxRoomSize = this.minRoomSize;
	}

	function initStartingConditions() {
		this.rooms = new Map<Int, Room>();
		this.roomsList = new List<Room>();
		this.toProcess = [];
		this.roomBound = [0, 0, this.map.cells.size.x - 1, this.map.cells.size.y - 1,];

		var startingRoom: Recti = null;
		if (this.startingRoom == null) this.startingRoom = getStartingRoom();
		if (this.startingRoom == null) {
			this.map.error = "Fail to place start location";
			return;
		}

		var room = constructRoom(this.startingRoom);
		placeRoom(room);
		this.toProcess.push(room);
	}

	function getStartingRoom(): Recti {
		var size = getRoomSize(this.map.r);
		var tries = 0;
		final cells = this.map.cells;
		var startRoom: Point2i = null;
		while (tries < 10) {
			var startX = this.map.r.randomInt(cells.size.x - size.x);
			var startY = this.map.r.randomInt(cells.size.y - size.y);
			startRoom = [startX, startY];
			tries += 1;
			var rect = constructRoomRect(startRoom, size);
			if (canPlaceRoom(rect)) break;
			startRoom = null;
		}
		if (startRoom == null) return null;

		var r = constructRoomRect(startRoom, size);
		return r;
	}

	override public function init(configuration: Map<String, Dynamic>) {
		initConf(configuration);
		initStartingConditions();
	}

	function constructRoom(rect: Recti, excludeDirection: Direction = null): Room {
		var exits = [North, South, East, West];
		if (excludeDirection != null) exits.remove(excludeDirection);

		var r: Room = {
			id: roomsList.length,
			rect: rect,
			availableExits: exits,
		}
		this.rooms[r.id] = r;
		this.roomsList.add(r);
		return r;
	}

	function constructRoomRect(center: Point2i, size: Point2i): Recti {
		var left = center.x - Std.int((size.x - 1) / 2);
		var top = center.y - Std.int((size.y - 1) / 2);
		var right = left + size.x - 1;
		var bottom = top + size.y - 1;
		var rect = new Recti(left, top, right, bottom);
		return rect;
	}

	function placeRoom(room: Room) {
		final cells = this.map.cells;

		inline function setBorder(x, y) {
			var v = cells.get(x, y);
			if (v == this.tunnelSymbol || v == this.roomSymbol) return;
			if (this.allowedSymbols != null && !this.allowedSymbols.contains(v)) return;
			cells.set(x, y, this.borderSymbol);
		}

		for (x in room.rect.xMin...room.rect.xMax + 1) {
			for (y in room.rect.yMin...room.rect.yMax + 1) {
				cells.set(x, y, roomSymbol);
			}
		}
		if (this.borderSymbol != null) {
			var r = room.rect.clone();
			r.expand(this.borderSize);
			for (x in r.xMin...r.xMax + 1) {
				for (y in 0...this.borderSize) {
					setBorder(x, r.yMin + y);
					setBorder(x, r.yMax - y);
				}
			}

			for (y in r.yMin...r.yMax + 1) {
				for (x in 0...this.borderSize) {
					setBorder(r.xMin + x, y);
					setBorder(r.xMax - x, y);
				}
			}
		}
	}

	override public function canStep(): Bool {
		return ((this.maxRoom == -1 || this.roomsList.length < this.maxRoom) && toProcess.length > 0);
	}

	// ensure that no room is touching each other
	function canPlaceRoom(rect: Recti): Bool {
		var r: Recti = [rect.xMin - 1, rect.yMin - 1, rect.xMax + 1, rect.yMax + 1,];
		if (this.borderSymbol != null) r.expand(this.borderSize);

		for (y in r.yMin...r.yMax + 1) {
			for (x in r.xMin...r.xMax + 1) {
				if (!this.roomBound.contains(x, y)) return false;
				if (!canPlaceRoomCell(x, y)) return false;
			}
		}
		return true;
	}

	dynamic public function canPlaceRoomCell(x: Int, y: Int): Bool {
		var v = this.map.cells.get(x, y);
		if (this.allowedSymbols != null && !this.allowedSymbols.contains(v)) {
			return false;
		}
		return v != this.roomSymbol && v != this.tunnelSymbol;
	}

	dynamic public function canTunnel(x: Int, y: Int, direction: Direction): Bool {
		final cells = this.map.cells;

		inline function isAllowed(v: String): Bool {
			if (v == this.roomSymbol) return false;
			if (this.allowedSymbols != null && !this.allowedSymbols.contains(v)) {
				return false;
			}
			if (!this.allowTunnelCrossing && v == this.tunnelSymbol) return false;
			return true;
		}

		var v = cells.get(x, y);
		if (!isAllowed(v)) return false;

		// check adjacents
		var adj: Point2i = direction.rotateCW(2);
		v = cells.get(x + adj.x, y + adj.y);
		if (!isAllowed(v)) return false;

		var adj: Point2i = direction.rotateCCW(2);
		v = cells.get(x + adj.x, y + adj.y);
		if (!isAllowed(v)) return false;

		return true;
	}

	/**
		Calculate and get a random room size
	**/
	dynamic public function getRoomSize(r: hxd.Rand): Point2i {
		var variance = -1;
		if (this.sharedSizeVarianceMin != -1 && this.sharedSizeVarianceMax != -1) {
			variance = r.randomWithinRange(this.sharedSizeVarianceMin, this.sharedSizeVarianceMax);
		} else if (this.sharedSizeVarianceMin != -1) {
			variance = this.sharedSizeVarianceMin;
		} else if (this.sharedSizeVarianceMax != -1) {
			variance = this.sharedSizeVarianceMax;
		}

		if (variance == -1) {
			// by default we will random the size between min and max
			var xSize = r.randomWithinRange(minRoomSize, maxRoomSize);
			var ySize = r.randomWithinRange(minRoomSize, maxRoomSize);
			return [xSize, ySize];
		} else {
			var varianceX = r.randomInt(Math.clampI(variance, 0, maxRoomSize - minRoomSize) + 1);
			var varianceY = Math.clampI(variance - varianceX, 0, maxRoomSize - minRoomSize);
			return [minRoomSize + varianceX, minRoomSize + varianceY];
		}
	}

	override public function step() {
		while (this.canStep() && !_step()) {}
	}

	function _step() {
		final r = this.map.r;
		final cells = this.map.cells;
		this.state = '${toProcess.length} Rooms to process';

		var room: Room = null;
		switch (this.tunnelOrder) {
			case Random:
				room = this.toProcess.randomItem(this.map.r);
			case Stack:
				room = this.toProcess.last();
			case Queue:
				room = this.toProcess.first();
			default:
				room = this.toProcess.randomItem(this.map.r);
		}
		if (room.availableExits.length == 0) {
			toProcess.remove(room);
			return false;
		}
		var size = this.getRoomSize(this.map.r);

		var direction = room.availableExits.randomItem(this.map.r, true);

		// store all the available places that we are placing rect
		var availableRects: Array<Recti> = [];

		var minDistance = this.minDistance;
		var maxDistance = this.maxDistance;

		// construct the initial rect
		var rect: Recti = [0, 0, size.x - 1, size.y - 1];
		var curr = room.rect;

		/**
			Assume direction is north, we will place it like this, X = tunnel, O = new

			OOOOO            OOOOO
			OOOOO            OOOOO
					X						 X
					X            X

			we will try different room values at each y index, such that the xMin will be bounded
			by the tunnel positions based on the following

			curr.xMin =< tunnel.x <= curr.xMax

		**/

		var tunnel: Point2i = [0, 0];

		// need to handle scanning up/down and scanning left/right differently
		// start scanning for available rooms that I can place
		if (direction == North || direction == South) {
			// choose a random x position to tunnel out from curr
			tunnel.x = r.randomWithinRange(curr.xMin, curr.xMax);
			// store delta, either -1 or +1 depends on direction
			var delta = 0;
			// then put the rect just away from the tunnel tile in the correct direction
			if (direction == North) {
				rect.bottom = curr.top - 2;
				delta = -1;
				tunnel.y = curr.top - 1;
			} else {
				rect.top = curr.bottom + 2;
				delta = 1;
				tunnel.y = curr.bottom + 1;
			}
			// calculate the xStart/xEnd to scan based on the calculation above
			var xStart = tunnel.x - rect.xDiff;
			var xEnd = tunnel.x;
			// start tunneling, at each point calculate the possible placement
			var d = 1;
			while (d <= maxDistance) {
				// ensure that this spot can be tunneled, if we can't tunnel, then we will just terminate since there is
				// no point going forward
				if (!canTunnel(tunnel.x, tunnel.y, direction)) break;
				// if we are already at the minDistance, then we scan for possible room placement
				if (d >= minDistance) {
					// scan all the x values to see if we can place a rect
					for (x in xStart...xEnd + 1) {
						rect.left = x;
						if (canPlaceRoom(rect)) availableRects.push(rect.clone());
					}
				}
				// shift both rect and tunnel
				rect.top += delta;
				tunnel.y += delta;
				// increase the distance
				d += 1;
			}
		} else if (direction == East || direction == West) {
			// choose a random y position to tunnel out from curr
			tunnel.y = r.randomWithinRange(curr.yMin, curr.yMax);
			// store delta, either -1 or +1 depends on direction
			var delta = 0;
			// put the tunnel spot just outside of the current room
			// then put the rect just away from the tunnel tile in the correct direction
			if (direction == West) {
				rect.right = curr.left - 2;
				delta = -1;
				tunnel.x = curr.left - 1;
			} else {
				rect.left = curr.right + 2;
				delta = 1;
				tunnel.x = curr.right + 1;
			}
			// calculate the xStart/xEnd to scan based on the calculation above
			var yStart = tunnel.y - rect.yDiff;
			var yEnd = tunnel.y;
			// start tunneling, at each point calculate the possible placement
			var d = 1;
			while (d <= maxDistance) {
				// ensure that this spot can be tunneled, if we can't tunnel, then we will just terminate since there is
				// no point going forward
				if (!canTunnel(tunnel.x, tunnel.y, direction)) break;
				// if we are already at the minDistance, then we scan for possible room placement
				if (d >= minDistance) {
					// scan all the y values to see if we can place a rect
					for (y in yStart...yEnd + 1) {
						rect.top = y;
						if (canPlaceRoom(rect)) availableRects.push(rect.clone());
					}
				}
				// shift both rect and tunnel
				rect.left += delta;
				tunnel.x += delta;
				// increase the distance
				d += 1;
			}
		} else {
			return false;
		}
		if (availableRects.length == 0) return false;
		// choose the rect to place
		var rect = availableRects.randomItem(this.map.r);
		var newRoom = constructRoom(rect, direction.opposite);
		placeRoom(newRoom);
		this.toProcess.push(newRoom);

		inline function setTunnel(x: Int, y: Int) {
			if (cells.get(x, y) == roomSymbol) return;
			cells.set(x, y, tunnelSymbol);
		}
		// tunnel from current room to new room
		var tunnelEnd: Point2i = tunnel.clone();
		switch (direction) {
			case North:
				tunnel.y = curr.top - 1;
				tunnelEnd.y = rect.bottom;
			case South:
				tunnel.y = curr.bottom + 1;
				tunnelEnd.y = rect.top;
			case East:
				tunnel.x = curr.right + 1;
				tunnelEnd.x = rect.left;
			case West:
				tunnel.x = curr.left - 1;
				tunnelEnd.x = rect.right;
			default:
		}
		while (tunnel != tunnelEnd) {
			setTunnel(tunnel.x, tunnel.y);
			tunnel += direction;
		}
		return true;
	}
}
