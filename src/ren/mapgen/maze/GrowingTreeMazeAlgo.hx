package ren.mapgen.maze;

import zf.Direction;
import zf.Point2i;
import zf.ds.Vector2D;

/**
	Implementation for growing tree maze generation

	http://www.astrolog.org/labyrnth/algrithm.htm

	TODO:
	1. Provide different selection criteria instead of always using activeList.first()

	Configuration

	- configuration.growingtree_increasedChanceForStraightLine - increased weightage when selecting direction

	Output

	- metadata.mazes: Array of List of Point2i for each region of the maze generated.
**/
class GrowingTreeMazeAlgo extends MapgenProcess {
	/** Configuration **/
	// how many more "chance" that when growing that it will be a straight line
	var increasedChanceForStraightLine: Int = 0;

	/** Process state data **/
	var activeList: List<Point2i>;

	var toTunnel: List<Point2i>;

	public var allowedSymbols: Array<String>;

	// this stores a list of points that are joined together, disjointed from each other.
	// in the case where this algo is ran first, then the whole maze would be a single region.
	var regions: Array<List<Point2i>>;

	var currentRegion: List<Point2i>;

	public var breakTileSymbol: String = ".";

	public function new(map: MapgenMap) {
		super(map);
		this.activeList = new List<Point2i>();
		this.allowedSymbols = ["#"];

		this.name = "Growing Tree Algo";
		this.confPrefix = "growingtree";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		this.toTunnel = new List<Point2i>();

		if (configuration['${confPrefix}_increasedChanceForStraightLine'] != null) {
			this.increasedChanceForStraightLine = configuration['${confPrefix}_increasedChanceForStraightLine'];
		}

		final cells = this.map.cells;
		for (x in 0...cells.size.x) {
			for (y in 0...cells.size.y) {
				if (x % 2 == 1 || y % 2 == 1) continue;
			}
		}

		for (x in 0...cells.size.x) {
			for (y in 0...cells.size.y) {
				if (x % 2 == 1 || y % 2 == 1) continue;
				if (this.allowedSymbols.contains(cells.get(x, y))) {
					this.toTunnel.push([x, y]);
				}
			}
		}

		toTunnel.shuffle(this.map.r);

		this.regions = [];
		this.map.metadata["mazes"] = this.regions;
	}

	override public function canStep(): Bool {
		if (activeList.length == 0 && this.toTunnel.length == 0) return false;
		return true;
	}

	override public function step() {
		if (!this.canStep()) return;

		final cells = this.map.cells;

		// if activeList is null, we need to start a new "region"
		if (activeList.length == 0) {
			// add the current regions to room if any
			if (this.currentRegion != null && this.currentRegion.length > 0) {
				this.regions.push(this.currentRegion);
				this.currentRegion = null;
			}

			this.currentRegion = new List<Point2i>();
			Logger.debug('New Tunnel Region');
			while (toTunnel.length > 0) {
				var p = toTunnel.pop();
				var s = cells.get(p.x, p.y);
				if (s == this.breakTileSymbol) continue;
				activeList.add(p);
				break;
			}
			// do a check again, specifically for activeList
			if (activeList.length == 0) return;
		}
		this.state = '${this.toTunnel.length}/${this.activeList.length}';

		while (activeList.length > 0) {
			var curr = activeList.first();
			if (cells.get(curr.x,
				curr.y) != this.breakTileSymbol) cells.set(curr.x, curr.y, this.breakTileSymbol);
			// check if this curr position have any space to move.
			var directions = getDiggable(curr);
			if (directions.length == 0) {
				activeList.pop();
				continue;
			}
			var direction = directions[this.map.r.randomInt(directions.length)];
			var points = tunnel(curr, direction);
			for (p in points) this.currentRegion.add(p);
			return;
		}
	}

	function getDiggable(point: Point2i): Array<Direction> {
		var directions: Array<Direction> = [];
		final cells = this.map.cells;
		// test all 4 direction
		for (d in Direction.allFourDirections()) {
			var p = point + d;
			var s = cells.get(p.x, p.y);
			// if the next stop is not breakable, we don't tunnel
			if (s != '#') continue;
			// get the next grid after the wall
			p = p + d;
			s = cells.get(p.x, p.y);
			// if the block after that is not a wall, then we don't tunnel
			if (s != '#') continue;
			// valid position
			directions.push(d);
			if (this.increasedChanceForStraightLine > 0) {
				var p = point + d.opposite;
				var s = cells.get(p.x, p.y);
				if (s != "#") {
					for (i in 0...this.increasedChanceForStraightLine) {
						directions.push(d);
					}
				}
			}
		}
		return directions;
	}

	/**
		Tunnel return the points that are changed.
	**/
	function tunnel(point: Point2i, direction: Direction): Array<Point2i> {
		final cells = this.map.cells;
		var tunneled: Array<Point2i> = [];
		var p = point + direction;
		cells.set(p.x, p.y, this.breakTileSymbol);
		tunneled.push(p.copy());
		p += direction;
		cells.set(p.x, p.y, this.breakTileSymbol);
		this.activeList.push(p);
		tunneled.push(p.copy());
		return tunneled;
	}

	override public function onFinish() {
		if (this.currentRegion != null && this.currentRegion.length > 0) {
			this.regions.push(this.currentRegion);
			this.currentRegion = null;
		}
	}
}
