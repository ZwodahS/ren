package ren.mapgen;

import zf.Point2i;
import zf.Recti;

private enum Mode {
	All;
	Border;
	Positions;
}

/**
	A simple process that reset all symbol in a region to a specified symbol

	If positions is specified, then we only set to those positions
**/
class Reset extends FunctionProcess {
	public var resetSymbol: String;

	public var flipProbability: Int = 100;
	public var resetPositions: Array<Point2i>;
	public var resetRect: Recti;

	var mode: Mode;

	public var append: Bool = false;

	function new(map: MapgenMap, resetSymbol: String) {
		super(map, "Reset");
		this.resetSymbol = resetSymbol;
		this.mode = All;
	}

	override public function func(map: MapgenMap) {
		final cells = map.cells;
		if (mode == All) {
			if (this.resetRect == null) {
				for (y in 0...cells.size.y) {
					for (x in 0...cells.size.x) {
						setCell(x, y);
					}
				}
			} else {
				for (y in this.resetRect.top...this.resetRect.bottom + 1) {
					for (x in this.resetRect.left...this.resetRect.right + 1) {
						setCell(x, y);
					}
				}
			}
		} else if (mode == Border) {
			var xMax = cells.size.x - 1;
			for (y in 0...cells.size.y) {
				setCell(0, y);
				setCell(xMax, y);
			}
			var yMax = cells.size.y - 1;
			for (x in 0...cells.size.x) {
				setCell(x, 0);
				setCell(x, yMax);
			}
		} else if (mode == Positions) {
			Assert.assert(this.resetPositions != null);
			for (p in this.resetPositions) {
				setCell(p.x, p.y);
			}
		}
	}

	inline function setCell(x: Int, y: Int) {
		final cells = this.map.cells;
		if (!match(cells.get(x, y))) return;
		if (this.flipProbability < 100 && !this.map.r.randomChance(this.flipProbability)) return;
		if (this.append) {
			var v = cells.get(x, y);
			cells.set(x, y, '${v}:${this.resetSymbol}');
		} else {
			cells.set(x, y, this.resetSymbol);
		}
	}

	dynamic public function match(v: String): Bool {
		return true;
	}

	public static function clearAll(resetSymbol: String, map: MapgenMap, rect: Recti = null) {
		var r = new Reset(map, resetSymbol);
		if (rect != null) r.resetRect = rect;
		return r;
	}

	public static function clearAllIf(resetSymbol: String, ifFunc: String->Bool, map: MapgenMap) {
		var r = new Reset(map, resetSymbol);
		r.match = ifFunc;
		return r;
	}

	public static function clearPositions(resetSymbol: String, positions: Array<Point2i>, map: MapgenMap) {
		var p = new Reset(map, resetSymbol);
		p.mode = Positions;
		p.resetPositions = positions;
		return p;
	}

	public static function clearBorder(resetSymbol: String, map: MapgenMap) {
		var p = new Reset(map, resetSymbol);
		p.mode = Border;
		return p;
	}

	public static function clearBorderIf(resetSymbol: String, ifFunc: String->Bool, map: MapgenMap) {
		var p = new Reset(map, resetSymbol);
		p.mode = Border;
		p.match = ifFunc;
		return p;
	}
}
