package ren.mapgen;

import zf.Recti;
import zf.Point2i;
import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;

class MapgenMap {
	/**
		Cells representing the map
	**/
	public var cells: Vector2DRegion<String>;

	/**
		Metadata for each cells
	**/
	public var cellsMetadata: Vector2DRegion<Map<String, Dynamic>>;

	/**
		Metadata created due to mapgen
	**/
	public var metadata: Map<String, Dynamic>;

	/**
		Rand created by using the seed
	**/
	public var r: hxd.Rand;

	/**
		The seed for this map
	**/
	public var seed: Int;

	public var parent: MapgenMap;

	public var error: String = null; // if error is not null, means that the process should stop

	function new() {}

	public static function makeNew(size: Point2i, seed: Int) {
		var map = new MapgenMap();
		map.cells = new Vector2D<String>(size, ".").getRegion();
		map.cellsMetadata = new Vector2D<Map<String, Dynamic>>(size, null).getRegion();
		for (pt => v in map.cellsMetadata.iterate()) {
			map.cellsMetadata.set(pt.x, pt.y, new Map<String, Dynamic>());
		}
		map.metadata = new Map<String, Dynamic>();
		map.r = new hxd.Rand(seed);
		map.seed = seed;
		return map;
	}

	/**
	**/
	public function subRegion(xMin: Int, yMin: Int, width: Int, height: Int): MapgenMap {
		var map = new MapgenMap();
		map.cells = this.cells.subRegion(xMin, yMin, width, height);
		map.cellsMetadata = this.cellsMetadata.subRegion(xMin, yMin, width, height);
		map.metadata = new Map<String, Dynamic>();
		map.r = this.r;
		map.seed = this.seed;
		map.parent = this;
		return map;
	}
}
