package ren.mapgen;

import zf.Point2i;
import zf.ds.Vector2DRegion;

/**
	A simple process that replaces on symbol to another
**/
class SymbolReplacer extends MapgenProcess {
	var mappings: Map<String, String>;

	// chance to replace the symbol
	public var probability: Int = 100;

	public var points: Array<Point2i> = null;

	var done: Bool = false;

	public function new(map: MapgenMap, mappings: Map<String, String>) {
		super(map);
		this.mappings = mappings;
		this.name = "Symbol Replacer";
		this.confPrefix = "sr";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_probability'] != null) {
			this.probability = configuration['${confPrefix}_probability'];
		}
	}

	override public function canStep(): Bool {
		return !done;
	}

	override public function step() {
		if (!canStep()) return;
		final cells = this.map.cells;
		inline function processCell(key: Point2i, value: String) {
			if (this.mappings[value] == null) return;
			if (this.probability < 100 && !this.map.r.randomChance(this.probability)) return;
			cells.set(key.x, key.y, this.mappings[value]);
		}
		if (this.points == null) {
			for (key => value in this.map.cells.iterate()) {
				processCell(key, value);
			}
		} else {
			for (pt in this.points) {
				var value = cells.get(pt.x, pt.y);
				processCell(pt, value);
			}
		}
		done = true;
	}

	public static function replace(mappings: Map<String, String>, map: MapgenMap) {
		var replacer = new SymbolReplacer(map, mappings);
		replacer.init(new Map<String, Dynamic>());
		replacer.finish();
		replacer.onFinish();
	}

	public static function makeReplacePoints(mappings: Map<String, String>, points: Array<Point2i>,
			map: MapgenMap) {
		var replacer = new SymbolReplacer(map, mappings);
		replacer.points = points;
		return replacer;
	}

	public static function make(mappings: Map<String, String>, map: MapgenMap): MapgenProcess {
		return new SymbolReplacer(map, mappings);
	}
}
