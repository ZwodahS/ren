package ren.mapgen;

import zf.Direction;
import zf.Point2i;
import zf.Recti;
import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;

/**
	This is an extension to various object.
	This should be used via `using ren.mapgen.Utils`;
**/
class Utils {
	/**
		Get valid tunnel directions from point to symbols

		@param startPosition the position to tunnel from
		@param findSymbols array of symbols to tunnel to
		@param configuration other configurations used to configure the functions

		- configuration.directions: if provided will limit the test to these direction
		- configuration.allowedSymbols: if provided, only these symbol will be allowed when tunneling
		- configuration.minDistance: if provided, ensure that the findSymbols is at least minDistance away
		- configuration.maxDistance: if provided, ensure that the findSymbols is at most maxDistance away
		- configuration.checkAdjacent: if true, then we will also check adjacent tiles for findSymbols

		distance is counted as the number of cells tunneled; i.e. if the findSymbols is adjacent to startPosition,
		the distance will be 0.

		Note: this does not modify the cells, just return the valid tunnel directions

		@return array of direction that the chosen symbols can be reached.
	**/
	public static function getTunnelDirectionsFromPointToSymbols(cells: Vector2DRegion<String>,
			startPosition: Point2i, findSymbols: Array<String>, configuration: {
			?directions: Array<Direction>,
			?allowedSymbols: Array<String>,
			?minDistance: Null<Int>,
			?maxDistance: Null<Int>,
			?checkAdjacent: Null<Bool>,
		}): Array<Direction> {
		var outDirections: Array<Direction> = [];

		// set up various default values
		final allowedSymbols = configuration.allowedSymbols;
		final directions = configuration.directions == null ? Direction.allFourDirections() : configuration.directions;
		final minDistance: Int = configuration.minDistance == null ? 0 : configuration.minDistance;
		final maxDistance: Null<Int> = configuration.maxDistance;
		final checkAdjacent: Bool = configuration.checkAdjacent == null ? false : configuration.checkAdjacent;

		inline function isValidDistance(distance: Int) {
			if (distance < minDistance) return false;
			if (maxDistance != null && distance > maxDistance) return false;
			return true;
		}

		inline function onSymbolReached(direction: Direction, distance: Int) {
			if (!isValidDistance(distance)) return;
			outDirections.push(direction);
		}

		// test each direction
		for (direction in directions) {
			var distance = 0;
			var curr = startPosition.clone();
			var delta: Point2i = direction;

			curr += delta;
			while (cells.inBound(curr.x, curr.y)) {
				var v = cells.get(curr.x, curr.y);
				if (findSymbols.contains(v)) {
					onSymbolReached(direction, distance);
					break;
				}
				// increase the distance, as we will be tunneling before checking adjacent
				distance += 1;
				// stop early once we have pass the max distance
				if (maxDistance != null && distance > maxDistance) break;

				// check adjacents
				if (checkAdjacent) {
					var adj: Point2i = direction.rotateCW(2);
					var v = cells.get(curr.x + adj.x, curr.y + adj.y);
					if (findSymbols.contains(v)) {
						onSymbolReached(direction, distance);
						break;
					}

					adj = direction.rotateCCW(2);
					v = cells.get(curr.x + adj.x, curr.y + adj.y);
					if (findSymbols.contains(v)) {
						onSymbolReached(direction, distance);
						break;
					}
				}

				if (allowedSymbols != null && !allowedSymbols.contains(v)) break;
				curr += delta;
			}
		}
		return outDirections;
		}

	/**
		scan a region, and calculate the longest continous cells that matches the symbol.

		Each cell will contain a Point2i.
		the x value contains the number of cell that matches the symbols to the left, including the cell itself.
		the y value contains the number of cell that matches the symbols to the top, including the cell itself.
		i.e. for cell {1,1}, the x will contain the number of matching cells from {0,1} - {1,1}

		Note that the digonals are not tested, i.e. it cannot be assumed that a 2x2 rectangle can be constructed.
		i.e, if the cell value is [3, 3], it cannot be assumed at a 3x3 rect can be created.
	**/
	public static function scanContinuousCells(cells: Vector2DRegion<String>,
			symbols: Array<String>): Vector2D<Point2i> {
		var computed = new Vector2D<Point2i>(cells.size, null);
		for (y in 0...cells.size.y) {
			for (x in 0...cells.size.x) {
				var v = cells.get(x, y);
				if (!symbols.contains(v)) {
					computed.set(x, y, [0, 0]);
					continue;
				}
				// get the previous values
				var wx = computed.get(x - 1, y);
				var wy = computed.get(x, y - 1);
				var nx = wx == null ? 1 : wx.x + 1;
				var ny = wy == null ? 1 : wy.y + 1;
				computed.set(x, y, [nx, ny]);
			}
		}
		return computed;
	}

	/**
		tunnel the region from start, in direction until we reached a set of predefined symbols
	**/
	public static function tunnelUntilSymbols(cells: Vector2DRegion<String>, start: Point2i,
			direction: Direction, symbols: Array<String>, tunnelSymbol: String) {
		var curr = start.clone();
		var delta: Point2i = direction;
		curr += delta;
		while (cells.inBound(curr.x, curr.y) && !symbols.contains(cells.get(curr.x, curr.y))) {
			cells.set(curr.x, curr.y, tunnelSymbol);
			curr += delta;
		}
	}

	/**
		Using the output from scanContinuousCells, check if we can place a recti
	**/
	public static function canPlaceRoom(cells: Vector2D<Point2i>, rect: Recti): Bool {
		// ensure that rect is in bound
		if (rect.xMin < 0
			|| rect.yMin < 0
			|| rect.xMax >= cells.size.x
			|| rect.yMax >= cells.size.y) return false;
		// we first check the bottom right of the rect to make sure that the cells value is enough
		{
			final c = cells.get(rect.xMax, rect.yMax);
			// if there isn't enough value, then we will return
			if (c.x < rect.width || c.y < rect.height) return false;
		}

		// we scan the shorter side
		if (rect.width < rect.height) {
			for (x in 1...rect.width) {
				final c = cells.get(rect.xMax - x, rect.yMax);
				if (c.y < rect.height) return false;
			}
		} else {
			for (y in 1...rect.height) {
				final c = cells.get(rect.xMax, rect.yMax - y);
				if (c.x < rect.width) return false;
			}
		}
		return true;
	}

	/**
		Scan and find the various rect that is placeable

		@param point the point of reference
		@param direction the direction from the point to place, only North South East West is allowed
		@size the size of the rect

		the rect will be in the direction of the reference point.

		Assume direction is north, we will place it like this, X = point of reference, O = rect,
		We will try the different x value that `rect.xMin =< point.x <= rect.xMax`

		OOOOO  OOOOO  OOOOO  OOOOO  OOOOO
		OOOOO  OOOOO  OOOOO  OOOOO  OOOOO
		OOOOO  OOOOO  OOOOO  OOOOO  OOOOO
		X       X 			X       X       X

		@return array of recti that is can be placed
	**/
	public static function getValidRoomsFromPoint(cells: Vector2D<Point2i>, point: Point2i,
			direction: Direction, size: Point2i, borderSize: Int = 0): Array<Recti> {
		Assert.assert(Direction.allFourDirections().contains(direction));
		var available: Array<Recti> = [];
		if (direction == North || direction == South) {
			// handle north south differently from east west
			var rect: Recti = null; // the rect to start scanning
			if (direction == North) {
				rect = [point.x - size.x + 1, point.y - size.y, point.x, point.y - 1];
			} else {
				rect = [point.x - size.x + 1, point.y + 1, point.x, point.y + size.y];
			}
			// iterate the x to test
			for (x in 0...size.x) {
				var r = rect.copy();
				r.left += x;
				var checkRect = r;
				if (borderSize != 0) {
					checkRect = r.copy();
					checkRect.expand(borderSize);
				}
				if (canPlaceRoom(cells, checkRect)) available.push(r);
			}
		} else {
			var rect: Recti = null;
			if (direction == West) {
				rect = [point.x - size.x, point.y - size.y + 1, point.x - 1, point.y];
			} else {
				rect = [point.x + 1, point.y - size.y + 1, point.x + size.x, point.y];
			}
			// iterate the y to test
			for (y in 0...size.y) {
				var r = rect.copy();
				r.top += y;
				var checkRect = r;
				if (borderSize != 0) {
					checkRect = r.copy();
					checkRect.expand(borderSize);
				}
				if (canPlaceRoom(cells, checkRect)) available.push(r);
			}
		}
		return available;
	}
}
