package ren.mapgen;

import zf.Recti;

/**
	SubRegionPipeline allows a single pipeline to be runned on different sub region / sub map

	SubRegionPipeline is treated as a "process" because it runned within a MapgenPipeline

**/
class SubRegionPipeline extends MapgenProcess {
	// this is the key to get the sub regions
	public var regionKey: String = "subregion_rooms";
	public var pipeline: MapgenPipeline;

	/**
		map the parent map
	**/
	public var subRegions: Array<MapgenMap>;

	public var overrideConfiguration: Bool = true;

	var currentMap: MapgenMap;
	var toProcess: Array<MapgenMap>;

	public function new(map: MapgenMap, pipeline: MapgenPipeline) {
		super(map);
		this.pipeline = pipeline;
	}

	override public function init(configuration: Map<String, Dynamic>) {
		// override the pipeline configuration
		if (overrideConfiguration) this.pipeline.configuration = configuration;
		this.subRegions = [];
		toProcess = [];
		var regions: Array<Recti> = [];
		if (this.map.metadata[regionKey] != null) {
			regions = this.map.metadata[regionKey];
		}
		for (r in regions) {
			var m = this.map.subRegion(r.xMin, r.yMin, r.width, r.height);
			subRegions.push(m);
			toProcess.push(m);
		}
	}

	override public function canStep(): Bool {
		return currentMap != null || toProcess.length != 0;
	}

	override public function step() {
		if (!canStep()) return;
		if (this.currentMap == null) {
			Assert.assert(toProcess.length != 0);
			this.state = "setting up next map";
			this.currentMap = toProcess.shift();
			this.pipeline.reset(this.currentMap);
			@:privateAccess var subRect = this.currentMap.cells.subRect;
			this.name = 'SubRegion ${subRect}';
			return;
		}
		Assert.assert(currentMap != null);
		this.pipeline.step();
		this.state = '${this.pipeline.getState()}';
		if (this.pipeline.isFinish) {
			this.currentMap = null;
		}
	}
}
