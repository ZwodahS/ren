package ren.mapgen.connectors;

import zf.Point2i;
import zf.Recti;
import zf.Direction;

using zf.ds.ArrayExtensions;

/**
	Takes in a list of Recti, tunnel out from these rect until we need a predefined set of symbols.
	Allow various configurations to bound the tunneling
**/
class ConnectRoomsViaTunnel extends MapgenProcess {
	/**
		A list of symbol that can be tunneled, and what symbol to replace it with.
	**/
	public var tunnelSymbols: Map<String, String>;

	// if a symbol is tunnelel to somehow, but is not in tunnelSymbols, this will be used.
	public var defaultSymbol = ".";

	/**
		A list of symbol that the process will tunnel until
	**/
	public var tunnelUntilSymbols: Array<String> = ["."];

	/**
		The starting rooms to tunnel out from
	**/
	public var sourceRooms: Array<Recti>;

	public var maxDistance: Int = -1;

	public function new(map: MapgenMap, sourceRooms: Array<Recti> = null) {
		super(map);
		this.name = "Tunnel Until";
		this.tunnelSymbols = new Map<String, String>();
		// set up the default symbols
		this.tunnelSymbols["#"] = ".";
		this.tunnelSymbols["."] = ".";
		if (sourceRooms != null) this.sourceRooms = [for (i in sourceRooms) i];
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		// also take from metadata if exists
		if (this.map.metadata['${confPrefix}_sourceRooms'] != null) {
			this.sourceRooms = this.map.metadata['${confPrefix}_sourceRooms'];
		}
		if (this.sourceRooms == null) this.sourceRooms = [];
		Logger.debug('Rooms found: ${this.sourceRooms.length}');
	}

	override public function canStep() {
		return this.sourceRooms.length > 0;
	}

	override public function step() {
		if (!canStep()) return;
		this.state = 'Room left: ${this.sourceRooms.length}';

		var room = this.sourceRooms.randomItem(this.map.r, true);

		// for each direction, figure out which direction we can go
		var validDirections: Array<{d: Direction, start: Point2i, end: Point2i}> = [];
		final cells = this.map.cells;
		final r = this.map.r;
		// for each direction find all the possible directions
		for (direction in Direction.allFourDirections()) {
			// gather the starting positions to tunnel out from
			var starts: Array<Point2i> = [];
			switch (direction) {
				case North:
					for (x in room.xMin...room.xMax + 1) {
						starts.push([x, room.yMin]);
					}
				case South:
					for (x in room.xMin...room.xMax + 1) {
						starts.push([x, room.yMax]);
					}
				case West:
					for (y in room.yMin...room.yMax + 1) {
						starts.push([room.xMin, y]);
					}
				case East:
					for (y in room.yMin...room.yMax + 1) {
						starts.push([room.xMax, y]);
					}
				default:
			}
			// for each starting point, we tunnel out until we hit an end
			for (start in starts) {
				var curr = start.clone();
				var isValid = true;
				var distance: Int = 0;
				while (true) {
					// scan until we hit a valid point
					curr += direction;
					var v = cells.get(curr.x, curr.y);
					if (v == null) {
						isValid = false;
						break;
					}

					// if we reached a good symbol we are done
					if (tunnelUntilSymbols.contains(v)) break;

					// if adj tile is valid, we are also done
					var cw: Point2i = direction.rotateCW(2);
					var ccw: Point2i = direction.rotateCCW(2);
					var adj1 = cells.get(curr.x + cw.x, curr.y + cw.y);
					var adj2 = cells.get(curr.x + ccw.x, curr.y + ccw.y);

					// if adjacent tile is the symbol we want, we also stop
					if (tunnelUntilSymbols.contains(adj1) || tunnelUntilSymbols.contains(adj2)) break;

					// if we hit a symbol that we can't tunnel, we are done, but set it to false
					// @formatter:off
					if (!tunnelSymbols.exists(v) ||
							!tunnelSymbols.exists(adj1) ||
							!tunnelSymbols.exists(adj2)
							) {
						isValid = false;
						break;
					}
					distance += 1;
					if (this.maxDistance != -1 && this.maxDistance < distance) {
						isValid = false;
						break;
					}
				}
				// if valid, then we add it to the list of valid directions
				if (isValid) validDirections.push({d: direction, start: start, end: curr});
			}
		}

		// if there are no connections, means that there is no way to tunnel out of the room
		if (validDirections.length == 0) {
			Logger.debug('No Connections');
			return;
		}

		inline function getSymbol(v: String): String {
			var o = this.tunnelSymbols[v];
			if (o != null) return o;
			Logger.debug('v: ${v} is not in tunnelSymbols');
			return this.defaultSymbol;
		}

		// choose a direction randomly
		var chosenPath = validDirections.randomItem(this.map.r);
		var curr = chosenPath.start;
		// don't replace the start Symbol
		curr += chosenPath.d;
		// tunnel out
		while (curr != chosenPath.end) {
			var v = cells.get(curr.x, curr.y);
			cells.set(curr.x, curr.y, getSymbol(v));
			curr += chosenPath.d;
		}
		// set the end path as well
		var v = cells.get(curr.x, curr.y);
		cells.set(curr.x, curr.y, getSymbol(v));
	}
}
