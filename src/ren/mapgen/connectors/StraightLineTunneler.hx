package ren.mapgen.connectors;

import zf.Point2i;
import zf.Direction;

using zf.MathExtensions;

/**
	Draw a straight line from one point to a direction
**/
class StraightLineTunneler extends MapgenProcess {
	public var minWidth: Int = 1;
	public var maxWidth: Int = 1;
	public var maxTurn: Int = 1;
	public var maxDeviation: Int = 2;
	public var turnProbability: Int = 15;
	public var resizeProbability: Int = 15;
	public var minTurnDelay = 3;
	public var maxTurnDelay = -1;
	public var minResizeDelay = 3;
	public var tunnelSymbol: String = ".";
	// if this is provided, then symbol found in this map will be replaced with a corresponding symbol
	// tunnelSymbol will be used if not found
	public var replaceSymbol: Map<String, String> = null;
	public var affectedSymbols: Array<String>;

	public var movePath: Array<Point2i>;

	var start: Point2i;
	var moveAmt: Int;
	var direction: Direction;

	var current: Point2i;
	var currentDirection: Direction;
	var currentDeviation: Int = 0;
	var currentSize: Int = 1;
	var moved: Int = 0;
	var turnedCounter: Int = 0;
	var isTurned: Bool = false;
	// delay counter for turn and resize
	var turnDelay: Int = 0;
	var resizeDelay: Int = 0;
	// the last direction that we were heading before turning
	// this is used to make sure that turns with width > 1 have a proper right angle
	var lastDirection: Direction;

	public function new(map: MapgenMap, start: Point2i, direction: Direction, moveAmt: Int) {
		super(map);
		this.start = start;
		this.moveAmt = moveAmt;
		this.direction = direction;
		this.confPrefix = "slt";
		this.name = "Tunneling";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_maxTurn'] != null)
			this.maxTurn = configuration['${confPrefix}_maxTurn'];
		if (configuration['${confPrefix}_maxDeviation'] != null)
			this.maxDeviation = configuration['${confPrefix}_maxDeviation'];
		if (configuration['${confPrefix}_minWidth'] != null)
			this.minWidth = configuration['${confPrefix}_minWidth'];
		if (configuration['${confPrefix}_maxWidth'] != null)
			this.maxWidth = configuration['${confPrefix}_maxWidth'];
		if (configuration['${confPrefix}_minTurnDelay'] != null)
			this.minTurnDelay = configuration['${confPrefix}_minTurnDelay'];
		if (configuration['${confPrefix}_maxTurnDelay'] != null)
			this.maxTurnDelay = configuration['${confPrefix}_maxTurnDelay'];
		if (configuration['${confPrefix}_minResizeDelay'] != null)
			this.minResizeDelay = configuration['${confPrefix}_minResizeDelay'];
		if (configuration['${confPrefix}_turnProbabilty'] != null)
			this.turnProbability = configuration['${confPrefix}_turnProbabilty'];
		if (configuration['${confPrefix}_resizeProbability'] != null)
			this.resizeProbability = configuration['${confPrefix}_resizeProbability'];

		this.currentSize = this.minWidth;
		if (configuration['${confPrefix}_startSize'] != null)
			this.currentSize = configuration['${confPrefix}_startSize'];

		this.current = this.start;
		this.lastDirection = this.direction.rotateCW(2);
		this.currentDirection = this.direction;
		this.turnDelay = 0;
		this.movePath = [];
	}

	override public function step() {
		final cells = this.map.cells;
		final r = this.map.r;

		var resizeThisTurn = false;
		// should i resize ?
		if (resizeDelay <= 0 && this.minWidth != this.maxWidth) {
			if (r.randomChance(resizeProbability)) {
				if (this.currentSize == minWidth) {
					this.currentSize += 1;
				} else if (this.currentSize == maxWidth) {
					this.currentSize -= 1;
				} else {
					if (r.randomInt(2) == 0) {
						this.currentSize += 1;
					} else {
						this.currentSize -= 1;
					}
				}
				resizeThisTurn = true;
				this.resizeDelay = this.minResizeDelay;
			}
		}

		// check if i am turning
		if (!resizeThisTurn) {
			if (isTurned) {
				// @formatter:off
				if (Math.iAbs(this.currentDeviation) >= this.maxDeviation || // if we move too much away from center
						// if we have not turned for a while
						this.turnDelay >= this.maxTurnDelay ||
						// if we have pass the delay, then we random the chance
						(this.turnDelay >= this.minTurnDelay && r.randomChance(this.turnProbability))) {
					this.lastDirection = this.currentDirection;
					this.currentDirection = this.direction;
					this.isTurned = false;
					this.turnDelay = 0;
				}
			} else {
				if (this.turnDelay >= this.minTurnDelay && this.turnedCounter < this.maxTurn) {
					// if we haven't turned yet, random chance to turn
					if (this.turnDelay >= this.maxTurnDelay || r.randomChance(this.turnProbability)) {
						this.turnedCounter += 1;
						this.lastDirection = this.currentDirection;
						if (this.currentDeviation < 0 && -this.currentDeviation >= this.maxDeviation) {
							this.currentDirection = this.currentDirection.get_clockwise().get_clockwise();
						} else if (this.currentDeviation > this.maxDeviation) {
							this.currentDirection = this.currentDirection.get_cclockwise().get_cclockwise();
						} else if (r.randomInt(2) == 0) {
							this.currentDirection = this.currentDirection.get_clockwise().get_clockwise();
						} else {
							this.currentDirection = this.currentDirection.get_cclockwise().get_cclockwise();
						}
						this.isTurned = true;
						this.turnDelay = 0;
					}
				}
			}
		}

		var next = this.current + this.currentDirection;
		if (!cells.inBound(next.x, next.y)) {
			moved = this.moveAmt;
			return;
		}
		inline function setCellValue(x: Int, y: Int) {
			var v = cells.get(x, y);
			if (this.affectedSymbols != null && !this.affectedSymbols.contains(v)) return;
			var r = null;
			if (this.replaceSymbol != null) r = this.replaceSymbol[v];
			if (r == null) r = this.tunnelSymbol;
			cells.set(x, y, r);
		}
		setCellValue(next.x, next.y);
		this.movePath.push(next);

		/** Keeping this here, i might want to add a choice to choose between the 2 types of width
			// set adjacents for each width
			var adj: Direction = this.lastDirection.opposite;
			var n = next.clone();
			for (_ in 1...currentSize) {
				n += adj;
				setCellValue(n.x, n.y);
			}
		**/
		// spread the additional width on both side
		var cw = this.currentDirection.rotateCW(2);

		var ccw = this.currentDirection.rotateCCW(2);
		var cwn = next.clone();
		var ccwn = next.clone();
		for (_ in 1...currentSize) {
			if (r.randomChance(50)) {
				cwn += cw;
				setCellValue(cwn.x, cwn.y);
			} else {
				ccwn += ccw;
				setCellValue(ccwn.x, ccwn.y);
			}
		}

		if (this.currentDirection == this.direction) {
			moved += 1;
		} else {
			// if the direction is not in the direction that we are moving,
			// the counter-clockwise direction is considered as negative deviation
			if (this.direction.rotateCCW(2) == this.currentDirection) {
				currentDeviation -= 1;
			} else {
				currentDeviation += 1;
			}
		}
		this.turnDelay += 1;
		this.resizeDelay -= 1;
		this.current = next;
		this.state = '${this.moved}/${this.moveAmt}';
	}

	override public function canStep() {
		return this.moved < this.moveAmt;
	}
}
