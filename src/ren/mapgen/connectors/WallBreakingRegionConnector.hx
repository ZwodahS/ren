package ren.mapgen.connectors;

import zf.ds.Vector2D;
import zf.Point2i;
import zf.Direction;

import ren.mapgen.MapgenRegions;
import ren.mapgen.MapgenRegions.MapgenRegion;

/**
	Connects all the regions together until all regions are connected.

	This connects regions by "breaking" walls.

	Input:
		- regions, provided in metadata via rc_regions.
				Should be provided as Array<List<Point2i>>
**/
class WallBreakingRegionConnector extends MapgenProcess {
	public var connectSymbol: String = ".";

	public var breakableSymbols: Array<String>;

	var breakablePoints: List<Point2i>;
	var regions: MapgenRegions;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Region Connector";
		this.breakableSymbols = ["#"];
	}

	override public function init(configuration: Map<String, Dynamic>) {
		this.state = "Initialising";

		/**
			All regions are provided in the metadata via "rc_regions"
		**/
		if (this.map.metadata["rc_mgRegions"] != null) {
			this.regions = this.map.metadata["rc_mgRegions"];
		} else if (this.map.metadata["rc_regions"] != null) {
			var regions: Array<List<Point2i>> = this.map.metadata["rc_regions"];
			this.regions = new MapgenRegions(this.map.cells.size);
			for (region in regions) {
				this.regions.addRegion(region);
			}
		}

		this.breakablePoints = new List<Point2i>();
		final cells = this.map.cells;
		for (pt => value in cells.iterate()) {
			if (breakableSymbols.contains(value)) breakablePoints.push(pt);
		}
		this.breakablePoints.shuffle(this.map.r);
	}

	override public function canStep(): Bool {
		return this.breakablePoints.length != 0;
	}

	override public function step() {
		if (!canStep()) return;
		while (canStep()) {
			this.state = '${this.breakablePoints.length}';
			var pt = this.breakablePoints.pop();
			var regions = getSeparatingRegions(pt);
			if (regions.length < 2) continue;

			var mergedRegion = this.regions.mergeRegions(regions);
			mergedRegion.addPoint(pt);
			this.map.cells.set(pt.x, pt.y, this.connectSymbol);
			return;
		}
	}

	function getSeparatingRegions(p: Point2i): Array<MapgenRegion> {
		var regions = [];
		var directions: Array<Direction> = [North, South, East, West];
		for (d in directions) {
			var dp: Point2i = p + d;
			var region = this.regions.cells.get(dp.x, dp.y);
			if (region == null) continue;
			if (regions.contains(region)) continue;
			regions.push(region);
		}
		return regions;
	}
}
