package ren.mapgen.connectors;

import zf.Point2i;
import zf.Direction;

class SimpleTunneler extends MapgenProcess {
	public var tunnelUntilSymbols: Array<String> = ["."];

	public var tunnelSymbol = ".";

	public var startPosition: Point2i = null;
	public var tunnelDirections: Array<Direction> = null;

	var isFinish: Bool = false;

	public function new(map: MapgenMap) {
		super(map);
		this.tunnelDirections = [];
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		if (this.map.metadata['${confPrefix}_startPosition'] != null) {
			this.startPosition = this.map.metadata['${confPrefix}_startPosition'];
		}

		if (this.map.metadata['${confPrefix}_direction'] != null) {
			this.tunnelDirections = this.map.metadata['${confPrefix}_directions'];
		}
	}

	override public function canStep() {
		return !isFinish;
	}

	override public function step() {
		if (!canStep()) return;

		for (tunnelDirection in this.tunnelDirections) {
			switch (tunnelDirection) {
				case North:
				case South:
				case East:
				case West:
				default:
					continue;
			}

			final cells = this.map.cells;

			var current = this.startPosition.clone();
			while (true) {
				if (!cells.inBound(current.x, current.y)) break;
				var value = cells.get(current.x, current.y);
				if (tunnelUntilSymbols.contains(value)) break;
				cells.set(current.x, current.y, this.tunnelSymbol);
				current += tunnelDirection;
			}
		}
		this.isFinish = true;
	}
}
