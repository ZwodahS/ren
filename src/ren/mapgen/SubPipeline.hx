package ren.mapgen;

/**
	Create a mapgen process with a internal pipeline.
	This is useful for grouping logical steps together.
**/
class SubPipeline extends MapgenProcess {
	// repeat this pipeline N times
	public var repeat: Int = 0;
	public var pipeline: MapgenPipeline;
	public var overrideConfiguration: Bool = true;

	var currentIteration: Int = 0;

	public function new(map: MapgenMap, pipeline: MapgenPipeline) {
		super(map);
		this.pipeline = pipeline;
		this.name = "Sub Pipeline";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		// override the pipeline configuration
		if (overrideConfiguration) this.pipeline.configuration = configuration;
		pipeline.reset(this.map);
	}

	override public function canStep(): Bool {
		return !this.pipeline.isFinish || (this.repeat != 0 && this.currentIteration <= this.repeat);
	}

	override public function step() {
		if (!canStep()) return;
		if (this.pipeline.isFinish) {
			this.currentIteration += 1;
			if (this.currentIteration <= this.repeat) {
				this.pipeline.reset(this.map);
			}
			updateState();
			return;
		}
		this.pipeline.step();
		updateState();
	}

	function updateState() {
		if (this.repeat != 0) {
			this.state = 'Iteration: ${currentIteration}: ${this.pipeline.getState()}';
		} else {
			this.state = this.pipeline.getState();
		}
	}

	public static function make(name: String, pipeline: MapgenPipeline, map: MapgenMap): MapgenProcess {
		var p = new SubPipeline(map, pipeline);
		p.name = name;
		return p;
	}

	public static function makeRepeat(name: String, numRepeat: Int, pipeline: MapgenPipeline,
			map: MapgenMap): MapgenProcess {
		var p = new SubPipeline(map, pipeline);
		p.name = name;
		p.repeat = numRepeat;
		return p;
	}
}
