package ren.mapgen;

/**
	Mapgen Pipeline defines a series of process that can be used to generate a new map

	@see MapgenProcess
	@see MapgenMap

	This pipeline can be set up to run multiple map with the same set of processes.
	Each map can have different configuration.

	Mon Jun 14 14:44:56 2021
	It it currently still slightly messy when and where the configuration is taken from
	if we allow configuration of the process to set both in map.metadata.configuration
	and process construction.

	Ideally, what can be set in configuration should be set in configuration,
	and what cannot be set in configuration can be directly set in the process
	during construction.

	There are some exception to this, in particular the map generation of sub regions.
	As time goes,	this may change.
**/
class MapgenPipeline {
	/**
		A list of process makers
		Because process have state, instead of having to reset the state everytime a new map is reset,
		we will just have it create a new process instead.
	**/
	var processMaker: Array<MapgenMap->MapgenProcess>;

	/**
		The current process that is being run.
		This will be null when
		1. At the start / end of the generation
		2. In between transition of process
	**/
	public var currentProcess(default, null): MapgenProcess;

	/**
		The current process id
	**/
	var currentProcessInd: Int = -1;

	/**
		Denote if the current map generation is complete
	**/
	public var isFinish(default, null): Bool = false;

	/**
		The current map.

		Reset this via reset method
	**/
	public var map(default, null): MapgenMap;

	public var configuration: Map<String, Dynamic>;

	/**
		Create a new MapgenPipeline
	**/
	public function new(processMaker: Array<MapgenMap->MapgenProcess>,
			configuration: Map<String, Dynamic> = null) {
		this.processMaker = processMaker;
		this.configuration = configuration != null ? configuration : new Map<String, Dynamic>();
	}

	/**
		Reset the map generation with a new map.
	**/
	public function reset(map: MapgenMap) {
		this.map = map;
		this.currentProcess = null;
		this.currentProcessInd = -1;
		this.isFinish = false;
		Logger.debug('Loaded Map: Seed (${map.seed})');
	}

	/**
		Step through the map generation once
	**/
	public function step(): Bool {
		if (this.map == null || this.map.error != null) this.isFinish = true;
		if (this.map == null || this.isFinish) return false;
		updateCurrentProcess();

		// this returns true to treat this as a final step to complete the process.
		// any call to step after this will return false due to the `if` above.
		if (this.currentProcess == null) return true;

		this.currentProcess.step();

		return true;
	}

	/**
		update current process
	**/
	function updateCurrentProcess() {
		if (this.currentProcess != null && !this.currentProcess.canStep()) {
			this.currentProcess.onFinish();
			this.currentProcess.onProcessFinish();
			this.currentProcess = null;
		}

		if (this.map.error != null) return;

		if (this.currentProcess == null) {
			this.currentProcessInd += 1;
			if (this.currentProcessInd >= this.processMaker.length) {
				this.isFinish = true;
				return;
			}
			this.currentProcess = this.processMaker[this.currentProcessInd](this.map);
			this.currentProcess.init(this.configuration);
		}
		Assert.assert(this.currentProcess != null);
	}

	/**
		Step until process changed
	**/
	public function bigStep(): Bool {
		if (this.map == null || this.map.error != null) this.isFinish = true;
		if (this.map == null || this.isFinish) return false;
		updateCurrentProcess();

		// this returns true to treat this as a final step to complete the process.
		// any call to step after this will return false due to the `if` above.
		if (this.currentProcess == null) return false;

		// step until the process finishes
		this.currentProcess.finish();
		Assert.assert(this.currentProcess.canStep() == false);
		return true;
	}

	/**
		Get a string representation of the current state of map generation
	**/
	public function getState(): String {
		if (this.map != null && this.map.error != null) return 'Error: ${this.map.error}';
		if (this.isFinish) return "Completed";
		if (this.currentProcess == null && this.currentProcessInd == -1) return "Init";
		if (this.currentProcess == null) return 'Preparing next process: ${this.currentProcessInd}';
		var s = '${this.currentProcess.name}';
		if (this.currentProcess.state != "") {
			s += ": " + this.currentProcess.state;
		}
		return s;
	}

	/**
		Step the map generation until it is done
	**/
	public function finish() {
		while (!this.isFinish && this.map.error == null) {
			bigStep();
		}
	}
}
