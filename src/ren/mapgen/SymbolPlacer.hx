package ren.mapgen;

import zf.Point2i;
import zf.ds.Vector2DRegion;

import ren.mapgen.cells.CellularAutomata;

/**
	Symbol Placer

	The process will first scan all the possible values and construct a list.
	Afterwhich, the values are then shuffled and processed one at a time.
	Each time a cell is selected, it will be rechecked before flipping.

	This process is useful in cases that we don't want to place the same symbol next to each other.
	For example, if I want to replace a grass tile that is surrounded by grass with a tree tile, but do
	not want 2 tree side by side.

	This will only run once.

	Sat Jun 19 15:28:24 2021
	The first part of this process is similar to CellScan
**/
class SymbolPlacer extends MapgenProcess {
	// Probability of placing the tile once the criteria is met
	public var probability: Int = 100;

	// the symbol to place
	public var placeSymbol: String = '#';

	var validPoints: List<Point2i>;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Symbol Placer";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		scan();
	}

	function scan() {
		this.validPoints = new List<Point2i>();
		// do a scan and collect all the points that matches
		// this first scan is to limit down the all the points that is affected and also shuffle the order
		// in which we select them to be modified.
		for (pt => v in this.map.cells.iterate()) {
			if (match(this.map.cells, pt.x, pt.y)) this.validPoints.add(pt);
		}
		this.validPoints.shuffle(this.map.r);
	}

	override public function canStep(): Bool {
		return this.validPoints.length > 0;
	}

	override public function step() {
		final cells = this.map.cells;
		while (canStep()) {
			var pt = this.validPoints.pop();
			if (!match(cells, pt.x, pt.y)) continue;
			if (this.probability < 100 && !this.map.r.randomChance(this.probability)) continue;
			cells.set(pt.x, pt.y, this.placeSymbol);
		}
	}

	dynamic public function match(cells: Vector2DRegion<String>, x: Int, y: Int): Bool {
		return false;
	}
}
