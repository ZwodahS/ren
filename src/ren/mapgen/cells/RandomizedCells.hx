package ren.mapgen.cells;

/**
	Randomized all cells given a probability table
**/
class RandomizedCells extends FunctionProcess {
	/**
		The list of cells that will be randomized
	**/
	public var affectedSymbols: Array<String>;

	/**
		Probability Table
	**/
	public var symbols: zf.ProbabilityTable<String>;

	public function new(map: MapgenMap) {
		super(map, "Cells Randomizer");
		this.affectedSymbols = ["."];
		this.symbols = null;
	}

	override public function func(map: MapgenMap) {
		if (this.symbols == null || this.symbols.length == 0) return;
		final cells = map.cells;
		for (pt => v in cells.iterate()) {
			if (!affectedSymbols.contains(v)) continue;
			var s = symbols.randomItem(map.r);
			// null means no change
			if (s == null) continue;
			cells.set(pt.x, pt.y, s);
		}
	}

	public static function make(affectedSymbols: Array<String>, symbols: zf.ProbabilityTable<String>,
			map: MapgenMap): MapgenProcess {
		var p = new RandomizedCells(map);
		p.affectedSymbols = affectedSymbols;
		p.symbols = symbols;
		return p;
	}
}
