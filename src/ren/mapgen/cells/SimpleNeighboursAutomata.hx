package ren.mapgen.cells;

import zf.Point2i;

/**
	Simple automata that deals with "alive" and "dead"
**/
class SimpleNeighboursAutomata extends CellularAutomata {
	public var aliveSymbol = "#";
	public var deadSymbol = ".";

	/**
		this is the automata logic int
		this should have 9 different values from 0 to 8
		each index represent what to do when there is that number of neighbours
	**/
	// this applies to cells that are marked as 0
	public var deadLogic: Array<Bool>;

	// this applies to cells that are marked as 1
	public var aliveLogic: Array<Bool>;
	// this affect the value when cells is out of bound
	public var edgeValue: Bool = false;
	// chances to flip the cell from 1 to 0
	public var flipProbability: Int = 100;

	public function new(map: MapgenMap) {
		super(map);
		this.confPrefix = "cell";
		this.name = "SimpleNeighboursAutomata";

		// default conway game of life rule
		this.deadLogic = [false, false, false, true, false, false, false, false, false];
		this.aliveLogic = [false, false, true, true, false, false, false, false, false];
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		if (configuration['${confPrefix}_deadLogic'] != null) {
			this.deadLogic = configuration['${confPrefix}_deadLogic'];
		}
		if (configuration['${confPrefix}_aliveLogic'] != null) {
			this.aliveLogic = configuration['${confPrefix}_aliveLogic'];
		}
		if (configuration['${confPrefix}_edgeValue'] != null) {
			this.edgeValue = configuration['${confPrefix}_edgeValue'];
		}
		if (configuration['${confPrefix}_flipProbability'] != null) {
			this.flipProbability = configuration['${confPrefix}_flipProbability'];
		}

		Logger.debug(' Dead Logic: ${this.deadLogic}');
		Logger.debug('Alive Logic: ${this.aliveLogic}');
	}

	override public function cellular(x: Int, y: Int): String {
		final cells = this.map.cells;
		var curr = cells.get(x, y);
		// only affect the cell if they match our alive and dead symbol
		if (!willAffect(curr)) return null;
		// check probability
		if (this.flipProbability < 100 && !this.map.r.randomChance(flipProbability)) return null;
		// choose the correct logic to use
		final logic = match(curr) ? aliveLogic : deadLogic;
		// count the number of neighbours
		var numNeighbour = cells.countAround(x, y, this.match);
		// if symbol is the same we return null
		var symbol = logic[numNeighbour] ? aliveSymbol : deadSymbol;
		if (symbol == curr) return null;
		return symbol;
	}

	dynamic public function willAffect(s: String): Bool {
		return s == aliveSymbol || s == deadSymbol;
	}

	/**
		returns true if the symbol is "1", false otherwise
	**/
	dynamic public function match(s: String): Bool {
		if (s == null) return edgeValue;
		return s == aliveSymbol;
	}
}
