package ren.mapgen.cells;

import haxe.ds.Vector;

import zf.Direction;
import zf.Point2i;
import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;

@:structInit private class CellularAutomataChange {
	public var x: Int;
	public var y: Int;
	public var value: String;
}

/**
	Generic iterations based cellular automata.

	configurations
	- maxIterations
**/
class CellularAutomata extends MapgenProcess {
	/**
		The number of iterations to run
	**/
	public var maxIterations: Int = 1;

	var iteration: Int = 0;

	/**
		Provide some simple way to use this without having to extend the class.
	**/
	public var handleCell: (Vector2DRegion<String>, Int, Int) -> String = null;

	public function new(map: MapgenMap) {
		super(map);
		this.confPrefix = "cell";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_maxIterations'] != null) {
			this.maxIterations = configuration['${confPrefix}_maxIterations'];
		}
		this.state = "Initialising";
	}

	override public function step() {
		if (!this.canStep()) return;
		iteration += 1;
		this.state = 'Iterations: ${this.iteration} / ${this.maxIterations}';

		// compute the changes for each cell
		var changes: Array<CellularAutomataChange> = [];
		// the changes are consolidated first before updating
		for (pt => v in this.map.cells.iterate()) {
			var change = cellular(pt.x, pt.y);
			if (change != null) changes.push({x: pt.x, y: pt.y, value: change});
		}

		// update all the cells
		for (change in changes) {
			this.map.cells.set(change.x, change.y, change.value);
		}
	}

	override public function canStep(): Bool {
		return (this.iteration < this.maxIterations);
	}

	/**
		process a single cell on the grid.

		@param x the x position of the cell
		@param y the y position of the cell
		@return the new value of the cell, null if there is no change.
	**/
	public function cellular(x: Int, y: Int): String {
		if (this.handleCell != null) return handleCell(map.cells, x, y);
		return null;
	}
}
