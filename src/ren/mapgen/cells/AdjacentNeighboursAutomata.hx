package ren.mapgen.cells;

enum abstract NType(Int) from Int to Int {
	var N0 = 0;
	var N1 = 1;
	var N2A = 2;
	var N2O = 3;
	var N3 = 4;
	var N4 = 5;
}

/**
	Automata for only counting adjacent cells.
	Rather than only counting neighbour, this also allows for handling patterns

	We will store the flip logic in an array like SimpleNeighboursAutomata.
	[N0, N1, N2A, N2O, N3, N4]

	N0 - no neighbour matched
	N1 - 1 neighbour matched
	N2A - 2 adjacent neighbours matched
	N2O - 2 opposite neighbours matched
	N3 - 3 neighbours matched
	N4 - 4 neighbours matched

	matched is test using the `match` function.

	Similar to SimpleNeighboursAutomata, this only flip between "alive" and "dead",
	cells that are neither is not affected.
**/
class AdjacentNeighboursAutomata extends CellularAutomata {
	public var aliveSymbol = "#";
	public var deadSymbol = ".";

	// this applies to cells that are marked as false
	public var deadLogic: Array<Bool>;

	// this applies to cells that are marked as true
	public var aliveLogic: Array<Bool>;

	// what value to use for out of bound cells
	public var edgeValue: Bool = false;

	// chances to flip the cell
	public var flipProbability: Int = 100;

	public function new(map: MapgenMap) {
		super(map);
		this.confPrefix = "cell";
		this.name = "AdjacentNeighboursAutomata";

		// default rule is changing nothing
		this.deadLogic = [false, false, false, false, false, false];
		this.aliveLogic = [true, true, true, true, true, true];
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		if (configuration['${confPrefix}_deadLogic'] != null) {
			this.deadLogic = configuration['${confPrefix}_deadLogic'];
		}
		if (configuration['${confPrefix}_aliveLogic'] != null) {
			this.aliveLogic = configuration['${confPrefix}_aliveLogic'];
		}
		if (configuration['${confPrefix}_edgeValue'] != null) {
			this.edgeValue = configuration['${confPrefix}_edgeValue'];
		}
		if (configuration['${confPrefix}_flipProbability'] != null) {
			this.flipProbability = configuration['${confPrefix}_flipProbability'];
		}
	}

	override public function cellular(x: Int, y: Int): String {
		final cells = this.map.cells;
		var curr = cells.get(x, y);
		// only affect the cell if they match our alive and dead symbol
		if (curr != aliveSymbol && curr != deadSymbol) return null;
		// count adjacent neighbour
		var nType = getAdjacentNeighbourType(x, y);
		var symbol = cell(curr, nType, this.map);
		// if symbol is the same we return null
		if (symbol == null || symbol == curr) return null;
		return symbol;
	}

	dynamic public function cell(symbol: String, nType: Int, map: MapgenMap): String {
		// choose the correct logic to use
		final logic = match(symbol) ? aliveLogic : deadLogic;
		var s = logic[nType] ? aliveSymbol : deadSymbol;
		// if the symbol is the same, we don't have to roll the probability
		if (s == symbol) return null;
		// check probability and if not flip, return null
		if (this.flipProbability < 100 && !this.map.r.randomChance(flipProbability)) return null;
		return s;
	}

	// return the index corresponding to the rules
	function getAdjacentNeighbourType(x: Int, y: Int): NType {
		// count the number of neighbours
		final cells = this.map.cells;
		var numNeighbour = cells.countAdjacent(x, y, this.match);
		switch (numNeighbour) {
			case 0:
				return N0;
			case 1:
				return N1;
			case 2:
				// take the north / south value
				// this means that this is
				var v1 = this.match(cells.get(x, y - 1));
				var v2 = this.match(cells.get(x, y + 1));
				if (v1 == v2) return N2O;
				// the logic works here because if there are exactly 2 neighbour,
				// then either opposites are equal or adjacent are.
				// checking for opposite is easier, as there are more adjacent configurations
				return N2A;
			case 3:
				return N3;
			case 4:
				return N4;
			default:
				Assert.unreachable();
				return N0;
		}
	}

	/**
		returns true if the symbol is "1", false otherwise
	**/
	dynamic public function match(s: String): Bool {
		if (s == null) return edgeValue;
		return s == aliveSymbol;
	}
}
