package ren.mapgen.verify;

import zf.Point2i;
import zf.Direction;

import astar.Graph;
import astar.types.Direction in ASDirection;
import astar.MovementDirection;

/**
	Find the shortest path between pairs of point

	# Input
	- passableSymbols(Array<String>, default: ["."]): list of symbols that is passable
	- points(Map<String, Array<Point>>, default: empty map): map of points, string to identify the pair
**/
class PathFinding extends MapgenProcess {
	public var color: String = null;
	public var passableSymbols: Array<String> = ["."];
	public var points: Map<String, Array<Point2i>> = null;

	// set the allowed movement direction, default to FourWay
	public var movementDirection: MovementDirection = FourWay;

	var toProcess: Array<String>;

	public var processedPoints: Map<String, Array<Point2i>>;

	/**
		the graph costs, use 0 as full obstacle, 1 for fully passable.
		other values are open to be used for other uses.
	**/
	public var graphCosts = [
		1 => [N => 1., S => 1., W => 1., E => 1., NE => 1.3, NW => 1.3, SE => 1.3, SW => 1.3],
	];

	var graph: Graph = null;

	var current: Array<Point2i> = null;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "PathFinding";
		this.points = new Map<String, Array<Point2i>>();
		this.confPrefix = "pathfind";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		this.processedPoints = new Map<String, Array<Point2i>>();
		this.toProcess = [for (key => _ in points) key];

		final cells = this.map.cells;
		this.graph = new Graph(cells.size.x, cells.size.y, this.movementDirection);
		final graphArray = [for (i in 0...(cells.size.x * cells.size.y)) 0];
		var p = 0;
		for (y in 0...cells.size.y) {
			for (x in 0...cells.size.x) {
				graphArray[p++] = cost(x, y);
			}
		}
		graph.setWorld(graphArray);
		graph.setCosts(graphCosts);
	}

	override public function canStep(): Bool {
		return this.toProcess.length > 0;
	}

	override public function step() {
		if (!canStep()) return;
		final cells = this.map.cells;
		// clear the previous color
		resetColor();
		// choose the nxt one to process
		var key = this.toProcess.pop();
		var points = this.points[key];
		var result = this.graph.solve(points[0].x, points[0].y, points[1].x, points[1].y);
		var path: Array<Point2i> = null;
		Logger.debug('result: ${result.result}');
		if (result.result == Solved) {
			var path: Array<Point2i> = [for (p in result.path) [p.x, p.y]];
			this.processedPoints[key] = path;
			if (this.color != null) {
				for (p in path) cells.set(p.x, p.y, '${cells.get(p.x, p.y)}:${this.color}');
				this.current = path;
			}
		} else {
			this.processedPoints[key] = null;
		}
	}

	override public function onFinish() {
		resetColor();
	}

	function resetColor() {
		final cells = this.map.cells;
		if (this.color != null && this.current != null) {
			for (p in this.current) {
				var v = cells.get(p.x, p.y);
				cells.set(p.x, p.y, v.charAt(0));
			}
		}
		this.current = null;
	}

	dynamic public function cost(x: Int, y: Int): Int {
		final cells = this.map.cells;
		var value = cells.get(x, y);
		if (this.passableSymbols.contains(value)) return 1;
		return 0;
	}
}
