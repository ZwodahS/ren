package ren.mapgen.verify;

import zf.Point2i;
import zf.Direction;

import ren.mapgen.MapgenRegions;

/**
	Scan the map and find all various connected regions

	Fri Jun 18 23:55:44 2021
	IDEA: Add a debug mode, i.e. one that doesn't run outside of debug mode
	This is useful when I just want to visualise the connectivity in between steps
**/
class Connectivity extends MapgenProcess {
	public var passableSymbols: Array<String> = ["."];

	// for coloring purpose we will label them these
	// also, we will remove all the coloring at the end of the process
	public var coloring: Array<String> = ["b", "g", "r", "y", "m"];

	public var fastFinish: Bool = true;

	var currColoring: Int = 0;
	var currentRegion: MapgenRegion;
	var activeList: List<Point2i>;
	var toProcess: List<Point2i>;

	// if provided, if these points are not in the same region, we will set the map as error
	public var ensureConnected: Array<Point2i>;

	/**
		This will contain the final regions generated by the connectivity process
	**/
	public var regions: MapgenRegions;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Connectivity";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		final cells = this.map.cells;
		this.regions = new MapgenRegions(cells.size);
		this.toProcess = new List<Point2i>();
		this.activeList = new List<Point2i>();
		for (pt => v in cells.iterate()) {
			if (passableSymbols.contains(v)) {
				toProcess.add(pt);
			}
		}
	}

	override public function canStep(): Bool {
		return (this.activeList.length != 0 || this.toProcess.length != 0);
	}

	override public function step() {
		if (!fastFinish) return _step();
		while (canStep()) {
			_step();
		}
	}

	function _step() {
		if (!canStep()) return;
		final cells = this.map.cells;

		// if activeList is null, we need to start a new region
		if (activeList.length == 0) {
			this.currentRegion = this.regions.addRegion([]);
			this.currColoring = this.currentRegion.id % this.coloring.length;
			while (toProcess.length > 0) {
				var p = toProcess.pop();
				var s = cells.get(p.x, p.y);
				// either it is not passable, or we had already process it
				if (!this.passableSymbols.contains(s)) continue;
				activeList.add(p);
				break;
			}
			// if by here we don't have anything to process, we are done
			if (activeList.length == 0) return;
		}

		this.state = '${this.toProcess.length}|${this.activeList.length}';

		while (this.activeList.length > 0) {
			var curr = this.activeList.pop();
			var v = cells.get(curr.x, curr.y);
			if (!this.passableSymbols.contains(v)) continue; // likely we have processed it
			// the (good) side effect of this relabeling is that it will no longer be passable.
			// this will skip any reprocessing
			cells.set(curr.x, curr.y, '${v}:${this.coloring[currColoring]}');
			this.currentRegion.addPoint(curr);

			// for each direction, we check if any of them is passable
			for (d in Direction.allFourDirections()) {
				var np = curr + d;
				var nv = cells.get(np.x, np.y);
				if (this.passableSymbols.contains(nv)) this.activeList.push(np);
			}
			return;
		}
	}

	override public function onFinish() {
		final cells = this.map.cells;
		// reset all the cells
		for (pt => v in cells.iterate()) {
			cells.set(pt.x, pt.y, v.charAt(0));
		}

		if (this.ensureConnected != null) {
			var region: MapgenRegion = null;
			for (p in this.ensureConnected) {
				var r = this.regions.cells.get(p.x, p.y);
				if (r == null || (region != null && r != region)) {
					map.error = 'not connected';
					break;
				}
			}
		}
	}

	public static function make(passableSymbols: Array<String>, map: MapgenMap): MapgenProcess {
		var p = new Connectivity(map);
		if (passableSymbols != null) p.passableSymbols = passableSymbols;
		return p;
	}
}
