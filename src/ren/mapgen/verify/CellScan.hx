package ren.mapgen.verify;

import zf.Point2i;
import zf.ds.Vector2DRegion;

enum ScanMode {
	FindFirst;
	FindAll;
}

enum ScanArea {
	Row;
	Column;
	All;
}

/**
	# Overview
	Scan cells and find cells that matches a criteria.
	This can be used as a pre-step to other process

	ScanMode:
		- FindFirst: stop when the first cell is found. Does not work for ScanArea all
		- FindAll: stop when all cell is scanned.

	Scan Area:
		- Row: scan row
		- Column: scan column
		- All: scan the whole map

	If the constructor is used directly, the default mode is All + FindAll

	# Configurations
	- color(String, default: "r"): a debug symbol to append while scanning

	## Dynamic Functions
	- match - must be override to match the cell
**/
class CellScan extends MapgenProcess {
	public var color = "r";

	var scanned: Bool = false;

	public var matchedPoints: List<Point2i>;

	var scanReversed: Bool = false;
	var scanArea: ScanArea = All;
	var scanMode: ScanMode = FindAll;

	var startPos: Point2i = null;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Scanning Cell";
	}

	override public function canStep(): Bool {
		return !this.scanned;
	}

	override public function step() {
		this.matchedPoints = new List<Point2i>();
		var it: KeyValueIterator<Point2i, String> = null;
		if (this.scanArea == All) {
			it = this.map.cells.iterate();
			this.scanMode = FindAll;
		} else if (this.scanArea == Row) {
			it = this.map.cells.iterateRow(this.startPos.x, this.startPos.y, scanReversed);
		} else if (this.scanArea == Column) {
			it = this.map.cells.iterateColumn(this.startPos.x, this.startPos.y, scanReversed);
		} else {
			this.scanned = true;
			return;
		}

		if (this.scanMode == FindAll) {
			for (pt => v in it) {
				if (match(this.map.cells, pt.x, pt.y)) {
					this.matchedPoints.add(pt);
					map.cells.set(pt.x, pt.y, map.cells.get(pt.x, pt.y) + ':${color}');
				}
			}
		} else {
			for (pt => v in it) {
				if (match(this.map.cells, pt.x, pt.y)) {
					this.matchedPoints.add(pt);
					map.cells.set(pt.x, pt.y, map.cells.get(pt.x, pt.y) + ':${color}');
					break;
				}
			}
		}

		this.scanned = true;
	}

	dynamic public function match(cells: Vector2DRegion<String>, x: Int, y: Int): Bool {
		return false;
	}

	override public function onFinish() {
		for (pt in this.matchedPoints) {
			map.cells.set(pt.x, pt.y, map.cells.get(pt.x, pt.y).charAt(0));
		}
	}

	public static function scanAll(map: MapgenMap): CellScan {
		var p = new CellScan(map);
		return p;
	}

	public static function scanRow(startPosition: Point2i, reversed: Bool, scanMode: ScanMode,
			map: MapgenMap): CellScan {
		var p = new CellScan(map);
		p.scanReversed = reversed;
		p.startPos = startPosition.clone();
		p.scanArea = Row;
		p.scanMode = scanMode;
		return p;
	}

	public static function scanColumn(startPosition: Point2i, reversed: Bool, scanMode: ScanMode,
			map: MapgenMap): CellScan {
		var p = new CellScan(map);
		p.scanReversed = reversed;
		p.startPos = startPosition.clone();
		p.scanArea = Column;
		p.scanMode = scanMode;
		return p;
	}

	public static function scanAllFindSymbols(symbols: Array<String>, map: MapgenMap) {
		var p = new CellScan(map);
		p.match = function(cells: Vector2DRegion<String>, x: Int, y: Int) {
			return symbols.contains(cells.get(x, y));
		}
		return p;
	}
}
