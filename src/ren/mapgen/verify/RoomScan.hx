package ren.mapgen.verify;

using zf.MathExtensions;

import zf.ds.Vector2D;
import zf.Point2i;
import zf.Recti;

/**
	Thu 14:20:29 07 Oct 2021
	This may have some room for optimisation.
	The way that the rooms are returned are not very ideal.
**/
class RoomScan extends MapgenProcess {
	/** Debug **/
	/**
		For debugging purpose, set to a color to color
	**/
	public var color: String = null;

	/** Input **/
	/**
		The allowed symbol to place room
	**/
	public var allowedSymbols: Array<String> = ["#"];

	/**
		The minimum size is each direction
	**/
	public var minSize(never, set): Int;

	public function set_minSize(s: Int): Int {
		this.minSizeX = s;
		this.minSizeY = s;
		return s;
	}

	public var minSizeX: Int = 3;
	public var minSizeY: Int = 3;

	/** output **/
	/**
		after analysis this will contains all the recti
	**/
	public var possibleValidRooms: Array<Recti>;

	/**
		store the maximum width and height that can be placed
	**/
	public var cache: Vector2D<Point2i>;

	public var minX: Null<Int> = null;
	public var minY: Null<Int> = null;
	public var maxX: Null<Int> = null;
	public var maxY: Null<Int> = null;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Scanning Room";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration['${confPrefix}_color'] != null) {
			this.color = configuration['${confPrefix}_color'];
		}
	}

	override public function canStep() {
		if (this.possibleValidRooms == null) return true;
		return false;
	}

	override public function step() {
		if (!canStep()) return;
		scan();
	}

	function scan() {
		final cells = this.map.cells;
		// Pre-Scan the longest distance for each direction for each cells
		// we probably need them anyway
		this.cache = Utils.scanContinuousCells(cells, this.allowedSymbols);

		// for each cell, generate the maximum width / height in each direction
		this.possibleValidRooms = [];

		var xMin = this.minX == null ? 0 : Math.clampI(this.minX, 0, cells.size.x);
		var xMax = this.maxX == null ? cells.size.x : Math.clampI(this.maxX, xMin, cells.size.x);
		var yMin = this.minY == null ? 0 : Math.clampI(this.minY, 0, cells.size.y);
		var yMax = this.maxY == null ? cells.size.y : Math.clampI(this.maxY, yMin, cells.size.y);

		for (y in yMin...yMax) {
			for (x in xMin...xMax) {
				var v = this.cache.get(x, y);
				// we go left until we get the full rect possible
				var height = v.y;
				for (tx in 0...v.x) {
					var cv = this.cache.get(x - tx, y);
					Assert.assert(cv != null);
					height = hxd.Math.imin(height, cv.y);
				}
				var xrect = new Recti(x - v.x + 1, y - height + 1, x, y);
				if (xrect.width < this.minSizeX || xrect.height < this.minSizeY) {
					xrect = null;
				}
				// we go up until we get a full rect
				var width = v.x;
				for (ty in 0...v.y) {
					var cv = this.cache.get(x, y - ty);
					Assert.assert(cv != null);
					width = hxd.Math.imin(width, cv.x);
				}

				var yrect = new Recti(x - width + 1, y - v.y + 1, x, y);
				if (yrect.width < this.minSizeX || yrect.height < this.minSizeY) {
					yrect = null;
				}

				// dedupe
				if (xrect != null) this.possibleValidRooms.push(xrect);
				if (yrect != null && yrect != xrect) this.possibleValidRooms.push(yrect);
			}
		}

		if (this.color != null) {
			inline function colorRect(rect: Recti) {
				for (y in rect.yMin...rect.yMax + 1) {
					for (x in rect.xMin...rect.xMax + 1) {
						cells.set(x, y, cells.get(x, y) + ':${color}');
					}
				}
			}
			for (rect in this.possibleValidRooms) {
				colorRect(rect);
			}
		}
	}

	override public function onFinish() {
		// remove the color
		if (this.color != null) {
			final cells = this.map.cells;
			// reset all the cells
			for (pt => v in cells.iterate()) {
				cells.set(pt.x, pt.y, v.charAt(0));
			}
		}
	}

	public static function make(allowedSymbols: Array<String>, minSize: Int, map: MapgenMap): MapgenProcess {
		var p = new RoomScan(map);
		p.allowedSymbols = allowedSymbols;
		p.minSize = minSize;
		return p;
	}
}
