package ren.mapgen;

import zf.ds.Vector2D;
import zf.Point2i;

class MapgenRegion {
	public var id: Int;
	public var points: List<Point2i>;
	public var regions: MapgenRegions;

	public function new(id: Int, regions: MapgenRegions) {
		this.id = id;
		this.points = new List<Point2i>();
		this.regions = regions;
	}

	public function mergeInto(region: MapgenRegion) {
		for (p in this.points) {
			region.addPoint(p);
		}
		this.remove();
	}

	public function addPoint(p: Point2i) {
		this.points.push(p);
		this.regions.cells.set(p.x, p.y, this);
	}

	public function remove() {
		this.regions.regions[this.id] = null; // clear away this region
	}

	public function toString() {
		// this is necessary because this reference regions and it will break js recursion
		return '(id:${id}): ${points}';
	}
}

class MapgenRegions {
	public var regions: Array<MapgenRegion>;
	public var cells: Vector2D<MapgenRegion>;

	public function new(size: Point2i) {
		this.regions = [];
		this.cells = new Vector2D<MapgenRegion>(size, null);
	}

	public function addRegion(points: Iterable<Point2i>): MapgenRegion {
		var r = new MapgenRegion(this.regions.length, this);
		for (p in points) {
			r.addPoint(p);
		}
		this.regions.push(r);
		return r;
	}

	public function mergeRegions(regions: Array<MapgenRegion>): MapgenRegion {
		Assert.assert(regions.length >= 2);
		var mainRegion: MapgenRegion = regions[0];

		// find the smallest id and merge into it
		for (i in 1...regions.length) {
			if (regions[i].id < mainRegion.id) {
				mainRegion = regions[i];
			}
		}

		var others: Array<MapgenRegion> = [];
		for (r in regions) {
			if (r.id != mainRegion.id) others.push(r);
		}

		for (other in others) {
			other.mergeInto(mainRegion);
		}

		return mainRegion;
	}
}
