package ren.mapgen.rooms;

import zf.Point2i;
import zf.Direction;

using zf.ds.ArrayExtensions;

private class Walker {
	public var position: Point2i;
	public var step: Int;
	public var walkCompleted: Int;
	public var currentDirection: Direction;
	public var traversed: Map<String, Point2i>;

	public function new(position: Point2i, startDirection: Direction) {
		this.position = position;
		this.step = 0;
		this.walkCompleted = 0;
		this.currentDirection = startDirection;
		this.traversed = new Map<String, Point2i>();
	}
}

class RandomWalk extends MapgenProcess {
	public var currentSymbol = "x";
	public var walkSymbol: String = ".";
	public var affectedSymbol: Array<String> = ["#", "."];

	public var maxStep: Int = 20;
	public var walkerStartPositions: Array<Point2i> = [];
	public var turnProbability: Int = 50;
	public var numWalk: Int = 1;

	var walkers: Array<Walker>;

	public function new(map: MapgenMap) {
		super(map);
		this.confPrefix = 'randomwalk';
	}

	override public function init(configuration: Map<String, Dynamic>) {
		this.walkers = [];
		if (configuration['${confPrefix}_maxStep'] != null) {
			this.maxStep = configuration['${confPrefix}_maxStep'];
		}
		if (configuration['${confPrefix}_walkerStartPositions'] != null) {
			this.walkerStartPositions = configuration['${confPrefix}_walkerStartPositions'];
		}
		if (configuration['${confPrefix}_turnProbability'] != null) {
			this.turnProbability = configuration['${confPrefix}_turnProbability'];
		}
		if (configuration['${confPrefix}_numWalk'] != null) {
			this.numWalk = configuration['${confPrefix}_numWalk'];
		}

		final cells = this.map.cells;

		for (w in this.walkerStartPositions) {
			walkers.push(new Walker(w, Direction.randomFourDirection(this.map.r)));
			cells.set(w.x, w.y, currentSymbol);
		}
	}

	override public function canStep(): Bool {
		for (w in this.walkers) {
			if (w.step < this.maxStep) return true;
		}
		return false;
	}

	override public function step() {
		for (w in this.walkers) {
			stepWalker(w);
		}
	}

	function stepWalker(w: Walker) {
		w.step += 1;
		var np = w.position + w.currentDirection;
		final cells = this.map.cells;
		var forceTurn = false;
		if (!cells.inBound(np.x, np.y) || !affectedSymbol.contains(cells.get(np.x, np.y))) forceTurn = true;
		if (forceTurn || this.map.r.randomChance(this.turnProbability)) {
			if (this.map.r.randomInt(2) == 0) {
				w.currentDirection = w.currentDirection.rotateCW(2);
			} else {
				w.currentDirection = w.currentDirection.rotateCCW(2);
			}
			np = w.position + w.currentDirection;
		}

		var value = cells.get(np.x, np.y);
		if (this.affectedSymbol.contains(value)) {
			cells.set(w.position.x, w.position.y, this.walkSymbol);
			cells.set(np.x, np.y, this.currentSymbol);
			w.position = np;
			w.traversed[np.toString()] = np;
		}

		if (w.step >= this.maxStep) {
			w.walkCompleted += 1;
			cells.set(w.position.x, w.position.y, this.walkSymbol);
			if (w.walkCompleted < this.numWalk) {
				w.step = 0;
				var traversed: Array<Point2i> = [for (v in w.traversed) v];
				traversed.shuffle(this.map.r);
				w.position = traversed[0];
				cells.set(w.position.x, w.position.y, this.currentSymbol);
			} else {
				cells.set(w.position.x, w.position.y, this.walkSymbol);
			}
		}
	}
}
