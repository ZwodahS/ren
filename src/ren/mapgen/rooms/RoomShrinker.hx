package ren.mapgen.rooms;

import zf.Recti;

using zf.MathExtensions;

/**
	Shrink the rooms down to a certain size
**/
class RoomShrinker extends MapgenProcess {
	// the key in metadata to find the rooms
	public var getKey = "rooms";
	// the key in metadata to set the new set of rooms
	public var setKey = "rooms";

	public var shrinkSymbol: String = "#";
	public var minSize: Int = 1;
	public var maxSize: Int = -1;

	/** Process state data **/
	var toProcess: Array<Recti>;

	var newRooms: Array<Recti>;

	public function new(map: MapgenMap) {
		super(map);
	}

	override public function init(configuration: Map<String, Dynamic>) {
		if (configuration["rs_minSize"] != null) this.minSize = configuration["rs_minSize"];
		if (configuration["rs_maxSize"] != null) this.maxSize = configuration["rs_maxSize"];

		this.toProcess = this.map.metadata[this.getKey];
		if (this.toProcess == null) this.toProcess = [];
		this.newRooms = [];
	}

	override public function step() {
		if (!canStep()) return;
		var room = toProcess.pop();

		var min = Math.clampI(this.minSize, 1, null);
		var max = maxSize == -1 ? -1 : Math.clampI(this.maxSize, min, null);
		// decide how much to shrink to

		var xSize = 0;
		// if there is not enough size to shrink to min, we will not shrink
		if (min > room.width) {
			xSize = room.width;
		} else {
			var xMax = max;
			if (max == -1 || max > room.width) {
				xMax = room.width;
			}
			xSize = this.map.r.randomWithinRange(min, xMax);
		}

		var ySize = 0;
		// similary, if there isn't enough height to shrink to min, we will not shrink
		if (min > room.height) {
			ySize = room.height;
		} else {
			var yMax = max;
			if (max == -1 || max > room.height) {
				yMax = room.height;
			}
			ySize = this.map.r.randomWithinRange(min, yMax);
		}

		var xStart = room.left;
		var yStart = room.top;

		var variableX = room.width - xSize;
		var variableY = room.height - ySize;

		if (variableX > 0) xStart += this.map.r.randomInt(variableX);
		if (variableY > 0) yStart += this.map.r.randomInt(variableY);

		// now that we have the room, let's construct the new room
		var newRoom: Recti = [xStart, yStart, xStart + xSize - 1, yStart + ySize - 1];
		fillRoom(room, newRoom);
		this.newRooms.push(newRoom);
	}

	function fillRoom(oldRoom: Recti, newRoom: Recti) {
		final cells = this.map.cells;
		for (y in oldRoom.yMin...oldRoom.yMax + 1) {
			for (x in oldRoom.xMin...oldRoom.xMax + 1) {
				if (newRoom.contains(x, y)) continue;
				cells.set(x, y, this.shrinkSymbol);
			}
		}
	}

	override public function canStep(): Bool {
		return toProcess.length > 0;
	}

	override public function onFinish() {
		this.map.metadata[setKey] = this.newRooms;
	}
}
