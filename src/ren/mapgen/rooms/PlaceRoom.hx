package ren.mapgen.rooms;

using zf.MathExtensions;
using zf.ds.ArrayExtensions;

import zf.ds.Vector2D;
import zf.Point2i;
import zf.Recti;

/**
	# Overview
	There is 2 mode for this.
	1. Take in a list of rects
	2. Take in a list of points

	if neither of this is present, it will be a no-op

	When a list of rects is provided, the room place will be within the rect.
	When a list of points is provided, the room place will be placed around the point, using the point as center

	# Configurations
	- numRooms(Int, default: 1): number of rooms to place.
	- minRoomSize(Int, default: 1): size of the room to place.
	- maxRoomSize(Int, default: 1): size of the room to place.
	- sharedVariableSize - whether there is a variabe room size component that is shared. See Notes
	- availableRects(Array<Rect>, default: null): the list of available rects to place
	- allowedSymbols(Array<String>, default: ["#"]): the list of allowed symbols.
	- roomSymbol(String, default: "."): the symbol to set.
	- outerBorder(String, null): the border symbol to set around the room
	- innerBorder(String, null): the border symbol to set inside the room

	# Notes
	Note on sharedVariableSize
		When this is -1, the room size wil be generated between min and max for both dimension
		When this is >= 0, then a random number will be generate between 0 and sharedVariableSize.
		This variance will be shared between both dimension, bounded by min and max size

	# Output:
	- placed(Array<Recti>): Array of Recti

**/
class PlaceRoom extends MapgenProcess {
	/** Input **/
	// num Room to place, this is more of a maximum room placed.
	public var numRooms: Int = 1;

	// minimum size for the room
	public var minRoomSize: Int = 1;
	// variable size and various parameters
	public var maxRoomSize: Int = 1;
	// if sharedVariableSize is != -1, then instead of randomly having a size between min and max,
	// the random room size will be generated between 0 and sharedVariableSize and share the value
	// between x and y
	public var sharedVariableSize: Int = -1;
	// list of recti that we can place
	public var availableRects: Array<Recti>;
	// list of points to place
	public var availablePoints: Array<Point2i>;

	public var allowedSymbols: Array<String> = ["#"];
	public var roomSymbol: String = ".";

	/**
		Surround the border with this symbol.
		This will prevent the placement from putting room side by side.
		This will also replacement symbols in the allowedSymbols

		Setting this to null will not set border.
	**/
	public var outerBorder: String = null;

	public var innerBorder: String = null;

	/** output **/
	public var placed: Array<Recti>;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Placing Room";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);

		if (configuration['${confPrefix}_numRooms'] != null) {
			this.numRooms = configuration['${confPrefix}_numRooms'];
		}

		if (configuration['${confPrefix}_minRoomSize'] != null) {
			this.minRoomSize = configuration['${confPrefix}_minRoomSize'];
		}

		if (configuration['${confPrefix}_maxRoomSize'] != null) {
			this.maxRoomSize = configuration['${confPrefix}_maxRoomSize'];
		}

		if (configuration['${confPrefix}_sharedVariableSize'] != null) {
			this.sharedVariableSize = configuration['${confPrefix}_sharedVariableSize'];
		}

		this.placed = [];
		if (this.availableRects != null) this.availableRects.shuffle(this.map.r);
		if (this.availablePoints != null) this.availablePoints.shuffle(this.map.r);
	}

	override public function canStep() {
		if (this.availableRects == null && this.availablePoints == null) return false;
		if (this.placed.length >= this.numRooms) return false;
		if (this.availableRects != null && this.availableRects.length == 0) return false;
		if (this.availablePoints != null && this.availablePoints.length == 0) return false;
		return true;
	}

	override public function step() {
		if (!canStep()) return;

		final cells = this.map.cells;
		inline function setCellValue(x: Int, y: Int, symbol: String) {
			var v = cells.get(x, y);
			if (isCellAllowed(v)) cells.set(x, y, symbol);
		}

		function canPlaceRoom(rect: Recti): Bool {
			// check if we can place the rect here
			for (x in rect.xMin...rect.xMax + 1) {
				for (y in rect.yMin...rect.yMax + 1) {
					if (!cells.inBound(x, y)) return false;
					if (!isCellAllowed(cells.get(x, y))) return false;
				}
			}
			return true;
		}

		function hasMoreRooms(): Bool {
			if (this.availableRects != null) {
				return this.availableRects.length > 0;
			} else if (this.availablePoints != null) {
				return this.availablePoints.length > 0;
			}
			return false;
		}

		while (hasMoreRooms()) {
			this.state = 'Placed: ${this.placed.length}';
			var rect = this.getRoomPlacement(this.map.r);
			if (!canPlaceRoom(rect)) continue;
			// now that we can place it, let's set the room
			this.placed.push(rect);
			for (x in rect.xMin...rect.xMax + 1) {
				for (y in rect.yMin...rect.yMax + 1) {
					cells.set(x, y, this.roomSymbol);
				}
			}
			// if border is true, then we set the 1 cell around the cell
			if (this.outerBorder != null) {
				for (x in rect.xMin - 1...rect.xMax + 2) {
					setCellValue(x, rect.yMin - 1, this.outerBorder);
					setCellValue(x, rect.yMax + 1, this.outerBorder);
				}
				for (y in rect.yMin - 1...rect.yMax + 2) {
					setCellValue(rect.xMin - 1, y, this.outerBorder);
					setCellValue(rect.xMax + 1, y, this.outerBorder);
				}
			}
			// if innerBorder is true, then we set the 1 cell around the cell
			if (this.innerBorder != null) {
				for (x in rect.xMin...rect.xMax + 1) {
					cells.set(x, rect.yMin, this.innerBorder);
					cells.set(x, rect.yMax, this.innerBorder);
				}
				for (y in rect.yMin...rect.yMax + 1) {
					cells.set(rect.xMin, y, this.innerBorder);
					cells.set(rect.xMax, y, this.innerBorder);
				}
			}
			return;
		}
	}

	/**
		Get a random rect to place
	**/
	function getRoomPlacement(r: hxd.Rand): Recti {
		if (this.availableRects != null) {
			var rect = this.availableRects.pop();
			var roomSize = getRoomSize(r, rect);
			var xSize = roomSize.x;
			var ySize = roomSize.y;

			var startX: Int = r.randomWithinRange(rect.xMin, rect.width - xSize);
			var startY: Int = r.randomWithinRange(rect.yMin, rect.height - ySize);
			return [startX, startY, startX + xSize - 1, startY + ySize - 1];
		} else if (this.availablePoints != null) {
			var point = this.availablePoints.pop();
			var roomSize = getRoomSize(r, null);
			var rect = constructRectFromCenter(point, roomSize, r);
			return rect;
		}
		return null;
	}

	function constructRectFromCenter(point: Point2i, size: Point2i, r: hxd.Rand): Recti {
		final rect = new Recti(point.x, point.y, point.x, point.y);
		var spareX = 0;
		var spareY = 0;
		var sizeX = size.x - 1;
		var sizeY = size.y - 1;
		if (sizeX % 2 == 1) {
			spareX = 1;
			sizeX -= 1;
		}
		if (sizeY % 2 == 1) {
			spareY = 1;
			sizeY -= 1;
		}
		sizeX = Std.int(sizeX / 2);
		sizeY = Std.int(sizeY / 2);
		rect.xMin -= sizeX;
		rect.xMax += sizeX;
		rect.yMin -= sizeY;
		rect.yMax += sizeY;
		if (spareX == 1) {
			if (r.randomInt(2) == 0) {
				rect.xMin -= 1;
			} else {
				rect.xMax += 1;
			}
		}
		if (spareY == 1) {
			if (r.randomInt(2) == 0) {
				rect.yMin -= 1;
			} else {
				rect.yMax += 1;
			}
		}
		return rect;
	}

	/**
		Calculate and get a random room size
	**/
	function getRoomSize(r: hxd.Rand, rect: Recti = null): Point2i {
		if (rect != null) {
			if (this.sharedVariableSize == -1) {
				// by default we will random the size between min and max
				var xSize = r.randomWithinRange(minRoomSize, hxd.Math.imin(rect.width, maxRoomSize));
				var ySize = r.randomWithinRange(minRoomSize, hxd.Math.imin(rect.height, maxRoomSize));
				return [xSize, ySize];
			} else {
				var variable = r.randomInt(this.sharedVariableSize + 1);
				var variableX = r.randomInt(Math.clampI(variable, 0,
					hxd.Math.imin(rect.width, maxRoomSize) - minRoomSize));
				var variableY = Math.clampI(variable - variableX, 0,
					hxd.Math.imin(rect.height, maxRoomSize) - minRoomSize);
				return [minRoomSize + variableX, minRoomSize + variableY];
			}
		} else {
			if (this.sharedVariableSize == -1) {
				// by default we will random the size between min and max
				var xSize = r.randomWithinRange(minRoomSize, maxRoomSize);
				var ySize = r.randomWithinRange(minRoomSize, maxRoomSize);
				return [xSize, ySize];
			} else {
				var variable = r.randomInt(this.sharedVariableSize + 1);
				var variableX = r.randomInt(Math.clampI(variable, 0, maxRoomSize - minRoomSize));
				var variableY = Math.clampI(variable - variableX, 0, maxRoomSize - minRoomSize);
				return [minRoomSize + variableX, minRoomSize + variableY];
			}
		}
	}

	dynamic public function isCellAllowed(s: String): Bool {
		return this.allowedSymbols.contains(s);
	}
}
