package ren.mapgen.rooms;

import zf.Recti;
import zf.Point2i;

using zf.MathExtensions;
using zf.ds.ArrayExtensions;

/**
	# Overview
	This process will randomly place room on the map.
	There are various configuration available to configure the room placement process.

	# Configurations
	- maxRoom(Int, default: -1): the maximum number of room to place, -1 for no limit, until maxTries is reached.
	- oddIndexRoom(Bool, default: false): place the room at odd index. See below for notes on this
	- maxTries(Int, default: 50): maximum number of tries when placing rooms.
	- minRoomSize(Int, default: 1): minimum room size. See below for notes when combining with oddIndexRoom
	- maxRoomSize(Int, default: 1): minimum room size. See below for notes when combining with oddIndexRoom
	- setBorder(Bool, default: true) - whether or not the room border is set to another symbol
	- sharedVariableSize - whether there is a variabe room size component that is shared. See Notes

	# Notes
	Note on oddIndexRoom and sizes
		This is used in combination with various maze generations that have pillars.
		This changes how size is calculated.
		When oddIndexRoom is true, all size are counted twice, i.e. size 1 = 1x1, size 2 = 3x3
		This also applies to sharedVariableSize

	Note on sharedVariableSize
		When this is -1, the room size wil be generated between min and max for both dimension
		When this is >= 0, then a random number will be generate between 0 and sharedVariableSize.
		This variance will be shared between both dimension, bounded by min and max size

	# Output:
	- metadata.rooms: Array of Recti
**/
class RandomRoomPlacement extends MapgenProcess {
	/**
		the maximum number room to generate. -1 for no limit.
	**/
	public var maxRoom: Int = -1;

	/**
		define if the rooms generated is in the odd index.
		this is important for maze generations that have pillars in the odd index
		in the case of oddIndexRoom, size 1 is (1x1) and size 2 is (3x3)
	**/
	public var oddIndexRoom: Bool = false;

	/**
		maximum number of tries to place room.
	**/
	public var maxTries: Int = 50;

	/**
		minimum size for the room
	**/
	public var minRoomSize: Int = 1;

	/**
		variable size and various parameters
	**/
	public var maxRoomSize: Int = 1;

	/**
		if none of sharedSizeVariance are provided, then the size will be randomed between
		minRoomSize - maxRoomSize for both dimension

		if only min is provided, then the variance will be set to min
		if only max is provided, then the variance will be set to max
		if both are provided, then a variance will be randomed between min and max

		After the variance is set, then the amount will be randomly spread to both dimension

		if variance > ((maxRoomSize - minRoomSize) * 2) then it will have the same effect as setting it as -1
	**/
	/**
		min shared size variance
	**/
	public var sharedSizeVarianceMin: Int = -1;

	/**
		max shared size variance
	**/
	public var sharedSizeVarianceMax: Int = -1;

	/**
		the symbol used for room
	**/
	public var roomSymbol: String = ".";

	/**
		the symbol used for border
	**/
	public var borderSymbol: String = "x";

	/**
		Allowed symbols to generate the room in
	**/
	public var allowedSymbols: Array<String>;

	/**
		Whether or not the border wall around the room is changed to a different symbol
	**/
	public var setBorder: Bool = true;

	/** Process state data **/
	var tries: Int = 0;

	/**
		Tracks the number of room placed
	**/
	var placed: Int = 0;

	/**
		Store the list of rooms placed
	**/
	var rooms: Array<Recti>;

	public function new(map: MapgenMap) {
		super(map);
		this.allowedSymbols = ["#"];

		this.name = 'Random Room Placement';
		this.confPrefix = "rrp";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		if (configuration['${confPrefix}_maxRoom'] != null) {
			this.maxRoom = configuration['${confPrefix}_maxRoom'];
		}
		if (configuration['${confPrefix}_oddIndexRoom'] != null) {
			this.oddIndexRoom = configuration['${confPrefix}_oddIndexRoom'];
		}
		if (configuration['${confPrefix}_maxTries'] != null) {
			this.maxTries = configuration['${confPrefix}_maxTries'];
		}
		if (configuration['${confPrefix}_minRoomSize'] != null) {
			this.minRoomSize = configuration['${confPrefix}_minRoomSize'];
		}
		if (configuration['${confPrefix}_maxRoomSize'] != null) {
			this.maxRoomSize = configuration['${confPrefix}_maxRoomSize'];
		}
		if (configuration['${confPrefix}_setBorder'] != null) {
			this.setBorder = configuration['${confPrefix}_setBorder'];
		}
		if (configuration['${confPrefix}_sharedSizeVarianceMin'] != null) {
			this.sharedSizeVarianceMin = configuration['${confPrefix}_sharedSizeVarianceMin'];
		}
		if (configuration['${confPrefix}_sharedSizeVarianceMax'] != null) {
			this.sharedSizeVarianceMax = configuration['${confPrefix}_sharedSizeVarianceMax'];
		}
		this.rooms = [];
	}

	override public function step() {
		if (!canStep()) return;

		this.tries += 1;
		this.state = 'Tries: ${this.tries}/${this.maxTries}';

		final cells = this.map.cells;
		// get the placement
		var rect = getRoomPlacement(this.map.r);
		if (debug) Logger.debug('Placing Room: ${rect}', "RandomRoomPlacement");

		// check if we can place the rect here
		for (x in rect.xMin...rect.xMax + 1) {
			for (y in rect.yMin...rect.yMax + 1) {
				if (!cells.inBound(x, y)) return;
				if (!isCellAllowed(cells.get(x, y))) return;
			}
		}
		if (debug) Logger.debug('Placed Room: ${rect}', "RandomRoomPlacement");
		// now that we can place it, let's set the room
		this.rooms.push(rect);
		for (x in rect.xMin...rect.xMax + 1) {
			for (y in rect.yMin...rect.yMax + 1) {
				cells.set(x, y, this.roomSymbol);
			}
		}
		placed += 1;

		inline function setCellValue(x: Int, y: Int, symbol: String) {
			var v = cells.get(x, y);
			if (this.allowedSymbols.contains(v)) cells.set(x, y, symbol);
		}

		// if border is true, then we set the 1 cell around the cell to 'x'
		if (this.setBorder) {
			for (x in rect.xMin - 1...rect.xMax + 2) {
				setCellValue(x, rect.yMin - 1, this.borderSymbol);
				setCellValue(x, rect.yMax + 1, this.borderSymbol);
			}
			for (y in rect.yMin - 1...rect.yMax + 2) {
				setCellValue(rect.xMin - 1, y, this.borderSymbol);
				setCellValue(rect.xMax + 1, y, this.borderSymbol);
			}
		}
	}

	override public function canStep(): Bool {
		return (tries < maxTries && (maxRoom == -1 || placed < this.maxRoom));
	}

	override public function onFinish() {
		// update all the rooms into metadata
		if (this.map.metadata["rooms"] == null) {
			this.map.metadata["rooms"] = new Array<Recti>();
		}
		var rooms: Array<Recti> = this.map.metadata["rooms"];
		rooms.pushArray(this.rooms);
	}

	dynamic public function isCellAllowed(s: String): Bool {
		return this.allowedSymbols.contains(s);
	}

	/**
		Get a room size and get a random room placement location
		This doesn't check for valid room placement
	**/
	dynamic public function getRoomPlacement(r: hxd.Rand): Recti {
		var roomSize = getRoomSize(r);
		var xSize = roomSize.x;
		var ySize = roomSize.y;
		if (this.oddIndexRoom) {
			xSize = (xSize * 2) - 1;
			ySize = (ySize * 2) - 1;
			var startX: Int = r.randomWithinRange(1, this.map.cells.size.x - xSize);
			var startY: Int = r.randomWithinRange(1, this.map.cells.size.y - ySize);
			if (startX % 2 == 0) startX -= 1;
			if (startY % 2 == 0) startY -= 1;
			return [startX, startY, startX + xSize - 1, startY + ySize - 1];
		} else {
			var startX: Int = r.randomWithinRange(0, this.map.cells.size.x - xSize);
			var startY: Int = r.randomWithinRange(0, this.map.cells.size.y - ySize);
			return [startX, startY, startX + xSize - 1, startY + ySize - 1];
		}
	}

	/**
		Calculate and get a random room size
	**/
	dynamic public function getRoomSize(r: hxd.Rand): Point2i {
		var variance = -1;
		if (this.sharedSizeVarianceMin != -1 && this.sharedSizeVarianceMax != -1) {
			variance = r.randomWithinRange(this.sharedSizeVarianceMin, this.sharedSizeVarianceMax);
		} else if (this.sharedSizeVarianceMin != -1) {
			variance = this.sharedSizeVarianceMin;
		} else if (this.sharedSizeVarianceMax != -1) {
			variance = this.sharedSizeVarianceMax;
		}

		if (variance == -1) {
			// by default we will random the size between min and max
			var xSize = r.randomWithinRange(minRoomSize, maxRoomSize);
			var ySize = r.randomWithinRange(minRoomSize, maxRoomSize);
			return [xSize, ySize];
		} else {
			var varianceX = r.randomInt(Math.clampI(variance, 0, maxRoomSize - minRoomSize) + 1);
			var varianceY = Math.clampI(variance - varianceX, 0, maxRoomSize - minRoomSize);
			return [minRoomSize + varianceX, minRoomSize + varianceY];
		}
	}
}
