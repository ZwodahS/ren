package ren.mapgen.rooms;

import zf.Point2i;
import zf.Recti;
import zf.Direction;

using zf.ds.ArrayExtensions;

using ren.mapgen.Utils;

typedef Room = {
	rect: Recti,
}

/**
	# Overview
	Given a list of points, tunnel out and place a room.
	This will try all the points or until the maximum number of room is placed.

	# Configurations
**/
class PlaceRoomFromPoint extends MapgenProcess {
	/** Input **/
	/**
		The number of room to place
	**/
	public var maxRooms: Int = 1;

	/**
		The list of points to process
	**/
	public var points: Array<Point2i> = [];

	/**
		the minimum size for room
	**/
	public var minRoomSize: Int = 1;

	/**
		the maximum size for room
	**/
	public var maxRoomSize: Int = 1;

	/**
		the rect to place, without having to deal with minRoomSize
	**/
	public var roomSize: Point2i = null;

	/**
		the list of allowedSymbols that rooms and tunnels can be placed
	**/
	public var allowedSymbols: Array<String> = ["#"];

	/**
		the list of allowedSymbols that border can be placed
	**/
	public var borderAllowedSymbols: Array<String> = null;

	/**
		the room symbol to place
	**/
	public var roomSymbol: String = ".";

	/**
		the tunnel symbol to place
	**/
	public var tunnelSymbol: String = ".";

	/**
		min distance to tunnel
	**/
	public var minDistance: Int = 0;

	/**
		max distance to tunnel
	**/
	public var maxDistance: Int = 1;

	/**
		Border symbol
	**/
	public var borderSymbol: String = null;

	/**
		Border Size
	**/
	public var borderSize: Int = 1;

	/**
		check border is able to place.
		if true, the border will also be check against allowedSymbols
	**/
	public var checkBorder: Bool = false;

	public var checkTunnelAdjacentTile: Bool = false;

	public var placedRooms: Array<Room> = [];

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Placing Room from points";
		this.confPrefix = "placeroomfrompoint";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);

		// shuffle the points
		this.points.shuffle(map.r);
	}

	override public function step() {
		if (!canStep()) return;
		final cells = this.map.cells;

		// choose a point
		var pt = this.points.randomItem(this.map.r, true);

		// randomize the size of the rect if not provided
		var roomSize: Point2i = null;
		if (this.roomSize != null) {
			roomSize = this.roomSize.copy();
		} else {
			var width = this.map.r.randomWithinRange(this.minRoomSize, this.maxRoomSize);
			var height = this.map.r.randomWithinRange(this.minRoomSize, this.maxRoomSize);
			roomSize = [width, height];
		}

		// compute the longest distance so we can find rects to place
		final cache = cells.scanContinuousCells(this.allowedSymbols);

		final actualBorderSize = this.borderSymbol == null ? 0 : this.borderSize;

		// try random directions until one stick
		for (direction in Direction.allFourDirections(map.r)) {
			var curr = pt.copy();
			var availableDistances: Array<Int> = [];
			// starting from distance 0
			var distance = 0;
			var delta: Point2i = direction;
			while (distance <= maxDistance) {
				if (distance >= minDistance) {
					var availableRooms = cache.getValidRoomsFromPoint(curr, direction, roomSize,
						actualBorderSize);
					if (availableRooms.length > 0) availableDistances.push(distance);
				}
				// check if the next position can be tunneled
				curr += delta;
				distance += 1;
				if (!canTunnel(curr.x, curr.y, direction)) break;
			}
			if (availableDistances.length == 0) continue;

			// choose a random distance
			var chosenDistance = availableDistances.randomItem(this.map.r);
			// start tunneling
			var chosenPt = pt.clone();
			for (_ in 0...chosenDistance) {
				chosenPt += delta;
				cells.set(chosenPt.x, chosenPt.y, this.tunnelSymbol);
			}
			var availableRooms = cache.getValidRoomsFromPoint(chosenPt, direction, roomSize,
				actualBorderSize);
			// place room then connect the tunnel
			var room = availableRooms.randomItem(this.map.r);
			placeRoom(room);
			break;
		}
	}

	function placeRoom(room: Recti) {
		final cells = this.map.cells;

		inline function setBorder(x, y) {
			var v = cells.get(x, y);
			if (this.borderAllowedSymbols != null && !this.borderAllowedSymbols.contains(v)) return;
			cells.set(x, y, this.borderSymbol);
		}

		for (x in room.xMin...room.xMax + 1) {
			for (y in room.yMin...room.yMax + 1) {
				cells.set(x, y, roomSymbol);
			}
		}
		if (this.borderSymbol != null) {
			var r = room.clone();
			r.expand(this.borderSize);
			for (x in r.xMin...r.xMax + 1) {
				for (y in 0...this.borderSize) {
					setBorder(x, r.yMin + y);
					setBorder(x, r.yMax - y);
				}
			}

			for (y in r.yMin...r.yMax + 1) {
				for (x in 0...this.borderSize) {
					setBorder(r.xMin + x, y);
					setBorder(r.xMax - x, y);
				}
			}
		}
		this.placedRooms.push({rect: room.copy()});
	}

	dynamic public function canTunnel(x: Int, y: Int, moveDirection: Direction) {
		if (!this.allowedSymbols.contains(map.cells.get(x, y))) return false;
		if (this.checkTunnelAdjacentTile) {
			var cw: Point2i = moveDirection.rotateCW(2);
			var ccw: Point2i = moveDirection.rotateCCW(2);

			var cwPt: Point2i = [x, y];
			cwPt += cw;
			if (!this.allowedSymbols.contains(map.cells.get(cwPt.x, cwPt.y))) return false;

			var ccwPt: Point2i = [x, y];
			ccwPt += ccw;
			if (!this.allowedSymbols.contains(map.cells.get(ccwPt.x, ccwPt.y))) return false;
		}
		return true;
	}

	override public function canStep(): Bool {
		// @formatter:off
		return (
			this.points.length > 0 &&
			(this.maxRooms == -1 || this.placedRooms.length < this.maxRooms)
		);
	}
}
