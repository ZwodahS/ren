package ren.mapgen.rooms;

import zf.Recti;

using zf.MathExtensions;
using zf.ds.ArrayExtensions;

/**
	A binary space partition algorithms to generate rooms

	References:
	- http://www.roguebasin.com/index.php?title=Basic_BSP_Dungeon_generation

	Slight different from the article above, this does not shrink the room, nor
	does it connect the rooms with corridor.
	Instead, this process only generates the partitions.

	Wall will be placed if configured to do so.
	Wall will be placed not around the room, but in the dividing line

	Metadata will be added to the map for other process to use

	Min size for rooms will always be 1, and in the case of with wall,
	the minimum size to allow for split is 3,
	i.e split into 2 with a wall placed between 2 divided region.
	In the case that wall is not generated, then the min size to be divided will be 2.

	When specifying a min room size, any number below 1 will be treated as 1.

	Configuration:

	- configuration.bsp_numIterations - Number of time the regions is split
	- configuration.bsp_minSize - minimum size of the room
	- configuration.bsp_variance - the variable component of the room

	Output:

	- metadata.rooms: Array of Recti

**/
class BinarySpacePartition extends MapgenProcess {
	/**
		Number of time to split
	**/
	public var numIterations: Int = 1;

	/**
		If this is set, a wall will be placed around the divided region.
		If so, the room region will exclude the wall.
	**/
	public var wallSymbol: String = null;

	/**
		variance defines the variable part of the division.
		Default to 0, it will always divide equally, with odd size randomly assigned to either side.

		Assume no wall and we have a size of Y, and we set variance to X,
		both side will be given (Y-X)/2 length.
		A random number(Z) will be generated between 0 and X(inclusive) and
		one side will get Z and the other will get X-Z
		Therefore, one side will be (Y-X)/2 + Z while the other will be (Y-X)/2 + (X-Z)
	**/
	public var variance: Int = 0;

	public var minSize: Int = 1;

	var currIteration: Int = 0;
	var currentRegions: Array<Recti>;

	public function new(map: MapgenMap) {
		super(map);
		this.name = "Binary Space Partition";
	}

	override public function init(configuration: Map<String, Dynamic>) {
		super.init(configuration);
		if (configuration["bsp_numIterations"] != null) {
			this.numIterations = configuration["bsp_numIterations"];
		}
		if (configuration["bsp_minSize"] != null) {
			this.minSize = configuration["bsp_minSize"];
		}
		if (configuration["bsp_variance"] != null) {
			this.variance = configuration["bsp_variance"];
		}
		this.currentRegions = [];
		// set up the main region to divide
		this.currentRegions.push([0, 0, this.map.cells.size.x - 1, this.map.cells.size.y - 1]);
		if (this.minSize < 1) this.minSize = 1;
	}

	override public function canStep(): Bool {
		return this.currIteration < numIterations;
	}

	override public function step() {
		if (!canStep()) return;
		currIteration += 1;
		this.state = 'Iteration: ${currIteration}';

		var generatedRegions: Array<Recti> = [];
		/**
			For each region, attempt to split them
		**/
		for (region in currentRegions) {
			var newRegions = split(region);
			/**
				if there is no split, we add the original region back to the generated Regions
				we need this to update the metadata.

				Mon Jun 14 13:25:53 2021
				This might have unnecessary computation in cases where the number of iterations is high,
				but the regions can no longer be split. We don't really want to care about those cases,
				and make the code unnecessarily complicated and manage 2 set of regions.
			**/

			if (newRegions == null) {
				generatedRegions.push(region);
				continue;
			}
			// add each region into the generated regions.
			for (r in newRegions) {
				if (r != null) generatedRegions.push(r);
			}
		}
		// reset the regions to process
		this.currentRegions = generatedRegions;
	}

	override public function onFinish() {
		// on finish set the rooms metadata
		if (this.map.metadata["rooms"] == null) {
			this.map.metadata["rooms"] = new Array<Recti>();
		}
		var rooms: Array<Recti> = this.map.metadata["rooms"];
		rooms.pushArray(this.currentRegions);
	}

	function split(region: Recti): Array<Recti> {
		// check if we need to put wall
		var hasWall = this.wallSymbol != null;
		// calculate the minimum size we need to split
		var min = this.minSize < 1 ? 1 : this.minSize;
		min = min * 2 + (hasWall ? 1 : 0);

		if (debug) Logger.debug('Min Size: ${min}, Region size: ${region.width} ${region.height}',
			"BinarySpacePartition");

		// get the size depend on how we are splitting
		var size = 0;
		// 0 for horizontal, 1 for vertical
		var direction = 0;
		// check to make sure both direction can split
		if (region.width < min && region.height < min) {
			// if both side is too small to split, then we will just stop
			return null;
		} else if (region.width < min) {
			// if width is too small, we split vertical
			direction = 1;
			size = region.height;
		} else if (region.height < min) {
			// if height is too small, we split horizontal
			direction = 0;
			size = region.width;
		} else {
			direction = this.map.r.randomInt(2);
			size = direction == 0 ? region.width : region.height;
		}

		if (debug) Logger.debug('Direction: ${direction}, Size: ${size}', "BinarySpacePartition");

		// check how much actual variance can have
		var maxVariance = size - min;
		var v = Math.clampI(this.variance, 0, maxVariance);
		var fixed = size - v - (hasWall ? 1 : 0);
		// if the fixed size is odd, we add it more to variance
		if (fixed % 2 == 1) {
			v += 1;
			fixed -= 1;
		}
		Assert.assert(fixed % 2 == 0);
		fixed = Std.int(fixed / 2);

		// calculate the variable length
		var v0 = this.map.r.randomInt(v + 1);
		var v1 = v - v0;

		// calculate the 2 size
		var s0 = fixed + v0;
		var s1 = fixed + v1;

		final cells = this.map.cells;
		if (direction == 0) { // try split horizontally
			var rects = region.splitHorizontal(s0);
			if (this.wallSymbol != null) {
				for (y in region.yMin...region.yMax + 1) {
					cells.set(rects[1].xMin, y, this.wallSymbol);
				}
				// shift the right rect by 1
				rects[1].xMin += 1;
			}
			Assert.assert(rects[0].width == s0);
			Assert.assert(rects[1].width == s1);
			return rects;
		} else { // try split vertically
			var rects = region.splitVertical(s0);
			if (this.wallSymbol != null) {
				for (x in region.xMin...region.xMax + 1) {
					cells.set(x, rects[1].yMin, this.wallSymbol);
				}
				// shift the bottom rect by 1
				rects[1].yMin += 1;
			}
			Assert.assert(rects[0].height == s0);
			Assert.assert(rects[1].height == s1);
			return rects;
		}
	}
}
