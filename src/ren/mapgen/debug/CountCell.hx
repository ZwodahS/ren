package ren.mapgen.debug;

class CountCell extends FunctionProcess {
	var symbols: Array<String>;

	public function new(map: MapgenMap, symbols: Array<String>) {
		super(map, 'Counting Cell: ${symbols}');
		this.symbols = symbols;
	}

	override public function func(map: MapgenMap) {
		var count = 0;
		for (key => value in this.map.cells.iterate()) {
			if (this.symbols.contains(value)) count += 1;
		}
		Logger.debug('Counting ${this.symbols}, found: ${count}');
	}

	public static function make(symbols: Array<String>, map: MapgenMap): MapgenProcess {
		return new CountCell(map, symbols);
	}
}
