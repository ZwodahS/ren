package ren.mapgen.debug;

class MetadataPrinter extends FunctionProcess {
	public function new(map: MapgenMap) {
		super(map, "Metadata Printer");
	}

	override public function func(map: MapgenMap) {
		for (k => v in map.metadata) {
			Logger.debug('${k}', "MetadataPrinter");
			Logger.debug('${v}', "MetadataPrinter");
		}
	}

	public static function make(map: MapgenMap): MapgenProcess {
		return new MetadataPrinter(map);
	}
}
