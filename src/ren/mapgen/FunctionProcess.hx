package ren.mapgen;

/**
	Provide a single 1 step func that apply onto the whole map

	This can be used to update metadata, do coordinates transformation etc.
**/
class FunctionProcess extends MapgenProcess {
	public var isFinish: Bool = false;

	public function new(map: MapgenMap, name: String, func: MapgenMap->Void = null) {
		super(map);
		this.name = name;
		if (func != null) this.func = func;
	}

	override public function canStep(): Bool {
		return !this.isFinish;
	}

	override public function step() {
		if (!canStep()) return;
		this.func(this.map);
		this.isFinish = true;
	}

	dynamic public function func(map: MapgenMap) {}

	public static function make(name: String, func: MapgenMap->Void, map: MapgenMap) {
		return new FunctionProcess(map, name, func);
	}
}
