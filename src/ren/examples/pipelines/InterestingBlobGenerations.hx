package ren.examples.pipelines;

import ren.mapgen.*;
import ren.mapgen.maze.*;
import ren.mapgen.rooms.*;
import ren.mapgen.connectors.*;
import ren.mapgen.cells.*;
import ren.mapgen.debug.*;

class InterestingBlobGenerations {
	public static function blob1(): MapgenPipeline {
		var pipeline = new MapgenPipeline([
			Reset.clearAll.bind("#"),
			function(map: MapgenMap): MapgenProcess {
				var p = new RandomRoomPlacement(map);
				p.allowedSymbols = ["#"];
				p.roomSymbol = "r";
				p.maxRoom = 40;
				p.oddIndexRoom = true;
				p.maxTries = 50;
				p.minRoomSize = 3;
				p.maxRoomSize = 8;
				p.setBorder = true;
				p.sharedSizeVarianceMin = 5;
				return p;
			},
			SymbolReplacer.make.bind(["#" => "."]),
			SymbolReplacer.make.bind(["x" => "#"]),
			function(map: MapgenMap): MapgenProcess {
				var p = new SimpleNeighboursAutomata(map);
				p.confPrefix = "noise";
				p.deadLogic = [false, true, true, true, false, true, true, true, false];
				p.aliveLogic = [false, false, false, false, false, true, true, true, false];
				p.flipProbability = 50;
				p.maxIterations = 5;
				p.edgeValue = true;
				return p;
			},
			SymbolReplacer.make.bind(["r" => "."]),
			function(map: MapgenMap): MapgenProcess {
				var p = new SimpleNeighboursAutomata(map);
				p.confPrefix = "caving";
				p.deadLogic = [false, false, false, false, true, true, true, true, true];
				p.aliveLogic = [false, false, false, false, true, true, true, true, true];
				p.flipProbability = 50;
				p.maxIterations = 5;
				p.edgeValue = true;
				return p;
			},
			SymbolReplacer.make.bind(["#" => ".", "." => "#"]),
		]);
		return pipeline;
	}
}
