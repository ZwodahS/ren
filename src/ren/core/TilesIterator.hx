package ren.core;

import zf.Point2i;

using zf.RandExtensions;

/**
	iterate tiles from a center point, slowly outwards, until it covers all the tiles in the level.
**/
class TilesIterator {
	var level: Level;
	var center: Point2i;
	var r: hxd.Rand;

	var currentDistance: Int = 0;
	var current: Array<Tile>;
	var finished: Bool = false;
	var minDistance: Int = 0;
	var maxDistance: Int = -1;

	public function new(level: Level, center: Point2i, r: hxd.Rand, minDistance: Int = 0,
			maxDistance: Int = -1) {
		this.level = level;
		this.center = center;
		this.r = r;
		this.current = [];
		this.currentDistance = minDistance;
		this.maxDistance = maxDistance;
	}

	function initNext() {
		var distance = this.currentDistance++;
		if (maxDistance != -1 && distance > this.maxDistance) {
			finished = true;
			return;
		}
		if (distance == 0) {
			var tile = this.level.getTile(center.x, center.y);
			if (tile != null) current.push(tile);
		} else {
			for (x in (center.x - distance)...(center.x + distance + 1)) {
				// push center.y - distance and center.y + distance
				var tile = this.level.getTile(x, center.y - distance);
				if (tile != null) current.push(tile);

				tile = this.level.getTile(x, center.y + distance);
				if (tile != null) current.push(tile);
			}

			for (y in (center.y - distance + 1)...(center.y + distance)) {
				// push center.x - distance + 1 and center.x + distance
				// however, don't repeat y = 0 because we already did it in the previous loop
				// similar, don't add y = center.y + distance - 1
				var tile = this.level.getTile(center.x - distance, y);
				if (tile != null) current.push(tile);

				tile = this.level.getTile(center.x + distance, y);
				if (tile != null) current.push(tile);
			}
		}
		// if there is no tiles after the above checks, we are done
		if (this.current.length == 0) finished = true;
	}

	public function hasNext(): Bool {
		if (this.current.length == 0 && !this.finished) initNext();
		return !this.finished;
	}

	public function next(): {key: Point2i, value: Tile} {
		var next = this.r.randomPop(this.current);
		return {key: next.position.copy(), value: next};
	}
}
