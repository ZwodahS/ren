package ren.core.systems;

import ren.core.messages.EntityTakeAction;
import ren.core.messages.EntityTakingAction;

/**
	ActionSystem is one of the core system that deals with action performed by entity.
	This system does not handles the turn and that should be handled by a separate turn system.

	@see ren.core.Action

	Also read the docs for ActionSystem and TurnSystem in Architecture.md
**/
class ActionSystem extends zf.ecs.System {
	var world: World;
	var dispatcher: zf.MessageDispatcher;

	public function new() {
		super();
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);
		this.dispatcher = world.dispatcher;

		// listen to any action that is dispatched.
		// @:listen ActionSystem Action 0
		this.dispatcher.listen(Action.MessageType, function(message: zf.Message) {
			var action = cast(message, Action);
			performAction(action);
		}, 0);
	}

	/**
		This can also be called directly instead of using the dispatcher.
	**/
	public function performAction(action: Action): Bool {
#if debug_action
		haxe.Log.trace('[ActionSystem] Performing Action: ${action}', null);
		var t0 = Sys.time();
		var t1: Float = 0;
#end
		// dispatch the pre-action event
		this.dispatcher.dispatch(new EntityTakingAction(action.entity, action));
		var success = action.perform(function(actionResult: ActionResult) {
#if debug_action
			if (t1 == 0) {
				t1 = Sys.time();
				var time = (t1 - t0) * 100;
				haxe.Log.trace('[ActionSystem] Performed Action: ${action} [${time}ms]', null);
			}
#end
			onActionFinish(action, action.entity, actionResult);
		});
#if debug_action
		if (t1 == 0) {
			t1 = Sys.time();
			var time = (t1 - t0) * 100;
			haxe.Log.trace('[ActionSystem] Performed Action: ${action} [${time}ms]', null);
		}
#end
		return success;
	}

	function onActionFinish(action: Action, entity: Entity, actionResult: ActionResult) {
		if (actionResult == null) return;
		if (entity == null) return;
		this.dispatcher.dispatch(new EntityTakeAction(entity, action, actionResult));
	}
}
