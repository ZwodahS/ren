package ren.core;

/**
	Mark the visibility of a tile
**/
enum abstract TileVisibility(String) from String to String {
	var Hidden = "hidden";
	var Seen = "seen";
	var Visible = "visible";

	public function intValue(): Int {
		switch (this) {
			case Hidden:
				return 10;
			case Seen:
				return 5;
			case Visible:
				return 0;
		}
		return 0;
	}
}
