package ren.core;

import zf.Assert;
import zf.Direction;
import zf.Point2i;
import zf.ds.Vector2D;
import zf.ecs.EntityMap;
import zf.ecs.EntityMap.ReadOnlyEntityMap;

import ren.core.components.LocationComponent;

/**
	Tile is a data container for a grid on the map
**/
typedef TileType = String;

class Tile {
	public static final MaxDistance = 0x10000000;

	/**
		Global Counter generation for id.
	**/
	public static var IdCounter: zf.IntCounter = new zf.IntCounter.SimpleIntCounter();

	/**
		Main Identifier for this tile.
	**/
	public var id(default, null): Int;

	/**
		The type representing the tile
	**/
	public var type: TileType;

	/**
		The xy position of the tile on the level.
	**/
	public var position(default, null): Point2i;

	/**
		Private map of all entities.
	**/
	var eMap(default, null): EntityMap<Entity>;

	/**
		The entities that are in the tile.
		This is only read only.
	**/
	public var entities(get, never): ReadOnlyEntityMap<Entity>;

	public function get_entities(): ReadOnlyEntityMap<Entity> {
		return eMap;
	}

	/**
		Store the level that the tile is in
		Should only be set by Level
	**/
	public var level: Level;

	/**
		A metadata map to store anything.
	**/
	public var metadata: Map<String, Dynamic>;

	/**
		Tile visibility relative to the player.
		Sat Jul 10 16:18:16 2021
		This value is currently a test, not sure if this will be stored here long term.
	**/
	public var visibility: TileVisibility = Hidden;

	/**
		Create a new tile object

		@param x the x position of this tile on the level
		@parma y the y position of this tile on the level
		@param type the type representing the tile
		@param id the int to override the default id counter.
	**/
	public function new(x: Int, y: Int, type: TileType, id: Null<Int> = null) {
		this.eMap = new EntityMap<Entity>();
		this.type = type;
		this.id = id != null ? id : IdCounter.getNextInt();
		this.position = [x, y];
		this.metadata = new Map<String, Dynamic>();
		this.lightSources = new Map<Int, Int>();
		init();
	}

	function init() {}

	/**
		Add Entity to the tile.

		@param e entity to add

		@return true if successfully added, false if fail or entity already exists.
	**/
	public function addEntity(e: Entity): Bool {
		if (!canAddEntity(e)) return false;

		var location = LocationComponent.get(e);

		if (location.tile != null) {
			// we should never call add Entity when the entity is in another level
			Assert.assert(location.level == this.level);
			location.tile.internalRemoveEntity(e);
		}
		internalAddEntity(e, location);

		return true;
	}

	function internalAddEntity(e: Entity, ?lc: LocationComponent) {
		if (lc == null) lc = LocationComponent.get(e);

		this.eMap.add(e);
		lc.tile = this;
		if (lc.position == null) {
			lc.position = this.position.copy();
		} else {
			lc.position.update(this.position);
		}
	}

	public function placeEntity(e: Entity): Bool {
		if (this.level == null) return false;
		return this.level.placeEntity(e, this.position);
	}

	/**
		Remove Entity from the tile

		@param e entity to remove

		@return true if successfully removed, false if fail or entity does not exists.
	**/
	public function removeEntity(e: Entity, ?lc: LocationComponent): Bool {
		if (!eMap.exists(e)) return false;

		internalRemoveEntity(e);
		// assert that this is the same. Outside of debugging this will be removed.
		Assert.assert(lc == null || lc == LocationComponent.get(e));
		lc = lc == null ? LocationComponent.get(e) : lc;
		Assert.assert(lc != null);

		lc.tile = null;
		lc.position = null;
		lc.level = null;

		return true;
	}

	inline public function hasEntity(e: Entity): Bool {
		return this.entities.exists(e);
	}

	/**
		Internally remove entity from this tile.

		This is to be used by other Tile object for when entity is moved added into a tile
		before removing from the old tile.

		Override this to add more functionality.
	**/
	function internalRemoveEntity(e: Entity) {
		this.eMap.remove(e);
	}

	/**
		Check if this entity can be added to this tile.

		@param e the entity to check.

		@return true if the entity can be added, false otherwise.
	**/
	public function canAddEntity(e: Entity): Bool {
		return true;
	}

	public function toString(): String {
		return '(Tile:${id},${position.x},${position.y})';
	}

	public function getTileInDirection(direction: Direction): Tile {
		if (this.level == null) return null;
		var targetPosition = this.position + direction;
		return this.level.getTile(targetPosition.x, targetPosition.y);
	}

	public function getAdjacentTiles(): Array<Tile> {
		return this.level.map.getAdjacent(this.position.x, this.position.y);
	}

	public function getTilesAround(grid: Vector2D<Tile> = null): Vector2D<Tile> {
		if (grid == null) grid = new Vector2D<Tile>([3, 3], null);
		if (this.level == null) return grid;
		for (y in -1...2) {
			for (x in -1...2) {
				grid.set(x + 1, y + 1, this.level.getTile(this.position.x + x, this.position.y + y));
			}
		}
		return grid;
	}

	public function distanceFrom(tile: Tile): Int {
		if (tile.level != this.level) return MaxDistance;
		return tile.level.distance(this.position, tile.position);
	}

	/** Various methods and function to handle light levels and light sources **/
	/**
		Store the light level of the tile.
		This is used together with visibility to render the fog and decide if entities should be rendered
		to the player.  This can also be used by systems for logic.
		Note: unlike visibility which is relative to the player, lightLevel is player-agnostic.

		No assumption is made for the value of light level.
		The max value for light level should be set in TileBasedRenderSystem or other systems.

		Tue 13:15:19 17 Aug 2021
		This value is also a temporary value here. Still figuring out if there is a better way to store this.
	**/
	public var baseLightLevel: Int = 0;

	public var lightLevel(get, never): Int;

	var _cacheLightLevel: Null<Int> = null;

	public function get_lightLevel(): Int {
		if (_cacheLightLevel == null) {
			this._cacheLightLevel = this.baseLightLevel;
			for (_ => l in this.lightSources) {
				this._cacheLightLevel += l;
			}
		}
		return this._cacheLightLevel;
	}

	// stores the entity id -> light level provided.
	var lightSources: Map<Int, Int>;

	public function addLightSource(entity: Entity, amount: Int) {
		this.lightSources[entity.id] = amount;
		this._cacheLightLevel = null;
	}

	public function removeLightSource(entity: Entity) {
		this.lightSources.remove(entity.id);
		this._cacheLightLevel = null;
	}

	/**
		Returns the entity id of all the light sources
	**/
	public function getLightSources(): Array<Int> {
		return [for (id => _ in lightSources) id];
	}

	/** end of light level / light source code **/
}
