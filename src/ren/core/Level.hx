package ren.core;

import astar.Graph;
import astar.types.Direction in ASDirection;
import astar.MovementDirection;

import zf.Assert;
import zf.ecs.EntityMap;
import zf.ds.Vector2D;
import zf.Point2i;
import zf.LinePoint2i;
import zf.Line2i;
import zf.MathUtils as MU;
import zf.Direction;

using zf.ds.MapExtensions;
using zf.RandExtensions;

import ren.core.components.LocationComponent;

typedef CostFunc = (Tile->Int);

enum BlockType {
	NoBlock; // not blocked by diagonal
	FullyBlocked; // fully blocked by 1 diagonal tile
	PartiallyBlocked; // Partially blocked by 1 diagonal tile, requires 2 tile to fully block it.
}

/**
	Level is a data container for a 2D finite floor.
	For now we don't deal with infinite coordinates.
**/
class Level {
	public static var IdCounter: zf.IntCounter = new zf.IntCounter.SimpleIntCounter();

	/**
		the identifier for the level.
		This is only set during creation
	**/
	public var id(default, null): Int;

	/**
		The readonly grid that can be used to get data
	**/
	public var map: ReadOnlyVector2D<Tile>;

	/**
		The internal 2D Map
	**/
	var _map: Vector2D<Tile>;

	/**
		size of the level
	**/
	public var size(get, never): Point2i;

	public function get_size(): Point2i {
		return [this.map.size.x, this.map.size.y];
	}

	/**
		This is a cache for all the entities that is in the level.
		Do not modify. This is a cache
	**/
	public var entities(get, null): ReadOnlyEntityMap<Entity>;

	public function get_entities(): ReadOnlyEntityMap<Entity> {
		return this._entities;
	}

	var _entities: EntityMap<Entity>;

	/** Path finding related data **/
	/**
		Sun May 23 14:59:36 2021
		The previous implementation of dynamic cost function for individiual entity is very slow.
		Considering that the cost of each tile don't change often, and even when they do change,
		only a few will change at any one time, it is better to generate a fixed cost array and
		update the array if there are changes. However, there is a need to handle different mode
		of movement, e.g. Flying vs Walking

		Because of this, a single cost array is not sufficient, hence we will be handling this
		by having multiple cost functions and caches.

		This is broken down into a few parts
		1.	graphTileCostsFunction stores an id -> CostFunc.
		2.	graphDefaultCostFunc stores the id of the cost func to use if one is not provided or
				the one provided is not valid.
		3.	graphArray stores the cached values for the costs of all the tiles calculated using
				the various cost functions.
		The graphcost however, will be the same.
	**/
	// graph for path finding on this level
	var graph: Graph;

	var graphTileCostsFunction: Map<String, CostFunc>;
	var graphDefaultCostFunc: String = "default";
	var graphArray: Map<String, Array<Int>>;
	// this need to be set to 8 way cost
	var graphCosts = [
		0 => [N => 1., S => 1., W => 1., E => 1., NE => 1.3, NW => 1.3, SE => 1.3, SW => 1.3],
	];
	// set the movement direction, default to FourWay
	var movementDirection: MovementDirection = FourWay;

	/**
		create a new level

		@param size the size of the level
		@param id an id to overwrite the default id generation
	**/
	public function new(size: Point2i, id: Null<Int> = null,
			movementDirection: MovementDirection = FourWay) {
		this._map = new Vector2D<Tile>(size, null);
		this.movementDirection = movementDirection;
		this.map = this._map;

		if (id == null) {
			this.id = IdCounter.getNextInt();
		} else {
			this.id = id;
		}
		this._entities = new EntityMap<Entity>();
		this.graphTileCostsFunction = new Map<String, Tile->Int>();
		init();
		setupGraphs();
	}

	function setupGraphs() {
		if (this.graphTileCostsFunction.isEmpty()) return;
		Assert.assert(this.graphTileCostsFunction[graphDefaultCostFunc] != null,
			"Default Cost Function not provided");
		this.graph = new Graph(size.x, size.y, this.movementDirection);
		this.graphArray = new Map<String, Array<Int>>();
		for (id => func in this.graphTileCostsFunction) {
			this.graphArray[id] = [for (i in 0...(size.x * size.y)) 0];
		}
		this.graph.setWorld(this.graphArray[graphDefaultCostFunc]);
		this.graph.setCosts(graphCosts);
	}

	function init() {}

	/**
		place an entity into this position on this level

		@param e the entity to be placed
		@param position the position to place into

		@return true if successful, false otherwise
	**/
	public function placeEntity(e: Entity, position: Point2i): Bool {
		if (!canPlaceEntity(e, position)) return false;
		var tile = this.map.get(position.x, position.y);
		if (tile == null) return false;
		if (!tile.addEntity(e)) return false;

		var location = LocationComponent.get(e);
		location.level = this;

		if (!this._entities.exists(e)) {
			this._entities.add(e);
			onEntityAdded(e, tile);
		}

		return true;
	}

	function onEntityAdded(e: Entity, tile: Tile) {}

	/**
		check if an entity can be placed in this position

		@param e the entity to be placed
		@param position the position to be placed into

		@return true if possible, false otherwise
	**/
	public function canPlaceEntity(e: Entity, position: Point2i): Bool {
		var tile = this.map.get(position.x, position.y);
		if (tile == null) return false;
		var b = tile.canAddEntity(e);
		return b;
	}

	/**
		Move entity to a new position

		This only work if the entity is already currently on the level.

		@param entity the Entity to move
		@param position the position to move to
		@param lc the location component of the entity.
		@return true if the move is successful, false otherwise
	**/
	public function moveEntity(entity: Entity, position: Point2i, ?lc: LocationComponent): Bool {
		if (lc == null) lc = LocationComponent.get(entity);
		if (lc == null || lc.level != this) return false;

		var toTile = this.map.get(position.x, position.y);
		if (toTile == null) return false;

		var fromTile = lc.tile;

		toTile.addEntity(entity);
		onEntityMoved(entity, fromTile, toTile);
		Assert.assert(!fromTile.hasEntity(entity));
		Assert.assert(toTile.hasEntity(entity));
		return true;
	}

	function onEntityMoved(entity: Entity, fromTile: Tile, toTile: Tile) {}

	/**
		Remove entity from this level

		@param entity the Entity to remove
		@param lc the location component of the entity
		@return true if entity is on the level, false otherwise
	**/
	public function removeEntity(entity: Entity, ?lc: LocationComponent): Bool {
		if (lc == null) lc = LocationComponent.get(entity);
		if (lc == null || lc.level != this || lc.tile == null) return false;

		var fromTile = lc.tile;
		lc.tile.removeEntity(entity, lc);

		if (this._entities.exists(entity)) {
			this._entities.remove(entity);
			onEntityRemoved(entity, fromTile);
		}

		return true;
	}

	function onEntityRemoved(e: Entity, tile: Tile) {}

	/**
		Get a random position on the level to place an entity

		@param e Entity to be placed
		@param r the randomizer
		@param maxTries the maximum number of tries to place the entity

		@return the random position to be placed, null if tries exceed.
	**/
	public function getRandomPlaceablePosition(e: Entity, r: hxd.Rand = null, maxTries: Int = 10): Point2i {
		r = r == null ? new hxd.Rand(Random.int(0, Std.int(Math.pow(2, 16)))) : r;
		var position: Point2i = null;
		var tries = 0;
		var size = this.size;

		// TODO: while this is easy to implement, we need a better one that spawn in a 3x3 or 5x5 area.

		while (position == null && tries < maxTries) {
			tries += 1;
			position = [r.randomInt(size.x), r.randomInt(size.y)];
			if (canPlaceEntity(e, position)) {
				zf.Logger.debug('Placeable after ${tries} tries');
				return position;
			}
			position = null;
		}
		return position;
	}

	/**
		Get a Tile on the level

		@param x the x position
		@param y the y position

		@return the tile. null if not in bound
	**/
	inline public function getTile(x: Int, y: Int): Tile {
		return this._map.get(x, y);
	}

	/**
		Set the tile

		@param x the x position
		@param y the y position

		@return previous tile in the position
	**/
	public function setTile(x: Int, y: Int, t: Tile): Tile {
		var prevTile = this._map.get(x, y);
		onTileUnset(prevTile, x, y);
		this._map.set(x, y, t);
		onTileSet(t, x, y);
		return prevTile;
	}

	function onTileUnset(t: Tile, x: Int, y: Int) {
		if (t == null) return;
		t.level = null;
		for (id => e in t.entities) {
			var lc = LocationComponent.get(e);
			if (lc == null) continue;
			lc.level = null;
			this._entities.remove(e);
		}
	}

	function onTileSet(t: Tile, x: Int, y: Int) {
		if (t == null) return;
		t.level = this;
		for (id => e in t.entities) {
			var lc = LocationComponent.get(e);
			if (lc == null) continue;
			lc.level = this;
			this._entities.add(e);
		}
	}

	/**
		The distance between 2 points on the level
	**/
	public function distance(pt1: Point2i, pt2: Point2i): Int {
		var diff = pt1 - pt2;
		return MU.iMax([MU.iAbs(diff.x), MU.iAbs(diff.y)]);
	}

	public function toString(): String {
		return '(Level:${id})';
	}

	/** Path finding related code **/
	/**
		Convert a position to the a index in graphArray

		@param position the position to convert
		@return return the graphArray index
	**/
	inline function apos(position: Point2i): Int {
		return position.y * this.map.size.x + position.x;
	}

	/**
		Find a path from start to end

		@param start the start position to search path from
		@param end the end position to search path to
		@return Array of points to end, null if path not found
	**/
	public function pathTo(start: Point2i, end: Point2i, graphId: String): Array<Point2i> {
		var p1 = apos(start);
		var p2 = apos(end);

		var ga = getGraphArray(graphId);

		// cache the value of start and end and set it to 0
		var o1 = ga[p1];
		var o2 = ga[p2];
		ga[p1] = 0;
		ga[p2] = 0;

		// resync the graph
		resyncWorld(ga);

		// get the path
		var result = this.graph.solve(start.x, start.y, end.x, end.y);
		var path: Array<Point2i> = null;
		if (result.result == Solved) {
			path = [for (p in result.path) [p.x, p.y]];
		}

		// set back the ga values
		ga[p1] = o1;
		ga[p2] = o2;

		return path;
	}

	/**
		draw a from start to line
		this uses getCost to decide if it is passable

		@param start the start position. start position's cost is never checked
		@param end the end position. end position's cost is never checked
		@param costFunc the cost function for the line check. Cost Func should return 1 when blocked, 0 when not
		@return 2 x LinePoint2i, one for each direction.
			The first will be forward direction, the second will be from end to start.
			If there is no line, the value will be null.
			This will always return an array with size 2
	**/
	public function lineTo(start: Point2i, end: Point2i, costFunc: CostFunc,
			diagonalBlock: BlockType = NoBlock): Array<LinePoint2i> {
		var lines = Line2i.getLinesBothDirection(start, end);
		var output = [null, null];
		var l0 = [for (i in lines[0]) i];
		var l1 = [for (i in lines[1]) i];
		if (checkLine(l0, costFunc, diagonalBlock)) output[0] = l0;
		if (checkLine(l1, costFunc, diagonalBlock)) output[1] = l1;
		return output;
	}

	function isDiagonalBlock(prev: Point2i, p: Point2i, costFunc: CostFunc, diagonalBlock: BlockType): Bool {
		var d: Direction = p - prev;
		var split = d.split();
		if (split.length == 2) {
			var blockCount = 0;
			for (s in split) {
				var adjP = prev + s;
				if (costFunc(this.map.get(adjP.x, adjP.y)) == 1) blockCount += 1;
			}
			if (blockCount == 2 || (blockCount == 1 && diagonalBlock == FullyBlocked)) return true;
		}
		return false;
	}

	function checkLine(line: LinePoint2i, costFunc: CostFunc, diagonalBlock: BlockType): Bool {
		// if line length is 2, then we just return true
		// this is because the entity is beside each other
		if (line.length == 2) return true;

		var prev = line[0];
		for (ind in 1...line.length - 1) {
			var p = line[ind];
			if (costFunc(this.map.get(p.x, p.y)) == 1) return false;
			if (diagonalBlock != NoBlock && isDiagonalBlock(prev, p, costFunc, diagonalBlock)) return false;
			prev = p;
		}

		if (diagonalBlock != NoBlock) {
			var p = line[line.length - 1]; // check the last diagonal
			if (isDiagonalBlock(prev, p, costFunc, diagonalBlock)) return false;
		}
		return true;
	}

	inline function resyncWorld(g: Array<Int>) {
		this.graph.setWorld(g);
		this.graph.setCosts(graphCosts);
	}

	inline function getGraphArray(graphId: String, useDefault: Bool = true) {
		var ga = this.graphArray[graphId];
		if (ga == null && useDefault) ga = this.graphArray[graphDefaultCostFunc];
		Assert.assert(ga != null || useDefault == false);
		return ga;
	}

	/**
		Force a recalculation of all graphs cost on the level
		This should be done once when all the entities are loaded, and just before it is added to the world.
	**/
	public function recalculateAllGraphs() {
		for (id in this.graphTileCostsFunction.keys()) {
			recalculateGraph(id);
		}
	}

	public function recalculateGraph(id: String) {
		var ga = getGraphArray(id, false);
		if (ga == null) return;
		var costFunc = this.graphTileCostsFunction[id];
		Assert.assert(costFunc != null);
		var p = 0;
		for (y in 0...this.map.size.y) {
			for (x in 0...this.map.size.x) {
				ga[p++] = costFunc(this.map.get(x, y));
			}
		}
	}

	public function recalculatePosition(position: Point2i) {
		var p = apos(position);
		var tile = this.map.get(position.x, position.y);
		if (tile == null) return;
		for (id => costFunc in this.graphTileCostsFunction) {
			var ga = getGraphArray(id, false);
			Assert.assert(ga != null);
			ga[p] = costFunc(tile);
		}
	}

	/** Iterator functions **/
	/**
		Level Iterators
	**/
	public function iterateLevelFromCenter(center: Point2i, r: hxd.Rand, minDistance: Int = 1,
			maxDistance: Int = -1): TilesIterator {
		return new TilesIterator(this, center, r, minDistance, maxDistance);
	}

	/** Utility Functions **/
	/**
		Map the tile from 1 value to another value and return
	**/
	public function mapTile<T>(f: Tile->T, nullValue: T): Vector2D<T> {
		var v = new Vector2D<T>(this.map.size, nullValue);
		for (pt => t in this.map.iterateYX()) {
			v.set(pt.x, pt.y, f(t));
		}
		return v;
	}

	public function getRandomPosition(r: hxd.Rand): Point2i {
		return new Point2i(r.randomInt(this.map.size.x), r.randomInt(this.map.size.y));
	}

	public function getRandomTile(r: hxd.Rand, f: Tile->Bool = null): Tile {
		var position = getRandomPosition(r);
		for (pt => t in iterateLevelFromCenter(position, r, 0)) {
			if (f == null || f(t)) return t;
		}
		return null;
	}
}
