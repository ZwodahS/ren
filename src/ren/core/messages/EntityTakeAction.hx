package ren.core.messages;

import ren.core.Entity;

class EntityTakeAction extends zf.Message {
	public static final MessageType = "EntityTakeAction";

	override public function get_type(): String {
		return MessageType;
	}

	public var action: Action;
	public var entity: Entity;
	public var actionResult: ActionResult;

	public function new(entity: Entity, action: Action, actionResult: ActionResult) {
		super();
		this.entity = entity;
		this.action = action;
		this.actionResult = actionResult;
	}

	override public function toString(): String {
		return '[m:EntityTakeAction: ${entity}:${action}->${this.actionResult}]';
	}
}
