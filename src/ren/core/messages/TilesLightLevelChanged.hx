package ren.core.messages;

class TilesLightLevelChanged extends zf.Message {
	public static final MessageType = "TilesLightLevelChanged";

	public var tiles: Array<ren.core.Tile>;

	public function new(tiles: Array<ren.core.Tile>) {
		super(MessageType);
		this.tiles = tiles;
	}
}
