package ren.core.messages;

import ren.core.Entity;
import ren.core.Tile;

class EntityDestroyed extends zf.Message {
	public static final MessageType = "EntityDestroyed";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;

	/**
		The tile that the entity was on before it was destroyed.
	**/
	public var tile: Tile;

	public function new(entity: Entity, tile: Tile) {
		super();
		this.entity = entity;
		this.tile = tile;
	}

	override public function toString(): String {
		return '[m:EntityDestroyed: ${entity}|${tile}>';
	}
}
