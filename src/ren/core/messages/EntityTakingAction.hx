package ren.core.messages;

import ren.core.Entity;

class EntityTakingAction extends zf.Message {
	public static final MessageType = "EntityTakingAction";

	override public function get_type(): String {
		return MessageType;
	}

	public var action: Action;
	public var entity: Entity;

	public function new(entity: Entity, action: Action) {
		super();
		this.entity = entity;
		this.action = action;
	}

	override public function toString(): String {
		return '[m:EntityTakingAction: ${entity}:${action}]';
	}
}
