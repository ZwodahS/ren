package ren.core.messages;

class EntityTurnEnd extends zf.Message {
	public static final MessageType = "EntityTurnEnd";

	public var entity: Entity;

	public function new(entity: Entity) {
		super(MessageType);
		this.entity = entity;
	}

	override public function toString(): String {
		return '[m:EntityTurnEnd: ${entity}]';
	}
}
