package ren.core.messages;

import ren.core.Entity;

/**
	A message dispatched by TurnSystem to inform that it is an Entity's Active Turn

	@see TurnSystem
**/
class EntityActiveTurn extends zf.Message {
	public static final MessageType = "EntityActiveTurn";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;

	public function new(entity: Entity) {
		super();
		this.entity = entity;
	}

	override public function toString(): String {
		return '[m:EntityActiveTurn: ${entity}]';
	}
}
