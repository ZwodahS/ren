package ren.core.messages;

import ren.core.Entity;

class PlayerSet extends ValueChanged<Entity> {
	public static final MessageType = "PlayerSet";

	override public function get_type(): String {
		return MessageType;
	}

	override public function toString(): String {
		return '[m:PlayerSet]';
	}
}
