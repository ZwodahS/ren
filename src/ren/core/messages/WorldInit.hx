package ren.core.messages;

class WorldInit extends zf.Message {
	public static final MessageType = "WorldInit";

	override public function get_type(): String {
		return MessageType;
	}

	override public function toString(): String {
		return '[m:WorldInit]';
	}
}
