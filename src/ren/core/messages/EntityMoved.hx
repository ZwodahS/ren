package ren.core.messages;

import zf.Point2i;

import ren.core.Level;
import ren.core.Entity;

/**
	A message for informing that an entity have moved.
**/
class EntityMoved extends zf.Message {
	public static final MessageType = "EntityMoved";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;
	public var oldLevel: Level;
	public var newLevel: Level;
	public var oldPosition: Point2i;
	public var newPosition: Point2i;

	public function new(entity: Entity, oldLevel: Level, oldPosition: Point2i, newLevel: Level,
			newPosition: Point2i) {
		super();
		this.entity = entity;
		this.oldLevel = oldLevel;
		this.oldPosition = oldPosition;
		this.newLevel = newLevel;
		this.newPosition = newPosition;
	}

	override public function toString(): String {
		return '[m:EntityMoved: ${entity}|${oldLevel}:${oldPosition}->${newLevel}:${newPosition}]';
	}
}
