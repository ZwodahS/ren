package ren.core.messages;

class EntityTurnStart extends zf.Message {
	public static final MessageType = "EntityTurnStart";

	public var entity: Entity;

	// if any system want to disrupt the entity turn, set this to true.
	public var disrupted: Bool = false;

	public function new(entity: Entity) {
		super(MessageType);
		this.entity = entity;
	}

	override public function toString(): String {
		return '[m:EntityTurnStart: ${entity}]';
	}
}
