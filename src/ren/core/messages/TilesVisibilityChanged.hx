package ren.core.messages;

class TilesVisibilityChanged extends zf.Message {
	public static final MessageType = "TilesVisibilityChanged";

	public var tiles: Array<ren.core.Tile>;

	public function new(tiles: Array<ren.core.Tile>) {
		super(MessageType);
		this.tiles = tiles;
	}
}
