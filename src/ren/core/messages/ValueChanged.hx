package ren.core.messages;

class ValueChanged<T> extends zf.Message {
	public static final MessageType = "ValueChanged<T>";

	override public function get_type(): String {
		return MessageType;
	}

	public var oldValue: T;
	public var newValue: T;

	public function new(oldValue: T, newValue: T) {
		super();
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	override public function toString(): String {
		return '[m:${this.type}: ${this.oldValue}->${this.newValue}]';
	}
}
