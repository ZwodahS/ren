package ren.core.messages;

import ren.core.Entity;
import ren.core.Tile;

/**
	This message can be used by any system to denote an entity is marked to be destroy.
	This allow some type of generic clean up function to only happened when the system update.

	For example, there can be a system "EntityDestroySystem" that listen to this message
	and store the entity, and call world.destroyEntity when update of the system is called.
**/
class EntityMarkedForDestroy extends zf.Message {
	public static final MessageType = "EntityMarkedForDestroy";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;

	public function new(entity: Entity) {
		super();
		this.entity = entity;
	}

	override public function toString(): String {
		return '[m:EntityMarkedForDestroy: ${entity}>';
	}
}
