package ren.core;

/**
	Action is the parent class of all action that changes the state of the world.

	Ideally, all mutation of world state should go through Action and the ActionSystem.

	This also extends Message, which allow it to be dispatched via the MessageDispatcher.
	Child class should not override the get_type or the type property of message.
	This will greatly change the behavior of ActionSystem.
**/
class Action extends zf.Message {
	public static final MessageType = "Action";

	override public function get_type(): String {
		return MessageType;
	}

	/**
		The Entity that is performing the Action
	**/
	public var entity: Entity;

	/**
		Private constructor, to be called by child class only.

		@param entity Entity performing the Action
	**/
	public function new(entity: Entity) {
		super();
		this.entity = entity;
	}

	/**
		Perform the action and call onFinish once the action is completed.

		@param onFinish
			if an integer is provided, it will be the cost of the action.
			If it is null, the action is considered to have not happened.

		@return true if the action is successful, false otherwise.
			The return tells the performer if the action is successful, not if the action is completed.
	**/
	dynamic public function perform(onFinish: ActionResult->Void): Bool {
		return false;
	}

	override public function toString(): String {
		return '<Action>';
	}
}
