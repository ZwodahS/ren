package ren.core.components;

class PlayerComponent extends zf.ecs.Component {
	public static final ComponentType = "PlayerComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public static function get(e: zf.ecs.DynamicEntity): PlayerComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, PlayerComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	override public function toString(): String {
		return '{c:PlayerComponent}';
	}
}
