package ren.core.components;

import zf.Point2i;

import ren.core.Level;
import ren.core.Tile;

class LocationComponent extends zf.ecs.Component {
	public static final ComponentType = "LocationComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public var position: Point2i = null;
	public var level: Level = null;
	public var tile: Tile = null;

	public static function get(e: Entity): LocationComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, LocationComponent);
	}

	override public function toString(): String {
		if (this.level == null) return '{c:LocationComponent: null|null}';
		return '{c:LocationComponent: ${level}|${tile}}';
	}
}
