package ren.core;

import hxd.Rand;

import zf.Point2i;

import ren.core.systems.ActionSystem;
import ren.core.messages.PlayerSet;
import ren.core.messages.EntityMoved;
import ren.core.messages.EntityDestroyed;
import ren.core.messages.WorldInit;
import ren.core.components.LocationComponent;

/**
	Define configuration for the world.
**/
typedef WorldConfiguration = {
	?seed: Int,
};

class World extends zf.ecs.World {
	public static var SeedMax: Int = 0x7FFFFFFE;

	/**
		This implementation of World
	**/
	/** Generalised Code **/
	/**
		Load a level.

		@param level level to load

		When a level is loaded, all the entities will be added to allow for simulation.
	**/
	public function loadLevel(level: Level) {
		if (isLoaded(level)) return;
		for (xy => tile in level.map.iterateYX()) {
			if (tile == null) continue;
			for (e in tile.entities) {
				this.addEntity(e);
			}
		}
		this.loadedLevels.push(level);
	}

	/**
		Unload a level
		This will unload all entities
	**/
	public function unloadLevel(level: Level) {
		if (!isLoaded(level)) return;
		for (xy => tile in level.map.iterateYX()) {
			if (tile == null) continue;
			for (e in tile.entities) {
				this.removeEntity(e);
			}
		}
		this.loadedLevels.remove(level);
	}

	/**
		Tue Nov 17 14:02:07 2020
			if checking this become expensive, need to create ID for level and use a Map
	**/
	var loadedLevels: Array<Level>;

	function isLoaded(level: Level): Bool {
		for (l in this.loadedLevels) {
			if (l == level) return true;
		}
		return false;
	}

	/** End of level changing **/
	/** Random number & seed **/
	public var r(default, null): Rand;

	public var seed(default, null): Int;

	/**
		Player Entity.

		A Special Entity since it is the one that the player control.
	**/
	public var player(default, set): Entity = null;

	public function set_player(p: Entity): Entity {
		if (this.player == p) return this.player;
		var oldValue = this.player;
		this.player = p;
		this.dispatcher.dispatch(new PlayerSet(oldValue, this.player));
		return this.player;
	}

	/** Animations **/
	public var blockingAnimator(default, null): zf.animations.Animator;

	public var nonBlockingAnimator(default, null): zf.animations.Animator;

	/**
		Returns if world is currently blocked by animations.
		Ideally, while this is true, no other logic systems should run.
	**/
	public var isAnimating(get, never): Bool;

	inline public function get_isAnimating(): Bool {
		return this.blockingAnimator.count > 0;
	}

	/** End of Animations **/
	/** Core Systems **/
	var actionSystem: ActionSystem;

	/**
		flag to denote if the world has been init
		This can only be set once and will send out a worldinit message
	**/
	public var isInit(default, set): Bool = false;

	public function set_isInit(b: Bool): Bool {
		if (this.isInit) throw("Error: attempting to init a world after it has been inited");
		this.isInit = true;
		this.dispatcher.dispatch(new WorldInit());
		return this.isInit;
	}

	public function new(configuration: WorldConfiguration = null) {
		super();
		if (configuration == null) {
			configuration = {seed: null};
		}
		this.seed = configuration.seed != null ? configuration.seed : Random.int(0, SeedMax);
		Logger.debug('World seed: ${this.seed}');
		this.r = new hxd.Rand(this.seed);
		this.loadedLevels = [];

		this.blockingAnimator = new zf.animations.Animator();
		this.nonBlockingAnimator = new zf.animations.Animator();

		// these systems are core, and should never be disabled.
		this.addSystem(this.actionSystem = new ActionSystem());

		setupSystems();
	}

	override public function update(dt: Float) {
		super.update(dt);
		this.blockingAnimator.update(dt);
		this.nonBlockingAnimator.update(dt);
	}

	/** End of Generalised Code **/
	/** Level handling code **/
	function setupSystems() {}

	public function performAction(action: Action): Bool {
		return this.actionSystem.performAction(action);
	}

	/**
		Generic method to move entity into a level.

		No assumption is made on if the entity is already in another level.
		This does not perform any checks.

		If an entity is already on the level, this will also work, just a bit more costly.
		The method moveEntity will be better
	**/
	public function moveEntityIntoLevel(entity: Entity, level: Level, position: Point2i,
			autoload: Bool = true): Bool {
		if (!isLoaded(level)) {
			if (!autoload) return false;
			loadLevel(level);
		}

		if (this.entities[entity.id] == null) this.addEntity(entity);

		var lc = LocationComponent.get(entity);

		// keep the previous value
		var prevLevel = lc.level;
		var prevPosition = lc.position != null ? lc.position.copy() : null;
		var prevTile = lc.tile;

		if (prevLevel != null) prevLevel.removeEntity(entity, lc);
		level.placeEntity(entity, position);

		this.dispatcher.dispatch(new EntityMoved(entity, prevLevel, prevPosition, level, position));

		return true;
	}

	/**
		Generic method to move entity within a level

		No assumptions or checks are made.

		Wed Jan 06 12:17:07 2021
		Not actually sure if this method should be here, or if it is actually too specialised.
	**/
	public function moveEntity(entity: Entity, position: Point2i): Bool {
		var lc = LocationComponent.get(entity);
		if (lc == null || lc.level == null) return false;
		var prevPosition = lc.position.copy();

		if (!lc.level.moveEntity(entity, position)) return false;

		this.dispatcher.dispatch(new EntityMoved(entity, lc.level, prevPosition, lc.level, position));

		return true;
	}

	/**
		Remove the entity from the level, but keep it around.
	**/
	public function removeEntityFromLevel(entity: Entity): Bool {
		var lc = LocationComponent.get(entity);
		if (lc == null || lc.level == null) return false;

		var prevLevel = lc.level;
		var prevPosition = lc.position.copy();
		prevLevel.removeEntity(entity, lc);

		this.dispatcher.dispatch(new EntityMoved(entity, prevLevel, prevPosition, null, null));
		return true;
	}

	/**
		Destroy an entity.

		@param e the entity to destroy
	**/
	public function destroyEntity(e: Entity) {
		final lc = LocationComponent.get(e);
		final tile = lc == null ? null : lc.tile;
		if (tile == null) {
			Logger.warn('calling destroyEntity when entity is not on tile', 'Engine');
			return;
		}
		removeEntityFromLevel(e);
		this.dispatcher.dispatch(new EntityDestroyed(e, tile));
		this.removeEntity(e);
	}
}
