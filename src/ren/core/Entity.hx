package ren.core;

import ren.core.components.LocationComponent;

/**
	The Main Entity class for the engine, to be subclassed.

	This only provide the core component used by core.
**/
class Entity extends zf.ecs.DynamicEntity {
	public function new() {
		super();
		attachComponent(new LocationComponent());
	}
}
