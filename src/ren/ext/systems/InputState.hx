package ren.ext.systems;

class InputState {
	var inputSystem: SimpleInputSystem;

	public function new() {}

	public function handleEvent(event: hxd.Event): Bool {
		return false;
	}

	public function onEnter() {}

	public function onExit() {}

	public function activate(push: Bool = false): Bool {
		if (this.inputSystem == null) return false;
		if (push) {
			this.inputSystem.pushState(this);
		} else {
			this.inputSystem.currentState = this;
		}
		return true;
	}

	public function deactivate(): Bool {
		if (this.inputSystem == null) return false;
		return this.inputSystem.deactivate(this);
	}

	public var isActive(get, never): Bool;

	public function get_isActive(): Bool {
		return this.inputSystem != null && this.inputSystem.currentState == this;
	}

	public var name(get, never): String;

	public function get_name(): String {
		return "InputState";
	}

	public function update(dt: Float) {}

	public function toString() {
		return '${name}';
	}
}
