package ren.ext.systems;

import zf.ecs.EntityMap;

import ren.core.Entity;
import ren.core.World;
import ren.core.messages.EntityTurnEnd;
import ren.core.messages.EntityMarkedForDestroy;
import ren.ext.components.HealthComponent;
import ren.ext.messages.EntityDamaged;

class DeathSystem extends zf.ecs.System {
	var world: World;

	public var updatedEntities: EntityMap<Entity>;
	public var destroyedEntities: EntityMap<Entity>;

	public function new() {
		super();
		this.updatedEntities = new EntityMap<Entity>();
		this.destroyedEntities = new EntityMap<Entity>();
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);
		var dispatcher = this.world.dispatcher;

		// @:listen DeathSystem EntityDamaged 0
		dispatcher.listen(EntityDamaged.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityDamaged);
			var hc = HealthComponent.get(m.entity);
			if (hc != null) this.updatedEntities.add(m.entity);
		}, 0);

		// @:listen DeathSystem EntityMarkedForDestroy 0
		dispatcher.listen(EntityMarkedForDestroy.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityMarkedForDestroy);
			this.destroyedEntities.add(m.entity);
		}, 0);

		// @:listen DeathSystem EntityTurnEnd 100
		dispatcher.listen(EntityTurnEnd.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityTurnEnd);
			this.updatedEntities.add(m.entity);
			onTurnEnd(m.entity);
		}, 100);
	}

	function onTurnEnd(e: Entity) {
		for (id => e in this.updatedEntities) {
			var hc = HealthComponent.get(e);
			if (hc == null || hc.current > 0) continue;
			this.destroyedEntities.add(e);
		}
		for (id => e in this.destroyedEntities) {
			onEntityDestroyed(e);
		}
		this.updatedEntities.clear();
		this.destroyedEntities.clear();
	}

	function onEntityDestroyed(e: Entity) {
		this.world.destroyEntity(e);
	}
}
