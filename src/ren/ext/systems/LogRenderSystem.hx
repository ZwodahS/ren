package ren.ext.systems;

class LogRenderSystem extends zf.ecs.System {
	public var drawLayer: h2d.Object;
	public var texts: haxe.ds.Vector<h2d.HtmlText>;
	public var font: h2d.Font;

	var MaxTextCount: Int = 0;
	var curr = 0;

	public function new(drawLayer: h2d.Object, font: h2d.Font, max: Int = 15) {
		super();
		this.drawLayer = drawLayer;
		this.font = font;
		this.MaxTextCount = max;

		this.texts = new haxe.ds.Vector<h2d.HtmlText>(MaxTextCount);
		for (i in 0...this.texts.length) {
			this.texts[i] = new h2d.HtmlText(font);
			this.texts[i].x = 5;
			this.drawLayer.addChild(this.texts[i]);
		}
		this.curr = 0;
	}

	override public function reset() {
		for (t in this.texts) {
			t.text = '';
		}
		this.curr = 0;
	}

	public function log(message: String) {
		this.texts[this.curr].text = message;
		this.curr += 1;
		if (this.curr == MaxTextCount) this.curr = 0; // wrap around
		var pos = 0;
		// align the text
		for (x in this.curr...MaxTextCount) {
			this.texts[x].y = pos;
			pos += 16;
		}
		for (x in 0...this.curr) {
			this.texts[x].y = pos;
			pos += 16;
		}
	}
}
