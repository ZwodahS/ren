package ren.ext.systems;

import zf.ds.CircularLinkedList;
import zf.ecs.messages.ComponentAttached;
import zf.ecs.messages.ComponentRemoved;

import ren.core.Entity;
import ren.core.World;
import ren.core.Action;
import ren.core.ActionResult;
import ren.core.messages.EntityActiveTurn;
import ren.core.messages.EntityTakeAction;
import ren.core.messages.WorldInit;
import ren.core.messages.EntityTurnStart;
import ren.core.messages.EntityTurnEnd;
import ren.ext.components.TUTurnComponent;

class TuDoNothing extends Action {
	var tc: Int;

	public function new(entity: Entity, tc: Int) {
		super(entity);
		this.tc = tc;
	}

	override public function perform(onFinish: ActionResult->Void): Bool {
		onFinish(new TUActionResult(this.tc, true));
		return true;
	}

	override public function toString(): String {
		return '<DoNothing:${this.tc}>';
	}
}

class TUActionResult extends ActionResult {
	public var timeCost: Int;
	public var endTurn: Bool;

	public function new(timeCost: Int, endTurn: Bool = true) {
		super();
		this.timeCost = timeCost;
		this.endTurn = endTurn;
	}
}

@:structInit class TurnSystemEntity {
	public var e: Entity;
	public var tc: TUTurnComponent;
}

class TurnQueue extends CircularLinkedList<TurnSystemEntity> {
	var map: Map<Int, CircularLinkedNode<TurnSystemEntity>>;

	public var activeEntity(get, never): Entity;

	public function get_activeEntity(): Entity {
		return this.current == null ? null : this.current.item.e;
	}

	public function new() {
		super();
		this.map = new Map<Int, CircularLinkedNode<TurnSystemEntity>>();
	}

	public function registerEntity(e: Entity, tc: TUTurnComponent) {
		if (map.exists(e.id)) return;
		final node = super.insertBefore({e: e, tc: tc});
		this.map[e.id] = node;
	}

	public function unregisterEntity(e: Entity) {
		if (!map.exists(e.id)) return;
		final node = map[e.id];
		node.remove();
		this.map.remove(e.id);
	}
}

/**
	This is a linear time system that allow each entity to take action in order.
	How much action the entity can take depends on the action.
	Some actions will end the turn immediately and some don't
**/
class TUTurnSystem extends zf.ecs.System {
	public var tuPerTurn(default, null): Int = 1;

	var world: World;
	var queue: TurnQueue;

	public var activeEntity(get, never): Entity;
	public var pause: Bool = false;

	public inline function get_activeEntity(): Entity {
		return this.queue.activeEntity;
	}

	/**
		if true, turn selection will wait until all animations is completed.
		setting to false may have weird effects if not handled properly.
	**/
	public var blockedByAnimator: Bool = true;

	public function new(timeUnitPerTurn: Int = 1, blockedByAnimator: Bool = true) {
		super();
		this.tuPerTurn = timeUnitPerTurn;
		this.queue = new TurnQueue();
		this.blockedByAnimator = blockedByAnimator;
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);

		// @:listen TUTurnSystem ComponentAttached 100
		this.world.dispatcher.listen(ComponentAttached.MessageType, function(message: zf.Message) {
			var m = cast(message, ComponentAttached);
			if (m.component.type == TUTurnComponent.ComponentType) {
				var tc = cast(m.component, TUTurnComponent);
				this.registerEntity(cast(m.entity, Entity), tc);
			}
		}, 100);

		// @:listen TUTurnSystem ComponentRemoved 100
		this.world.dispatcher.listen(ComponentRemoved.MessageType, function(message: zf.Message) {
			var m = cast(message, ComponentRemoved);
			if (m.component.type == TUTurnComponent.ComponentType) {
				this.unregisterEntity(cast(m.entity, Entity));
			}
		}, 100);

		// @:listen TUTurnSystem EntityTakeAction 100
		this.world.dispatcher.listen(EntityTakeAction.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityTakeAction);
			try {
				var ar = cast(m.actionResult, TUActionResult);
				onEntityTakeAction(m.entity, m.action, ar);
			} catch (e) {
				return;
			}
		}, 100);
	}

	function onEntityTakeAction(entity: ren.core.Entity, action: Action, actionResult: TUActionResult) {
		if (this.queue.activeEntity != entity) return;
		var tc = TUTurnComponent.get(entity);
		tc.tookAction = true;
		if (actionResult.endTurn) {
			tc.endTurn = true;
		} else {
			tc.timeunit -= actionResult.timeCost;
			if (tc.timeunit <= 0) tc.endTurn = true;
		}
	}

	override public function reset() {
		this.actualActiveEntity = null;
		this.entitiesTakenTurn.clear();
		this.pause = false;
	}

	public function forceEntityTurn(e: Entity) {
		final node = this.queue.findOneNode(function(n) {
			return n.e == e;
		});
		if (node != null) {
			while (this.queue.current.item.e != e)
				this.queue.next();
		}
	}

	override public function entityAdded(e: zf.ecs.Entity) {
		final entity = cast(e, Entity);
		final tc = TUTurnComponent.get(entity);
		if (tc == null) return;
		registerEntity(entity, tc);
	}

	override public function entityRemoved(e: zf.ecs.Entity) {
		unregisterEntity(cast(e, Entity));
	}

	function registerEntity(entity: Entity, tc: TUTurnComponent) {
		this.queue.registerEntity(entity, tc);
		tc.timeunit = tuPerTurn;
		tc.endTurn = false;
	}

	function unregisterEntity(entity: Entity) {
		this.queue.unregisterEntity(entity);
	}

	var actualActiveEntity: Entity = null;
	var entitiesTakenTurn: Map<Int, Entity> = new Map<Int, Entity>();

	override public function update(dt: Float) {
		/**
			In general, all implementation of TurnSystem need to fire 2 events.

			- EntityTurnStart to inform the start of entity turn
			- EntityTurnEnd to inform that the entity has ended their turn.
			- EntityActiveTurn to inform that it is now the entity's turn.

			The decision to fire these event handles on every update frame.

			There a lot of things that sometimes needs to happen between the messages, so we need to handle them.
			The basic gist of how turn system works is this

			> if no entity is currently active, find the next entity, and fire EntityActiveTurn
			> if there is an entity currently active, check if it ended its turn, and if so, fire EntityTurnEnd

			The part that makes it tricky is how this interacts with AI System.
			When TurnSystem fires EntityActiveTurn, and if the entity is controlled by an AI,
			it will immediately perform the action, ending the turn if there is no animations.

			When TurnSystem regain control, the state of the queue would have already changed
			and need to be handled.

			One of the biggest problems might be that the active entity might die and be removed from the queue.
			When this happens, the next entity will become the current entity in the queue,
			and if this is not handled properly, the entity might miss a turn, or the game will enter into
			a inconsistent state.

			In additional to that, if entity taking turn does not have animations, e.g. off screen enemy,
			then it is better to simulate them all in a single frame.
		**/

		if (this.pause) return;

		inline function endCurrentEntityTurn() {
			// reset the state of the current entity
			final current = this.queue.current;
			current.item.tc.endTurn = false;
			current.item.tc.timeunit = tuPerTurn;
			current.item.tc.tookAction = false;
			final dispatchedEntity = current.item.e;
			// move the queue
			this.queue.next();
			// set actualActiveEntity to null
			this.actualActiveEntity = null;
			// dispatch the end turn event
			// this dispatch might change the state of the turn queue.
			this.world.dispatcher.dispatch(new EntityTurnEnd(dispatchedEntity));
		}

		// ensure that every frame each entities will only take action once.
		// the loop here is to allow for more than 1 action performed per frame.
		// this is useful for when entity is out of sight and we are not animating.
		this.entitiesTakenTurn.clear();
		while (true) {
			// if the world is animating and we want to block turn from simulating
			// when there is a blocking animator, we don't do anything.
			if (this.blockedByAnimator && this.world.isAnimating) return;
			// when there is 0 item and nothing is current active, we just exit the loop
			if (this.actualActiveEntity == null && this.queue.current == null) return;
			// if the current entity is null, we treat the queue current as the new current and fire the event.
			if (this.actualActiveEntity == null) {
				this.actualActiveEntity = this.queue.current.item.e;

				final turnStart = new EntityTurnStart(this.actualActiveEntity);
				this.world.dispatcher.dispatch(turnStart);
				// if the turn is disrupted, we will immediately end the entity turn
				if (turnStart.disrupted) {
					endCurrentEntityTurn();
					continue;
				}
				this.world.dispatcher.dispatch(new EntityActiveTurn(this.actualActiveEntity));
				// there is no need to break the loop, since the EntityActiveTurn may have ended the entity's turn.
				// this is the case for AI taking their turn that does not involve animation.
			}
			// this case handles when the active entity is removed from the turnqueue as part of performing Action
			// this also handles the case where entityactive finishes and there is nothing in the queue,
			// i.e. the last entity died as part of the action;
			if (this.queue.current == null || this.actualActiveEntity != this.queue.current.item.e) {
				// we should fire the entity turn end here as well
				this.world.dispatcher.dispatch(new EntityTurnEnd(this.actualActiveEntity));
				// we also need to set the actualActiveEntity to null and restart the loop
				this.actualActiveEntity = null;
				continue;
			}
			// this is the normal flow. Current actualActiveEntity == this.queue.current
			final current = this.queue.current;
			if (!current.item.tc.endTurn) {
				// if entity took action but did not end turn, we send a ActiveTurn again.
				if (current.item.tc.tookAction) {
					current.item.tc.tookAction = false;
					this.world.dispatcher.dispatch(new EntityActiveTurn(current.item.e));
				}
				// we will return here as the turn is not ended yet
				return;
			}
			// clean up after entity end turn
			endCurrentEntityTurn();
			/**
				peek ahead, if the next entity that is about to take the turn already took a turn in this cycle,
				we exit the loop. This is to give the UI a chance to accept input.
				If we don't handle this, when the player dies, the game might enter a infinite loop when simulating
				enemies movement. This will not be a problem if the movement have animations, but in the case
				where there is no animations, it will be stucked forever.
			**/
			if (current == null) break;
			if (entitiesTakenTurn[current.item.e.id] != null) break;
			// cache the entity that have taken turn
			this.entitiesTakenTurn[current.item.e.id] = current.item.e;
		}
	}
}
