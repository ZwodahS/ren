package ren.ext.systems;

import zf.Assert;
import zf.ecs.messages.ComponentAttached;
import zf.ecs.messages.ComponentRemoved;

import ren.core.World;
import ren.core.Entity;
import ren.core.Action;
import ren.core.ActionResult;
import ren.ext.components.ApComponent;
import ren.core.messages.EntityActiveTurn;
import ren.core.messages.EntityTakeAction;

/**
	A simple action of do nothing, passing a certain number of ap
**/
class ApDoNothing extends Action {
	var ap: Int;

	public function new(entity: Entity, ap: Int) {
		super(entity);
		this.ap = ap;
	}

	override public function perform(onFinish: ActionResult->Void): Bool {
		onFinish(new ApActionResult(this.ap));
		return true;
	}

	override public function toString(): String {
		return '<DoNothing:${this.ap}>';
	}
}

class ApActionResult extends ActionResult {
	public var ap: Int;

	public function new(ap: Int) {
		super();
		this.ap = ap;
	}

	public function toString(): String {
		return '[ApActionResult: ${this.ap}]';
	}
}

/**
	ApTurnSystem uses a full action point system to simulate turns.

	After every action taken, if the ActionResult is an APActionResult,
	the entity's AP will be updated and the turn will simulate.
	This will then emit the EntityActionTurn to update whose turn it is.

	In order for this to work, action needs to emite ApActionResult.
	The entities also need to have ApComponent (previously know as ApComponent)

	NOTE:
	Some of the variable here looks weird, like turn, tc, etc.
	This is because ApComponent is previously named TurnComponent and renaming everything here is counter-productive.

	Mon 13:19:19 20 Sep 2021
	This is currently outdated and requires some fixes to make it emits the same type of messages as TUTurnSystem
**/
class ApTurnSystem extends zf.ecs.System {
	var world: World;
	var turnQueue: List<{e: Entity, turn: ApComponent}>;
	var requiredAP: Int;
	var doNothingCost: Int;
	var forceSimulate: Bool = false;

	public var activeEntity(get, never): Entity;

	public function get_activeEntity(): Entity {
		var e = this.turnQueue.first();
		if (e == null) return null;
		if (e.turn.ap < this.requiredAP) return null;
		return e.e;
	}

	public function new(requiredAP: Int) {
		super();
		this.requiredAP = requiredAP;
		this.doNothingCost = requiredAP;
		this.turnQueue = new List<{e: Entity, turn: ApComponent}>();
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);

		// @:listen ApTurnSystem ComponentAttached 100
		this.world.dispatcher.listen(ComponentAttached.MessageType, function(message: zf.Message) {
			var m = cast(message, ComponentAttached);
			if (m.component.type == ApComponent.ComponentType) {
				this.registerEntity(cast(m.entity, Entity));
			}
		}, 100);

		// @:listen ApTurnSystem ComponentRemoved 100
		this.world.dispatcher.listen(ComponentRemoved.MessageType, function(message: zf.Message) {
			var m = cast(message, ComponentRemoved);
			if (m.component.type == ApComponent.ComponentType) {
				this.unregisterEntity(cast(m.entity, Entity));
			}
		}, 100);

		// @:listen ApTurnSystem EntityTakeAction 100
		this.world.dispatcher.listen(EntityTakeAction.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityTakeAction);
			var tc = ApComponent.get(m.entity);
			Assert.assert(tc != null);
			try {
				// only handles those that extends ApActionResult
				var ar = cast(m.actionResult, ApActionResult);
				tc.ap -= ar.ap;
				var item = this.turnQueue.removeByFunc(function(item) {
					return item.e == m.entity;
				});
				this.turnQueue.add(item);
				this.forceSimulate = true;
			} catch (e) {
				return;
			}
		}, 100);
	}

	override public function entityAdded(e: zf.ecs.Entity) {
		var entity = cast(e, Entity);
		var tc = ApComponent.get(entity);
		if (tc == null) return;
		registerEntity(entity);
	}

	override public function entityRemoved(e: zf.ecs.Entity) {
		var entity = cast(e, Entity);
		var tc = ApComponent.get(entity);
		if (tc == null) return;
		unregisterEntity(entity);
	}

	function registerEntity(entity: Entity) {
		var tc = ApComponent.get(entity);
		this.turnQueue.add({e: entity, turn: tc});
	}

	function unregisterEntity(entity: Entity) {
		this.turnQueue.removeByFunc(function(item) {
			return item.e == entity;
		});
	}

	/**
		Sort the turn queue
	**/
	function sortTurn() {
		this.turnQueue.sort(function(e1, e2) {
			if (e1.turn.ap == e2.turn.ap) return 0;
			if (e1.turn.ap < e2.turn.ap) return 1;
			return -1;
		});
	}

	/**
		Simulate the AP until a entity is active.
		This will simulate regardless if the entity in the first position is already ready
		and will fire the event regardless.

		If the first entity already have enough, nothing will be simulated, and an event will be fired.
	**/
	function simulateTurn() {
		this.forceSimulate = false;
		sortTurn();
		var hasEnough = false;
		// do a single pass first
		if (this.turnQueue.length == 0) return;
		if (this.turnQueue.first().turn.ap < this.requiredAP) {
			// calculate the speed gain first.

			/**
				Tue Nov 17 11:44:14 2020
					This might be limiting for status buff which duration is based on AP rather than turns.
					Not my concern for now.
			**/
			var entitySpeed = new Map<Int, Int>();

			for (e in this.turnQueue) {
				var speed = getEntitySpeed(e.e, e.turn);
				entitySpeed[e.e.id] = speed;
			}

			// keep track on the number of time simulated. Might want to fire an event in the future.
			var simulated = 0;
			while (!hasEnough) {
				for (e in this.turnQueue) {
					e.turn.ap += entitySpeed[e.e.id];
					// when an entity have enough AP, we stop the simulation.
					hasEnough = hasEnough || e.turn.ap >= this.requiredAP;
				}
				simulated++;
			}
			// do a final sort to get the correct first entity.
			sortTurn();
		}
		// fire the event for the current active entity.
		this.world.dispatcher.dispatch(new EntityActiveTurn(this.turnQueue.first().e));
	}

	function shouldSimulate(): Bool {
		return this.forceSimulate
			|| (this.turnQueue.length != 0 && this.turnQueue.first().turn.ap < this.requiredAP);
	}

	override public function update(dt: Float) {
		if (!shouldSimulate()) return;
		/**
			Simulate until there isn't a need to.
			Because simulateTurn will dispatch a TurnEvent, it will cause AISystem to trigger and run the AI.
			After the AI run, it might return this to a state where a simulation is required, and this will loop.
			Eventually this will loop until
				1. Player's turn
				2. AI perform an action that has animation, which causes this.turnQueue.first().turn.ap not to changed,
				   hence not having to simulate. When the animation complete, it will update the ap, and
				   causing this to simulate again.

			Tue Nov 17 11:38:02 2020
				There is a drawback to this design, namely that a 0 AP cost action will cause this to stuck,
				since it rely on simulateTurn to trigger AI actions.
				This means that 0 AP Cost Action needs to be carefully handled elsewhere.
				In the case of player's 0 AP Cost, the problem is not that big (hopefully) since the player will be allowed
				to take action whenever it is their turn and it is not animating.
		**/

		while (shouldSimulate())
			simulateTurn();
	}

	public function isEntityActive(e: Entity): Bool {
		return activeEntity == e;
	}

	/** Specialised Code **/
	function getEntitySpeed(entity: Entity, turn: ApComponent): Int {
		var speed = turn.speed;
		return speed;
	}

	/** End of Specialised Code **/
	/** Debugging Code **/
#if debug
	public var queueString(get, never): String;

	public function get_queueString(): String {
		var qs = "";
		for (e in this.turnQueue) {
			qs += '${e.e}: ${e.turn.ap} / ${this.requiredAP}\n';
		}
		return qs;
	}

	public function printQueue(?label: String) {
		if (label == null) label = '';
		trace('[Debug] Turn Queue: ${label}');
		for (s in this.queueString.split("\n")) {
			trace('>> [Debug] ${s}');
		}
	}
#end
	/** End of Debugging **/
}
