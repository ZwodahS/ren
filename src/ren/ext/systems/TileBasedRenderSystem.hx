package ren.ext.systems;

import h2d.SpriteBatch;
import h2d.SpriteBatch.BatchElement;

import zf.Point2i;
import zf.Point2f;
import zf.Rectf;
import zf.animations.WrappedObject;
import zf.Color;

import ren.core.Entity;
import ren.core.World;
import ren.core.Level;
import ren.core.Tile;
import ren.core.TileVisibility;
import ren.core.messages.PlayerSet;
import ren.core.components.LocationComponent;
import ren.core.messages.TilesVisibilityChanged;
import ren.core.messages.TilesLightLevelChanged;
import ren.core.messages.EntityMoved;

using zf.MathExtensions;

/**
	A specialised render component for this system.
**/
class TBRenderComponent extends zf.ecs.Component {
	public static final ComponentType = "TBRenderComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public var drawLayer: String = "entity";
	public var world: h2d.Object = null;
	public var priority: Int = 0;
	public var renderInFog: Bool = false;

	public static function get(e: Entity, key: String = null): TBRenderComponent {
		var c = e.getComponent(key == null ? ComponentType : key);
		return c == null ? null : cast(c, TBRenderComponent);
	}

	override public function toString(): String {
		return '{c:TBRenderComponent: ${this.world}|${this.priority}}';
	}

	public function new(obj: h2d.Object = null, priority: Int = 0, renderInFog: Bool = false) {
		super();
		this.world = obj;
		this.priority = priority;
		this.renderInFog = renderInFog;
	}
}

/**
	Provide a generic tile based rendering system.

	**Assumptions**
	(0) A child class will be created to provide more concrete implementation.

	(1) The concept of visibility is relative to the observer; i.e. the player.
	However, this system does not make any assumptions as to who is the observer and this is handed to
	the child class to handle it.

	(2) The Fog of war implemented here is a fog-tile system.
	Rather than a memory system, the fog-tile system renders a fog on top of the actual tile.
	There are some implication to such implementation.

	For example, if a wall is destroyed by an entity when the player can't see the tile, the change
	will still be reflected on the tile.

	"Memory" - based system is the other approach to fog, such that the actual entity is not rendered
	but a memory of the tile with respect to the observer.

	This may be implemented in the future, but requires some thoughts on how to generalise it.
	This can be currently achieved by changing how syncTile works in child class of TileBasedRenderSystem,
	such that it hides all tiles/entities in that tile and implementing a custom MemorySystem

	**Changelog**
	Sat Jul 10 15:18:21 2021 Migrated Fog implemented here.

	Note that all previous changelog are removed, as they are essentially invalid at this moment.
**/
class TileBasedRenderSystem extends zf.ecs.System {
	/** various configuration for handling light level **/
	/**
		The maximum light level. Any value above this will be treated as this value.
	**/
	public var MaxLightLevel: Int = 100;

	// store a reference to the world that this system is added to.
	var world: World;

	// the size of each grid
	var gridSize: Point2i;

	// the main draw layer
	var drawLayer: h2d.Layers;

	var drawLayers: Map<String, h2d.Layers>;

	/**
		fogDrawLayer is rendered at 100
	**/
	var fogDrawLayer: h2d.SpriteBatch;

	var fogTile: h2d.Tile;

	/**
		entitiesDrawLayer is rendered at 50
	**/
	var entitiesDrawLayer: h2d.Layers;

	/**
		floorDrawLayer is rendered at 5
		it is almost assumed that nothing rendered below floorDrawLayer.
		the index that the floorDrawLayer is set to 5 in case new layers is needed.
	**/
	var floorDrawLayer: h2d.Layers;

	var currentLevel: Level;

	var entities: Map<Int, {e: Entity, rc: TBRenderComponent}>;

	/**
		flags
		these flags need to be set before the system is added to world, ideally in the constructor of child class
		to avoid unwanted side effects. These should never be changed once configured.
	**/
	/**
		if set to true, onTileOver, onTileOut will be called when player hover the mouse
		over the tile. The method should be handled in child class.
	**/
	public var allowHover: Bool = true;

	public var tilingFog: Bool = false;

	public var bgColor: Null<Color> = null;

	/**
		The viewport of the rendersystem. This handles centering of the world.
	**/
	public var viewport(default, set): Rectf = null;

	public function set_viewport(v: Rectf): Rectf {
		this.viewport = v;
		centerOn(this.centeredEntity);
		return this.viewport;
	}

	/**
		If set, the render system will center on the entity whenever any entity is moved.
	**/
	public var followEntity: Entity = null;

	/**
		if non-0, the priority that the entity is rendered on is added with y
		i.e. priority = yIndexMultiplier * y + priority

		this will currently be applied to the entitiesDrawLayer.
		for the other layers, only the priority will be used.
	**/
	public var yIndexMultiplier: Int = 0;

	public function set_followEntity(e: Entity): Entity {
		this.followEntity = e;
		centerOn(this.followEntity);
		return this.followEntity;
	}

	var defaultFogColor: Null<Color>;

	/** End of flags **/
	public function new(drawLayer: h2d.Layers, gridSize: Point2i) {
		super();
		if (this.defaultFogColor == null) this.defaultFogColor = 0x000000;
		this.drawLayer = drawLayer;
		this.drawLayers = new Map<String, h2d.Layers>();
		this.gridSize = gridSize;
		this.entities = new Map<Int, {e: Entity, rc: TBRenderComponent}>();
		addDrawLayer("bg", new h2d.Layers(), 0);
		addDrawLayer("floor", this.floorDrawLayer = new h2d.Layers(), 5);
		addDrawLayer("entity", this.entitiesDrawLayer = new h2d.Layers(), 50);
		this.fogTile = h2d.Tile.fromColor(defaultFogColor, gridSize.x, gridSize.y);
		this.drawLayer.add(this.fogDrawLayer = new h2d.SpriteBatch(fogTile), 100);
	}

	inline public function addDrawLayer(id: String, o: h2d.Layers, index: Int) {
		this.drawLayer.add(o, index);
		this.drawLayers[id] = o;
	}

	inline public function getDrawLayer(id: String): h2d.Layers {
		return this.drawLayers[id];
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);

		// @:listen TileBasedRenderSystem EntityMoved 100 update rendering when entity move
		world.dispatcher.listen(EntityMoved.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityMoved);
			if (m.newLevel == null) {
				if (m.oldLevel == null) return;
				var tile = m.oldLevel.map.get(m.oldPosition.x, m.oldPosition.y);
				if (tile == null) return;
				tryRemoveEntityFromDrawLayer(m.entity);
				syncTile(tile);
			} else {
				onEntityMoved(m.entity, m.oldPosition, m.newPosition);
			}
		}, 100);

		// @:listen TileBasedRenderSystem TilesVisibilityChanged 100 update fog when visibility changed
		world.dispatcher.listen(TilesVisibilityChanged.MessageType, function(message: zf.Message) {
			var m = cast(message, TilesVisibilityChanged);
			onVisibilityChanged(m.tiles);
		}, 100);

		// @:listen TileBasedRenderSystem TilesLightLevelChanged 100 update fog when light level changed
		world.dispatcher.listen(TilesLightLevelChanged.MessageType, function(message: zf.Message) {
			var m = cast(message, TilesLightLevelChanged);
			onVisibilityChanged(m.tiles);
		}, 100);
	}

	function onEntityMoved(entity: Entity, oldPosition: Point2i, newPosition: Point2i) {
		// if hideTileBackgroundWithEntity is not true, then nothing needs to be changed.
		var lc = ren.core.components.LocationComponent.get(entity);
		// currently this just stop updating the render when the level is not the current level.
		// However, when level caching is implemented, we will need to update the hidden layers as well.
		if (lc == null || lc.level == null || lc.level != this.currentLevel) return;

		// Moving between levels are handled in 2 different events. See DESIGN.md M.1 for the note on this
		// Because of this, when the entity enters another levle it will be from null oldPosition to newPosition
		// entity is moved into the current level
		if (newPosition != null) {
			// update entity if oldPosition is null, i.e. moving entity into a new level
			// update entity if yindex matters and yindex has changed
			if (oldPosition == null || (this.yIndexMultiplier != 0 && newPosition.y != oldPosition.y)) {
				tryAddEntityToDrawLayer(entity);
			}
		}
		// sync both the old and new tiles
		syncTile(oldPosition == null ? null : lc.level.map.get(oldPosition.x, oldPosition.y));
		syncTile(newPosition == null ? null : lc.level.map.get(newPosition.x, newPosition.y));

		if (this.followEntity != null) centerOn(this.followEntity);
	}

	function hideEntity(e: Entity, ?rc: TBRenderComponent) {
		if (rc == null) rc = TBRenderComponent.get(e);
		if (rc != null) rc.world.visible = false;
	}

	function showEntity(e: Entity, ?rc: TBRenderComponent) {
		if (rc == null) rc = TBRenderComponent.get(e);
		if (rc != null) rc.world.visible = true;
	}

	/**
		update the rendering for a tile.

		@param tile the tile to update. if null, nothing happens
		@param tileVisibility if provided, the value will be used, else getTileVisibility will be called
	**/
	function syncTile(tile: Tile, tileVisibility: TileVisibility = null) {
		if (tile == null) return;
		tileVisibility = tileVisibility == null ? getTileVisibility(tile) : tileVisibility;

		/**
			Render the tile depending on the tile visibility
		**/
		for (e in tile.entities) {
			var rc = TBRenderComponent.get(e);
			var shouldShow = (tile.visibility == Visible || rc.renderInFog);
			rc.world.visible = shouldShow;
			alignEntity(e);
		}
	}

	function tryAddEntityToDrawLayer(e: Entity) {
		var lc = LocationComponent.get(e);
		var rc = TBRenderComponent.get(e);
		if (rc != null && rc.world != null) {
			var layer = this.drawLayers[rc.drawLayer];
			if (layer == null) layer = entitiesDrawLayer;
			var priority = rc.priority;

			if (layer == this.entitiesDrawLayer) {
				priority += this.yIndexMultiplier * lc.position.y;
			}

			this.entitiesDrawLayer.add(rc.world, priority);
			hideEntity(e, rc);
		}
		alignEntity(e, rc, lc);
		this.entities[e.id] = {e: e, rc: rc};
	}

	override public function entityRemoved(e: zf.ecs.Entity) {
		tryRemoveEntityFromDrawLayer(cast(e, ren.core.Entity));
	}

	function tryRemoveEntityFromDrawLayer(e: Entity) {
		this.entities.remove(e.id);
		var rc = TBRenderComponent.get(e);
		if (rc.world != null) rc.world.remove();
	}

	inline public function alignEntity(entity: Entity, rc: TBRenderComponent = null,
			lc: LocationComponent = null) {
		if (lc == null) lc = LocationComponent.get(entity);
		if (rc == null) rc = TBRenderComponent.get(entity);
		alignRenderObjectToGrid(rc.world, lc.position);
	}

	function getTileVisibility(tile: Tile): TileVisibility {
		return tile.visibility;
	}

	public inline function alignRenderObjectToGrid(obj: h2d.Object, gridPosition: Point2i,
			offset: Point2i = null) {
		if (obj == null) return;
		if (offset == null) offset = [0, 0];
		obj.x = gridPosition.x * this.gridSize.x + offset.x;
		obj.y = gridPosition.y * this.gridSize.y + offset.y;
	}

	inline public function gridPositionToPosition(x: Int, y: Int): Point2f {
		return new Point2f(gridXToPos(x), gridYToPos(y));
	}

	inline public function gridXToPos(x: Int): Float {
		return x * this.gridSize.x;
	}

	inline public function gridYToPos(y: Int): Float {
		return y * this.gridSize.y;
	}

	public function loadLevel(level: Level) {
		if (this.currentLevel == level) return;
		unloadLevel();
		this.currentLevel = level;
		if (this.currentLevel == null) return;

		for (xy => tile in level.map.iterateYX()) {
			if (tile == null) continue;
			for (e in tile.entities) {
				tryAddEntityToDrawLayer(e);
			}

			tile.visibility = Hidden;
			updateFog(tile);

			syncTile(tile);
		}

		var width = level.map.size.x * this.gridSize.x;
		var height = level.map.size.y * this.gridSize.y;

		if (this.bgColor != null) {
			var bgBm = new h2d.Bitmap(h2d.Tile.fromColor(this.bgColor, width, height));
			var bgLayer = this.drawLayers["bg"];
			bgLayer.addChild(bgBm);
		}
	}

	public function unloadLevel() {
		if (currentLevel == null) return;
		for (k => l in this.drawLayers) {
			l.removeChildren();
		}
		this.entities.clear();

		var bgLayer = this.drawLayers["bg"];
		bgLayer.removeChildren();
		this.fogDrawLayer.clear();
	}

	public function animateBump(entity: Entity, tile: Tile, ?onFinish: Void->Void,
			animateDuration: Float = .15, blocking: Bool = true) {
		var targetPosition = gridPositionToPosition(tile.position.x, tile.position.y);
		// @formatter:off
		var rc = TBRenderComponent.get(entity);
		var animator = blocking ? this.world.blockingAnimator : this.world.nonBlockingAnimator;
		animator.runAnim(
			new zf.animations.MoveToLocationByDuration(
				new WrappedObject(rc.world),
				[targetPosition.x, targetPosition.y],
				animateDuration
			).whenDone(function() {
				var location = LocationComponent.get(entity);
				alignRenderObjectToGrid(rc.world, location.position);
				if (onFinish != null) onFinish();
			})
		);
	}

	public function animateMove(entity: Entity, tile: Tile, ?onFinish: Void->Void, duration: Float = 0.15, blocking: Bool = true) {
		var targetPosition = gridPositionToPosition(tile.position.x, tile.position.y);
		// @formatter:off
		var rc = TBRenderComponent.get(entity);
		var animator = blocking ? this.world.blockingAnimator : this.world.nonBlockingAnimator;
		animator.runAnim(
			new zf.animations.MoveToLocationByDuration(
				new WrappedObject(rc.world),
				[targetPosition.x, targetPosition.y],
				duration
			).whenDone(function() {
				if (onFinish != null) onFinish();
			})
		);
	}

	public function animateProjectile(projectile: h2d.Object, source: Point2i, destination: Point2i, ?onFinish: Void->Void,
			animationDuration: Float = 1.0, drawLayer: Int = 15, blocking: Bool = true) {
		projectile.x = (source.x * this.gridSize.x) + (this.gridSize.x / 2);
		projectile.y = (source.y * this.gridSize.y) + (this.gridSize.y / 2);
		this.entitiesDrawLayer.add(projectile, drawLayer);
		var endpoint: Point2f = [
			(destination.x * this.gridSize.x) + (this.gridSize.x / 2),
			(destination.y * this.gridSize.y) + (this.gridSize.y / 2),
		];
		var animator = blocking ? this.world.blockingAnimator : this.world.nonBlockingAnimator;
		// @formatter:off
		animator.runAnim(
			new zf.animations.MoveToLocationByDuration(
				new WrappedObject(projectile),
				endpoint, animationDuration
			).whenDone(function() {
				this.entitiesDrawLayer.removeChild(projectile);
				if (onFinish != null) onFinish();
			})
		);
	}

	var currentHoverTile: Tile;
	override public function update(dt: Float) {
		var scene = this.drawLayer.getScene();
		if (allowHover && scene != null && this.currentLevel != null) {
			var mouseX = scene.mouseX;
			var mouseY = scene.mouseY;
			var drawLayerPosition = this.drawLayer.globalToLocal(new h2d.col.Point(mouseX, mouseY));
			var gridPosition: Point2i = [
				Std.int(drawLayerPosition.x / this.gridSize.x),
				Std.int(drawLayerPosition.y / this.gridSize.y)
			];
			var tile = this.currentLevel.getTile(gridPosition.x, gridPosition.y);
			if (this.currentHoverTile != tile) {
				if (currentHoverTile != null) onTileOut(this.currentHoverTile);
				this.currentHoverTile = tile;
				if (this.currentHoverTile != null) onTileOver(this.currentHoverTile, [mouseX, mouseY]);
			}
		}
	}

	var centeredEntity: Entity = null;
	public function centerOn(e: Entity) {
		this.centeredEntity = e;
		if (this.centeredEntity == null) return;
		if (this.viewport == null) return;

		var lc = LocationComponent.get(e);
		if (lc == null || lc.level == null) return;
		var x = this.drawLayer.x;
		var y = this.drawLayer.y;

		var centerX = this.viewport.xMin + (this.viewport.width / 2);
		var centerY = this.viewport.yMin + (this.viewport.height / 2);

		x = centerX - (lc.position.x * gridSize.x * this.drawLayer.scaleX) - (gridSize.x / 2 * this.drawLayer.scaleX );
		y = centerY - (lc.position.y * gridSize.y * this.drawLayer.scaleY) - (gridSize.y / 2 * this.drawLayer.scaleY );

		this.drawLayer.x = x;
		this.drawLayer.y = y;
	}

	inline public function recenter() {
		if (this.centeredEntity == null) return;
		this.centerOn(this.centeredEntity);
	}

	/**
		Inform the rendersystem that these tiles have their visibility updated
	**/
	function onVisibilityChanged(tiles: Array<Tile>) {
		var m = new Map<Int, ren.core.Tile>();
		for (t in tiles) {
			// for tiles in the changed list, we will always sync them
			syncTile(t);
			// if we had already updated the fog, then we don't need to call this again
			if (m[t.id] == null) {
				updateFog(t);
				m[t.id] = t;
			}

			if (this.tilingFog) {
				// for adjacents tiles, we will only update fog
				// this is mainly for cases if we have tiling fog
				for (y in -1...2) {
					for (x in -1...2) {
						var ta = t.level.getTile(t.position.x + x, t.position.y +y);
						if (ta == null || m[ta.id] != null) continue;
						updateFog(ta);
						m[ta.id] = ta;
					}
				}
			}
		}
	}

	/**
		Update the rendering for the fog
	**/
	function updateFog(tile: Tile) {
		// this is a simple fog
		// if tile is visible, no fog
		// if tile is hidden, 1.0 alpha
		// if tile is seen, 0.5 alpha of black
		var fog: BatchElement = tile.metadata["fog"];
		if (fog == null) {
			final t = this.fogTile;
			fog = this.fogDrawLayer.alloc(t);
			fog.x = gridXToPos(tile.position.x);
			fog.y = gridYToPos(tile.position.y);
			tile.metadata["fog"] = fog;
		}

		switch(tile.visibility) {
			case Hidden:
				fog.a = 1.0;
			case Seen:
				fog.a = .5;
			case Visible:
				// get the light level
				if (tile.lightLevel < MaxLightLevel) {
					final lv = Math.clampI(tile.lightLevel, 0, MaxLightLevel);
					fog.a = 0.3;
					fog.a -= (0.05 * (lv / MaxLightLevel * 6));
				} else {
					fog.a = 0;
				}
		}
	}

	/**
		@param position the position of the mouse relative to the scene
	**/
	function onTileOver(tile: Tile, position: Point2f) {}
	function onTileOut(tile: Tile) {}
}
