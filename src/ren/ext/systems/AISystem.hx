package ren.ext.systems;

import ren.core.World;
import ren.core.messages.EntityActiveTurn;
import ren.ext.components.AIComponent;

class AISystem extends zf.ecs.System {
	var world: World;

	public function new() {
		super();
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);
		setupListeners();
	}

	function setupListeners() {
		// @:listen AISystem EntityActiveTurn 100
		this.world.dispatcher.listen(EntityActiveTurn.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityActiveTurn);
			var c = AIComponent.get(m.entity);
			if (c == null) return;
			var ai = cast(c, AIComponent);
			ai.takeTurn(this.world);
		}, 100);
	}
}
