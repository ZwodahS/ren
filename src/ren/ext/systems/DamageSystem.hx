package ren.ext.systems;

import ren.core.World;
import ren.core.Entity;
import ren.ext.messages.EntityAttacked;
import ren.ext.messages.EntityDamaged;
import ren.ext.components.HealthComponent;

/**
	This is currently deprecated.

	There are many types of "attack". This current implementation is too generic and serve almost no purpose.
**/
class DamageSystem extends zf.ecs.System {
	var world: World;

	public function new() {
		super();
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);
		setupListeners();
	}

	function setupListeners() {
		this.world.dispatcher.listen(EntityAttacked.MessageType, function(message: zf.Message) {
			var m = cast(message, EntityAttacked);
			onAttacked(m.source, m.target);
		}, 100);
	}

	function onAttacked(source: Entity, target: Entity) {
		var hc = HealthComponent.get(target);
		var damage = getAttackDamage(source, target);
		hc.current -= damage;
		this.world.dispatcher.dispatch(new EntityDamaged(target, damage));
	}

	function getAttackDamage(source, target): Int {
		return 1;
	}
}
