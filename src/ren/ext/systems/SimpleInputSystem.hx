package ren.ext.systems;

import ren.core.World;
import ren.ext.messages.InputStateChanged;

/**
	Provide a simple input handling system.
**/
class SimpleInputSystem extends zf.ecs.System {
	public var world(default, null): World;

	var states: Map<String, InputState>;

	public var currentState(default, set): InputState = null;

	/**
		The default state for the input system.
		The system will switch to this defaultState when the stack is empty after popping
	**/
	public var defaultState: InputState = null;

	var stateStack: List<InputState>;

	function set_currentState(s: InputState): InputState {
		var oldState = this.currentState;
		if (oldState != null) oldState.onExit();

		this.currentState = s;

		if (this.currentState != null) {
			@:privateAccess this.currentState.inputSystem = this;
			this.currentState.onEnter();
		}

		if (this.world != null) {
			this.world.dispatcher.dispatch(new InputStateChanged(oldState, s));
		}
		return this.currentState;
	}

	public function new() {
		super();
		this.states = new Map<String, InputState>();
		this.stateStack = new List<InputState>();
	}

	public function setState(s: InputState): InputState {
		this.stateStack.clear();
		return this.currentState = s;
	}

	public function pushState(s: InputState): InputState {
		if (this.currentState != null) this.stateStack.push(this.currentState);
		return this.currentState = s;
	}

	public function popState(): InputState {
		var newState = this.stateStack.length > 0 ? this.stateStack.pop() : null;
		if (newState == null) newState = this.defaultState;
		return this.currentState = newState;
	}

	public function popAll() {
		this.stateStack.clear();
	}

	public function deactivate(is : InputState): Bool {
		if (this.currentState == is) {
			this.popState();
			return true;
		}
		return this.stateStack.remove(is);
	}

	public function registerState(?name: String, state: InputState) {
		if (name == null) name = state.name;
		if (name == null) throw('Unable to register input state ${state}. name undefined');
		this.states[name] = state;
		@:privateAccess state.inputSystem = this;
	}

	public function switchStateByName(name: String): Bool {
		if (this.states[name] == null) return false;
		this.currentState = this.states[name];
		return true;
	}

	override public function init(world: zf.ecs.World) {
		this.world = cast(world, World);
	}

	override public function onEvent(event: hxd.Event): Bool {
		if (this.currentState == null) return false;
		return this.currentState.handleEvent(event);
	}

	override public function update(dt: Float) {
		if (this.currentState != null) this.currentState.update(dt);
	}
}
