package ren.ext.systems;

class Window extends h2d.Layers {
	public var system(default, null): WindowRenderSystem;

	public function close() {
		if (this.system != null) this.system.closeWindow(this);
	}

	/**
		trigger the first time the window is added to the system
	**/
	public function onInit(system: WindowRenderSystem) {
		this.system = system;
	}

	/**
		trigger when the window is open
		happens after on Init
	**/
	public function onOpen() {}

	/**
		trigger when the window is close
		happens after onLoseFocus
	**/
	public function onClose() {}

	/**
		trigger when the window become the active one
		When the window is first added to the WindowRenderSystem, this will be called after onInit()
	**/
	public function onGainFocus() {}

	/**
		trigger when the window is no longer the active one
	**/
	public function onLoseFocus() {}

	public function update(dt: Float) {}
}

/**
	Window Management system

	This does not handle rendering.
	The idea is to extend this and handle the rendering in the child class
**/
class WindowRenderSystem extends zf.ecs.System {
	var windowStack: Array<Window>;

	var drawLayer: h2d.Layers;

	public function new(drawLayer: h2d.Layers) {
		super();
		this.windowStack = [];
		this.drawLayer = drawLayer;
	}

	override public function update(dt: Float) {
		super.update(dt);
		for (w in this.windowStack) w.update(dt);
	}

	public function isWindowOpen(window: Window): Bool {
		for (w in this.windowStack) {
			if (w == window) return true;
		}
		return false;
	}

	public function openWindow(window: Window, addToStack: Bool = true) {
		if (window.system == null) window.onInit(this);
		// if we are adding to stack, we will handle focus
		this.drawLayer.addChild(window);
		window.onOpen();
		onWindowOpened(window);
		if (addToStack) {
			if (windowStack.length > 0) windowStack[windowStack.length - 1].onLoseFocus();
			this.windowStack.push(window);
			window.onGainFocus();
			onActiveWindowChanged();
		}
	}

	function onWindowOpened(window: Window) {}

	public function closeWindow(window: Window): Bool {
		// if the window is not managed by this system, then we exit
		if (window.system != this) return false;
		// check if it is the active window
		var isActive = windowStack[windowStack.length - 1] == window;
		// just try to remove the window from stack
		this.windowStack.remove(window);
		window.remove();
		// if this was the active window, we need to handle the focus
		if (isActive) window.onLoseFocus();
		// callbacks for window and system
		onWindowClosed(window);
		window.onClose();
		// if the closed window was active, we need to change the focus
		if (isActive) {
			// if there another window on the stack, set it to focus
			if (this.windowStack.length != 0) this.windowStack[this.windowStack.length - 1].onGainFocus();
			// trigger active window
			onActiveWindowChanged();
		}
		return true;
	}

	function onWindowClosed(window: Window) {}

	function onActiveWindowChanged() {}
}
