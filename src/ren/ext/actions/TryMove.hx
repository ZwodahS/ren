package ren.ext.actions;

import zf.Assert;
import zf.Point2i;
import zf.Direction;

import ren.core.Tile;
import ren.core.Action;
import ren.core.Entity;
import ren.core.World;
import ren.core.ActionResult;
import ren.core.components.LocationComponent;
import ren.core.messages.EntityMoved;
import ren.ext.messages.EntityCanMove;

/**
	A general purpose Action for trying to move in a specific direction.

	This provide multiple method that can be specialised to provide different type of movement logic.
**/
class TryMove extends Action {
	var direction: Direction;

	var world: World;
	var position: Point2i;

	public function new(world: World, entity: Entity, ?direction: Direction, ?position: Point2i) {
		super(entity);
		this.direction = direction;
		this.position = position;
		Assert.assert(direction != null || position != null, 'Direction and position cannot be both null');
		this.world = world;
	}

	override public function perform(onFinish: ActionResult->Void): Bool {
		// make sure the entity is on a map before attempting the move.
		var location = LocationComponent.get(this.entity);
		if (location == null || location.position == null) return false;
		var prevPosition = location.position.copy();
		if (prevPosition == null) {
			return false;
		}
		var level = location.level;

		// make sure that the new position has a tile
		var newPosition = this.position != null ? this.position.clone() : prevPosition + direction;
		var toTile = level.map.get(newPosition.x, newPosition.y);
		if (toTile == null) {
			return false;
		}

		// find an entity that can be 'bumped'
		var bumpable = toTile.entities.findEntity(isEntityBumpable);

		// if the bumpable is handled, then we are done, else we continue to try to move to the tile.
		if (bumpable != null) {
			if (onEntityBump(toTile, bumpable, onFinish)) return true;
		}

		var fromTile = location.tile;
		var toTile = location.level.map.get(newPosition.x, newPosition.y);

		if (!canMoveEntity(fromTile, toTile)) return false;

		moveEntity(entity, toTile, onFinish);
		return true;
	}

	function moveEntity(entity: Entity, tile: Tile, onFinish: ActionResult->Void) {
		this.animateMove(entity, tile.position, function() {
			this.world.moveEntity(entity, tile.position);
			onEntityMoved();
			onFinish(getActionResult());
		});
	}

	function animateMove(entity: Entity, newPosition: Point2i, onFinish: Void->Void) {
		onFinish();
	}

	function getActionResult(): ActionResult {
		return new ActionResult();
	}

	/**
		check if an entity can move to this position.

		@param position the position to move to

		@return null if unable to move, or the movement cost if the entity is able to move.
	**/
	function canMoveEntity(fromTile: Tile, toTile: Tile): Bool {
		return true;
	}

	function isEntityBumpable(target: Entity): Bool {
		return false;
	}

	function onEntityBump(tile: Tile, target: Entity, onFinish: ActionResult->Void): Bool {
		return false;
	}

	function onEntityMoved() {}

	override public function toString() {
		if (this.position != null) return 'TryMove: ${this.entity.id} -> ${this.position}';
		return '<TryMove:${this.entity.id}->${this.direction}>';
	}
}
