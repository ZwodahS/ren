package ren.ext.actions;

import zf.Direction;

import ren.core.World;
import ren.core.Entity;
import ren.core.ActionResult;
import ren.ext.components.FacingComponent;
import ren.ext.messages.EntityChangeFacing;

class ChangeFacing extends ren.core.Action {
	var world: World;
	var direction: Direction;

	public function new(world: World, entity: Entity, direction: Direction) {
		super(entity);
		this.world = world;
		this.direction = direction;
	}

	override public function perform(onFinish: ActionResult->Void): Bool {
		var fc = FacingComponent.get(this.entity);
		if (fc == null) return false;
		var oldDirection = fc.direction;
		fc.direction = this.direction;
		this.world.dispatcher.dispatch(new EntityChangeFacing(this.entity, oldDirection, this.direction));
		// changing facing does not cost any action.
		onFinish(null);
		return true;
	}

	override public function toString(): String {
		return '<ChangeFacing:${this.direction.toString()}>';
	}
}
