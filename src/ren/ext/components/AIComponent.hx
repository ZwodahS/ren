package ren.ext.components;

import ren.core.Entity;
import ren.core.World;
import ren.ext.systems.AISystem;

class AIComponent extends zf.ecs.Component {
	public static final ComponentType = "AIComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public static function get(e: zf.ecs.DynamicEntity): AIComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, AIComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	override public function toString(): String {
		return '{c:AIComponent}';
	}

	var entity: Entity;

	public function new(entity: Entity) {
		super();
		this.entity = entity;
	}

	public function takeTurn(world: World) {}
}
