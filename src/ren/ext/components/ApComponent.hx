package ren.ext.components;

import ren.core.Entity;

class ApComponent extends zf.ecs.Component {
	public static final ComponentType = "ApComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	/**
		Action Point
	**/
	public var ap: Int = 0;

	/**
		Action Point per simulation
	**/
	public var speed: Int = 0;

	public function new(speed: Int = 1) {
		super();
		this.speed = speed;
	}

	public static function get(e: Entity): ApComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, ApComponent);
	}

	override public function toString(): String {
		return '{c:ApComponent: ${this.ap}|${this.speed}}';
	}
}
