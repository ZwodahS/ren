package ren.ext.components;

import ren.core.Entity;

using zf.MathExtensions;

class HealthComponent extends zf.ecs.Component {
	public static final ComponentType = "HealthComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public var max: Int = 1;
	public var current: Int = 1;

	public function new(max: Int = 1) {
		super();
		this.max = max;
		this.current = max;
	}

	public static function get(e: Entity): HealthComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, HealthComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	override public function toString(): String {
		return '{c:HealthComponent: ${this.current}/${this.max}}';
	}

	public function recover(amount: Int): Int {
		var actual = Math.clampI(amount, 0, this.max - this.current);
		this.current += actual;
		return actual;
	}
}
