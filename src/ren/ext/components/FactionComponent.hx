package ren.ext.components;

enum abstract Faction(String) from String to String {
	var Neutral = "neutral";
	var Enemies = "enemies";
	var Player = "player";
	var Allies = "allies";
}

class FactionComponent extends zf.ecs.Component {
	public static final ComponentType = "FactionComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public static function get(e: zf.ecs.DynamicEntity): FactionComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, FactionComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	public var faction: Faction;

	public function new(faction: Faction) {
		super();
		this.faction = faction;
	}

	override public function toString(): String {
		return '{c:FactionComponent:${this.faction}}';
	}
}
