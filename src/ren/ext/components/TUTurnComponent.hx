package ren.ext.components;

class TUTurnComponent extends zf.ecs.Component {
	public static final ComponentType = "TUTurnComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public static function get(e: zf.ecs.DynamicEntity): TUTurnComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, TUTurnComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	/**
		All entity will have time unit.
		This is set by the TimeUnitTurnSystem.
	**/
	public var timeunit: Int = 0;

	/**
		Used internally by TUTurnSystem
	**/
	public var endTurn: Bool = false;

	/**
		Used internally by TUTurnSystem
	**/
	public var tookAction: Bool = false;

	override public function toString(): String {
		return '{c:TUTurnComponent: ${this.timeunit}|${this.endTurn}}';
	}
}
