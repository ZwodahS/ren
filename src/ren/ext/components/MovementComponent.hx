package ren.ext.components;

class MovementComponent extends zf.ecs.Component {
	public static final ComponentType = "MovementComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public var movementCost: Int = 0;

	public function new(movementCost: Int = 1) {
		super();
		this.movementCost = movementCost;
	}

	override public function toString(): String {
		return '{c:MovementComponent: ${this.movementCost}}';
	}
}
