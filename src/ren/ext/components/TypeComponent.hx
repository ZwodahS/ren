package ren.ext.components;

class TypeComponent extends zf.ecs.Component {
	public static final ComponentType = "TypeComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public static function get(e: zf.ecs.DynamicEntity): TypeComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, TypeComponent);
	}

	public static function exists(e: zf.ecs.DynamicEntity): Bool {
		return e.getComponent(ComponentType) != null;
	}

	public var value: String = "";

	public function new(value: String) {
		super();
		this.value = value;
	}

	override public function toString(): String {
		return '{c:TypeComponent: ${value}}';
	}
}
