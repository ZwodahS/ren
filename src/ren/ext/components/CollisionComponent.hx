package ren.ext.components;

import ren.core.Entity;

class CollisionComponent extends zf.ecs.Component {
	public static final ComponentType = "CollisionComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	/**
		how much the entity blocks movement, ideally 0 == no block, 10 == fully block
		It is up to individual systems and how the various other values is used.
	**/
	public var movementBlock: Int = 0;

	/**
		how much the entity blocks vision, ideally 0 == no block, 10 == fully block
		it is up to individual systems and how the various other values is used.
	**/
	public var visionBlock: Int = 0;

	/**
		how much the entity block light. ideally 0 == no block, 10 == fully block.
	**/
	public var lightBlock: Int = 0;

	public function new(movementBlock: Int = 0, visionBlock: Int = 0, lightBlock: Int = 0) {
		super();
		this.movementBlock = movementBlock;
		this.visionBlock = visionBlock;
		this.lightBlock = lightBlock;
	}

	public static function get(e: Entity): CollisionComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, CollisionComponent);
	}

	override public function toString(): String {
		return '{c:CollisionComponent: ${this.movementBlock}|${this.visionBlock}}';
	}
}
