package ren.ext.components;

import ren.core.Entity;

import zf.Direction;

class FacingComponent extends zf.ecs.Component {
	public static final ComponentType = "FacingComponent";

	override public function get_type(): String {
		return ComponentType;
	}

	public var direction: Direction = North;

	public static function get(e: Entity): FacingComponent {
		var c = e.getComponent(ComponentType);
		return c == null ? null : cast(c, FacingComponent);
	}

	override public function toString(): String {
		return '{c:FacingComponent: ${this.direction}}';
	}
}
