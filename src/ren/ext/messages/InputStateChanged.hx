package ren.ext.messages;

import ren.ext.systems.InputState;

class InputStateChanged extends zf.Message {
	public static final MessageType = "InputStateChanged";

	override public function get_type(): String {
		return MessageType;
	}

	public var oldState: InputState;
	public var newState: InputState;

	public function new(oldState: InputState, newState: InputState) {
		super();
		this.oldState = oldState;
		this.newState = newState;
	}

	override public function toString(): String {
		return '[m:InputStateChanged: ${oldState}->${newState}]';
	}
}
