package ren.ext.messages;

import ren.core.Entity;

class EntityDamaged extends zf.Message {
	public static final MessageType = "EntityDamaged";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;
	public var amount: Int;

	public function new(entity: Entity, amount: Int) {
		super();
		this.entity = entity;
		this.amount = amount;
	}

	override public function toString(): String {
		return '[m:EntityDamaged: ${entity}|${amount}]';
	}
}
