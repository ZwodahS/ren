package ren.ext.messages;

import ren.core.Entity;

class EntityAttacking extends zf.Message {
	public static final MessageType = "EntityAttacking";

	override public function get_type(): String {
		return MessageType;
	}

	public var source: Entity;
	public var target: Entity;

	public function new(source: Entity, target: Entity) {
		super();
		this.source = source;
		this.target = target;
	}

	override public function toString(): String {
		return '[m:EntityAttacking: ${source}X${target}]';
	}
}
