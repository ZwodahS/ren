package ren.ext.messages;

import ren.core.Entity;
import ren.core.Tile;

class EntityCanMove extends zf.Message {
	public static final MessageType = "EntityCanMove";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity: Entity;
	public var fromTile: Tile;
	public var toTile: Tile;

	public var movementCost: Int;
	public var canMove: Bool;

	public function new(entity: Entity, fromTile: Tile, toTile: Tile) {
		super();
		this.entity = entity;
		this.fromTile = fromTile;
		this.toTile = toTile;
		this.canMove = true;
		this.movementCost = 1;
	}

	override public function toString(): String {
		return '[m:EntityCanMove: ${entity}?${fromTile}->${toTile}|${movementCost}:${canMove}]';
	}
}
