package ren.ext.messages;

import ren.core.Entity;

import zf.Direction;

class EntityChangeFacing extends zf.Message {
	public static final MessageType = "EntityChangeFacing";

	override public function get_type(): String {
		return MessageType;
	}

	public var entity(default, null): Entity;
	public var oldDirection(default, null): Direction;
	public var newDirection(default, null): Direction;

	public function new(entity: Entity, oldDirection: Direction, newDirection: Direction) {
		super();
		this.entity = entity;
		this.oldDirection = oldDirection;
		this.newDirection = newDirection;
	}

	override public function toString(): String {
		return '[m:EntityChangeFacing: ${entity}|${oldDirection}->${newDirection}]';
	}
}
