package ren.ext.messages;

import ren.core.Entity;

class EntityHealthChanged extends zf.Message {
	public static final MessageType = "EntityHealthChanged";

	public var entity: Entity;
	public var oldValue: Int;
	public var newValue: Int;

	public function new(entity: Entity, oldValue: Int, newValue: Int) {
		super(MessageType);
		this.entity = entity;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	override public function toString(): String {
		return '[m:EntityHealthChanged: ${entity}:${oldValue}->${newValue}]';
	}
}
