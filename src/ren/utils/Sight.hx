package ren.utils;

import zf.Point2i;
import zf.Direction;
import zf.IteratorUtils;
import zf.ds.Vector2D;

enum abstract Visibility(Int) from Int to Int {
	// this enum uses bitmask, do not change the order
	// bit 1 == visibility
	// bit 2 == blocks vision
	var OutOfBound = -1;
	var Invisible = 0;
	var Visible = 1;
	var BlockVision = 2; // block vision and invisible
	var VisibleAndBlockVision = 3;
}

typedef GetVisionBlock = (Int, Int, Direction) -> Visibility;

/**
	VisionAlgo
	Param 1: Point2i, the center of vision
	Param 2: Int, the range of the vision
	Param 3: (Int, Int, Direction -> Visibility),
	a function that takes in a x, y, Direction, return the visibility flag
	the direction is the direction from where it is entering the tile
	Return a 2D array, size will be equal to (2*Range + 1), with the center tile being the center.
	The 2D array should return a 0 as dark, and 10 as light, essentially 10 - Block
**/
typedef VisionAlgo = (Point2i, Int, GetVisionBlock, Vector2D<Int>) -> Vector2D<Int>;

class Sight {
	/**
		ShadowCast Vision algorithm
		modified from http://www.adammil.net/blog/v125_roguelike_vision_algorithms.html#shadowcode
		See references/algo/shadowcast.cs if the site is down

		NOTE / TODO
		Tue Dec 15 17:02:44 2020
		currently this vision is binary, i.e. it uses getVisionBlock in a way that it treats 10 == opaque,
		and non-10 is transparent.
		Need to modify this to handles partial light.
	**/
	public static function shadowCast(center: Point2i, range: Int, getVisionBlock: GetVisionBlock,
			vision: Vector2D<Int> = null): Vector2D<Int> {
		// translate world coordinate to the vision coordinate
		if (vision == null) vision = new Vector2D<Int>([range * 2 + 1, range * 2 + 1], 0);
		inline function setVision(x: Int, y: Int, value: Int) {
			vision.set(x - center.x + range, y - center.y + range, value);
		}
		setVision(center.x, center.y, 10);

		inline function distanceFunc(x: Int, y: Int): Int {
			return Std.int(Math.sqrt(Math.pow(x - center.x, 2) + Math.pow(y - center.y, 2)));
		}

		function computeShadow(octant: Int, startRangeX: Int, top: Point2i, bottom: Point2i) {
			for (x in startRangeX...range + 1) {
				var topY: Int = Std.int(top.x == 1 ? x : ((x * 2 + 1) * top.y + top.x - 1) / (top.x * 2));
				var bottomY: Int = Std.int(bottom.y == 0 ? 0 : ((x * 2 - 1) * bottom.y
					+ bottom.x) / (bottom.x * 2));

				var wasOpaque = -1; // 0:false, 1:true, -1:not applicable

				for (y in IteratorUtils.iterateInt(topY, bottomY)) {
					var tx = center.x;
					var ty = center.y;
					var direction: zf.Direction = None;
					switch (octant) {
						case 0:
							tx += x;
							ty -= y;
							direction = Direction.fromXY(x, -y);
						case 1:
							tx += y;
							ty -= x;
							direction = Direction.fromXY(y, -x);
						case 2:
							tx -= y;
							ty -= x;
							direction = Direction.fromXY(-y, -x);
						case 3:
							tx -= x;
							ty -= y;
							direction = Direction.fromXY(-x, -y);
						case 4:
							tx -= x;
							ty += y;
							direction = Direction.fromXY(-x, y);
						case 5:
							tx -= y;
							ty += x;
							direction = Direction.fromXY(-y, x);
						case 6:
							tx += y;
							ty += x;
							direction = Direction.fromXY(y, x);
						case 7:
							tx += x;
							ty += y;
							direction = Direction.fromXY(x, y);
					}

					var distance = distanceFunc(tx, ty);
					var inRange = range < 0 || distance <= range;
					// this line is not symmetrical, so we will use the more complex check
					// if (inRange) setVision(tx, ty, 10);
					var v = getVisionBlock(tx, ty, direction);
					var isVisible = v != OutOfBound && v & Visible == Visible;
					var blocksVision = v != OutOfBound && v & BlockVision == BlockVision;

					// @formatter:off
					if (isVisible && inRange &&
							(y != topY || top.y * x >= top.x * y) &&
							(y != bottomY || bottom.y * x <= bottom.x * y)) {
						setVision(tx, ty, 10);
					}
					if (x == range) continue;

					if (blocksVision) {
						if (wasOpaque == 0) {
							var newBottom = [x * 2 - 1, y * 2 + 1];
							if (!inRange || y == bottomY) {
								bottom = newBottom;
								break;
							} else {
								computeShadow(octant, x + 1, top, newBottom);
							}
						}
						wasOpaque = 1;
					} else {
						if (wasOpaque == 1) top = [x * 2 + 1, y * 2 + 1];
						wasOpaque = 0;
					}
				}
				if (wasOpaque != 0) break;
			}
		}
		for (octant in 0...8) computeShadow(octant, 1, [1, 1], [1, 0]);
		return vision;
	}

	/**
		Adam Milazzo's algorithm
		http://www.adammil.net/blog/v125_roguelike_vision_algorithms.html#mycode
		See references/algo/adammil.cs if the site is down.
	**/
	public static function milazzoVision(center: Point2i, range: Int, getVisionBlock: GetVisionBlock,
			vision: Vector2D<Int> = null): Vector2D<Int> {
		if (vision == null) vision = new Vector2D<Int>([range * 2 + 1, range * 2 + 1], 0);
		vision.setAll(0);

		var rangeSquared = Math.pow(range, 2);
		inline function setVision(x: Int, y: Int, octant: Int, value: Int) {
			var nx = range;
			var ny = range;
			switch (octant) {
				case 0:
					nx += x;
					ny -= y;
				case 1:
					nx += y;
					ny -= x;
				case 2:
					nx -= y;
					ny -= x;
				case 3:
					nx -= x;
					ny -= y;
				case 4:
					nx -= x;
					ny += y;
				case 5:
					nx -= y;
					ny += x;
				case 6:
					nx += y;
					ny += x;
				case 7:
					nx += x;
					ny += y;
			}
			vision.set(nx, ny, value);
		}
		inline function getVisibility(x: Int, y: Int, octant: Int): Visibility {
			var nx = center.x;
			var ny = center.y;
			var direction: Direction = None;
			switch (octant) {
				case 0:
					nx += x;
					ny -= y;
					direction = Direction.fromXY(x, -y);
				case 1:
					nx += y;
					ny -= x;
					direction = Direction.fromXY(y, -x);
				case 2:
					nx -= y;
					ny -= x;
					direction = Direction.fromXY(-y, -x);
				case 3:
					nx -= x;
					ny -= y;
					direction = Direction.fromXY(-x, -y);
				case 4:
					nx -= x;
					ny += y;
					direction = Direction.fromXY(-x, y);
				case 5:
					nx -= y;
					ny += x;
					direction = Direction.fromXY(-y, x);
				case 6:
					nx += y;
					ny += x;
					direction = Direction.fromXY(y, x);
				case 7:
					nx += x;
					ny += y;
					direction = Direction.fromXY(x, y);
			}
			return getVisionBlock(nx, ny, direction);
		}

		inline function isBlocked(x: Int, y: Int, octant: Int): Bool {
			return getVisibility(x, y, octant) & BlockVision == BlockVision;
		}
		vision.set(range, range, 10);
		inline function slopeGreater(slope: Point2i, x: Int, y: Int) {
			return slope.y * x > slope.x * y;
		}
		inline function slopeGreaterOrEqual(slope: Point2i, x: Int, y: Int) {
			return slope.y * x >= slope.x * y;
		}
		inline function slopeLess(slope: Point2i, x: Int, y: Int) {
			return slope.y * x < slope.x * y;
		}
		inline function slopeLessOrEqual(slope: Point2i, x: Int, y: Int) {
			return slope.y * x <= slope.x * y;
		}
		inline function distanceFunc(x: Int, y: Int): Int {
			return Std.int(Math.pow(x, 2) + Math.pow(y, 2));
		}
		function compute(octant: Int, startRangeX: Int, top: Point2i, bottom: Point2i) {
			for (x in startRangeX...range + 1) {
				var topY: Int = x;
				if (top.x != 1) {
					topY = Std.int(((x * 2 - 1) * top.y + top.x) / (top.x * 2));
					if (isBlocked(x, topY, octant)) {
						if (slopeGreaterOrEqual(top, x * 2,
							topY * 2 + 1) && (isBlocked(x, topY + 1, octant))) {
							topY++;
						}
					} else {
						var ax = x * 2;
						if (isBlocked(x + 1, topY + 1, octant)) ax++;
						if (slopeGreater(top, ax, topY * 2 + 1)) topY++;
					}
				}

				var bottomY = bottom.y;
				if (bottom.y != 0) {
					bottomY = Std.int(((x * 2 - 1) * bottom.y + bottom.x) / (bottom.x * 2));
					if (slopeGreaterOrEqual(bottom, x * 2, bottomY * 2 + 1)
						&& isBlocked(x, bottomY, octant)
						&& !isBlocked(x, bottomY + 1, octant)) bottomY++;
				}

				var wasOpaque = -1;
				for (y in IteratorUtils.iterateInt(topY, bottomY)) {
					if (range < 0 || distanceFunc(x, y) <= rangeSquared) {
						var v = getVisibility(x, y, octant);
						var isVisible = v != OutOfBound && v & Visible == Visible;
						var blocksVision = v != OutOfBound && v & BlockVision == BlockVision;

						if (isVisible && (y != topY || slopeGreaterOrEqual(top, x, y))
								&& (y != bottomY || slopeLessOrEqual(bottom, x, y))) {
							setVision(x, y, octant, 10);
						}
						if (x != range) {
							if (blocksVision) {
								if (wasOpaque == 0) {
									var nx: Int = x * 2;
									var ny: Int = y * 2 + 1;
									if (slopeGreater(top, nx, ny)) {
										if (y == bottomY) {
											bottom = [nx, ny];
											break;
										} else {
											compute(octant, x + 1, top, [nx, ny]);
										}
									} else {
										if (y == bottomY) return;
									}
								}
								wasOpaque = 1;
							} else {
								if (wasOpaque > 0) {
									var nx = x * 2;
									var ny = y * 2 + 1;
									if (slopeGreaterOrEqual(bottom, nx, ny)) return;
									top = [nx, ny];
								}
								wasOpaque = 0;
							}
						}
					}
				}
				if (wasOpaque != 0) break;
			}
		}
		for (octant in 0...8) compute(octant, 1, [1, 1], [1, 0]);
		return vision;
	}
}
