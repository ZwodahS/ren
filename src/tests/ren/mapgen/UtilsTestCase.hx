package tests.ren.mapgen;

import zf.Point2i;
import zf.Recti;
import zf.tests.TestCase;
import zf.ds.Vector2D;
import zf.ds.Vector2DRegion;

import ren.mapgen.Utils;

class UtilsTestCase extends TestCase {
	public function test_scanContinuousCells1() {
		// @formatter:off
		final strings = [
			"#####",
			"#####",
			"#####",
		];
		final cells = regionFromStringArray(strings);

		var v2d = Utils.scanContinuousCells(cells, ["#"]);
		for (pt => value in v2d.iterateYX()) {
			// for each point here, the value should be the pt + 1 for both x and y
			assertEqual(pt.x + 1, value.x);
			assertEqual(pt.y + 1, value.y);
		}
	}

	public function test_scanContinuousCells2() {
		// @formatter:off
		final strings = [
			"#####",
			"##xx#",
			"#xx##",
			"#####",
		];
		final cells = regionFromStringArray(strings);

		var v2d = Utils.scanContinuousCells(cells, ["#"]);

		// the output should be this
		final expected: Array<Array<Point2i>> = [
			[[1, 1], [2, 1], [3, 1], [4, 1], [5, 1]],
			[[1, 2], [2, 2], [0, 0], [0, 0], [1, 2]],
			[[1, 3], [0, 0], [0, 0], [1, 1], [2, 3]],
			[[1, 4], [2, 1], [3, 1], [4, 2], [5, 4]],
		];
		for (pt => value in v2d.iterateYX()) {
			var ep: Point2i = expected[pt.y][pt.x];
			assertEqual(ep.x, value.x, '@${pt}.x');
			assertEqual(ep.y, value.y, '@${pt}.y');
		}
	}

	public function test_canPlaceRoom1() {
		// @formatter:off
		final strings = [
			"#####",
			"##xx#",
			"#xx##",
			"#####",
		];
		final cells = regionFromStringArray(strings);
		var v2d = Utils.scanContinuousCells(cells, ["#"]);

		var r1 = new Recti(0, 0, 1, 1);
		var canPlace = Utils.canPlaceRoom(v2d, r1);
		assertEqual(canPlace, true);

		var r2 = new Recti(1, 0, 2, 1);
		canPlace = Utils.canPlaceRoom(v2d, r2);
		assertEqual(canPlace, false);

		var r3 = new Recti(2, 2, 4, 4);
		canPlace = Utils.canPlaceRoom(v2d, r3);
		assertEqual(canPlace, false);
	}

	public function test_getValidRoomsFromPoint1() {
		// @formatter:off
		final strings = [
			"#####",
			"#####",
			"##x##",
			"#####",
			"#####",
		];
		final cells = regionFromStringArray(strings);
		var v2d = Utils.scanContinuousCells(cells, ["#"]);

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], West, [2, 2]);
		assertEqual(valid.length, 2);
		assertTrue(valid[0] == new Recti(0, 1, 1, 2), '${valid[0]} != [0, 1, 1, 2]');
		assertTrue(valid[1] == new Recti(0, 2, 1, 3), '${valid[1]} != [0, 2, 1, 3]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], North, [3, 2]);
		assertEqual(valid.length, 3, '${valid} ${v2d}');
		assertTrue(valid[0] == new Recti(0, 0, 2, 1), '${valid[0]} != [0, 0, 2, 1]');
		assertTrue(valid[1] == new Recti(1, 0, 3, 1), '${valid[0]} != [1, 0, 3, 1]');
		assertTrue(valid[2] == new Recti(2, 0, 4, 1), '${valid[0]} != [2, 0, 4, 1]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], East, [2, 3]);
		assertEqual(valid.length, 3);
		assertTrue(valid[0] == new Recti(3, 0, 4, 2), '${valid[0]} != [3, 0, 4, 2]');
		assertTrue(valid[1] == new Recti(3, 1, 4, 3), '${valid[0]} != [3, 1, 4, 3]');
		assertTrue(valid[2] == new Recti(3, 2, 4, 4), '${valid[0]} != [3, 2, 4, 4]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], South, [3, 2]);
		assertEqual(valid.length, 3, '${valid} ${v2d}');
		assertTrue(valid[0] == new Recti(0, 3, 2, 4), '${valid[0]} != [0, 3, 2, 4]');
		assertTrue(valid[1] == new Recti(1, 3, 3, 4), '${valid[0]} != [1, 3, 3, 4]');
		assertTrue(valid[2] == new Recti(2, 3, 4, 4), '${valid[0]} != [2, 3, 4, 4]');
	}

	public function test_getValidRoomsFromPoint2() {
		// @formatter:off
		final strings = [
			"#####",
			"###x#",
			"##x##",
			"#x###",
			"#####",
		];
		final cells = regionFromStringArray(strings);
		var v2d = Utils.scanContinuousCells(cells, ["#"]);

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], West, [2, 2]);
		assertEqual(valid.length, 1);
		assertTrue(valid[0] == new Recti(0, 1, 1, 2), '${valid[0]} != [0, 1, 1, 2]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], North, [3, 2]);
		assertEqual(valid.length, 1, '${valid} ${v2d}');
		assertTrue(valid[0] == new Recti(0, 0, 2, 1), '${valid[0]} != [0, 0, 2, 1]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], East, [2, 3]);
		assertEqual(valid.length, 1);
		assertTrue(valid[0] == new Recti(3, 2, 4, 4), '${valid[0]} != [3, 2, 4, 4]');

		var valid = Utils.getValidRoomsFromPoint(v2d, [2, 2], South, [3, 2]);
		assertEqual(valid.length, 1, '${valid} ${v2d}');
		assertTrue(valid[0] == new Recti(2, 3, 4, 4), '${valid[0]} != [2, 3, 4, 4]');
	}

	function regionFromStringArray(strings: Array<String>): Vector2DRegion<String> {
		var v2d = new Vector2D<String>([strings[0].length, strings.length], null);
		for (y in 0...strings.length) {
			var xs = strings[y];
			for (x in 0...xs.length) {
				v2d.set(x, y, xs.charAt(x));
			}
		}
		return new Vector2DRegion(v2d);
	}
}
